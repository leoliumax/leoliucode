
//字符串反排函数,字符长度计算，字符统计。中文算2个字符；
var str = "abcdABCD就烟云过眼";
function re(str) {
	var s = str,
		args = [],
		len = 0;
//	console.log(s);
//	for (var i = 0, length = s.length; i < length; i++) {
//		args.push(s.charAt(i));
//	}
//	args.reverse();
//	return args.join('');
	return str.split('').reverse().join('');
}
console.log(re(str));

function mix(target, source) {			//如果最后参数是布尔，判定是否覆写同名属性
    var args = [].slice.call(arguments), i = 1, key,
            ride = typeof args[args.length - 1] == "boolean" ? args.pop() : true;
    if (args.length === 1) {				//处理$.mix(hash)的情形
        target = !this.window ? this : {};
        console.log(target,target.window)
        i = 0;
    }
    while ((source = args[i++])) {
        for (key in source) {			//允许对象糅杂，用户保证都是对象
            if (ride || !(key in target)) {
                target[ key ] = source[ key ];
            }			
        }
    }
    return target;
}
var makeArray = function(array) {
    var ret = [];
    if (array != null) {
        var i = array.length;
        // The window, strings (and functions) also have 'length'
        if (i == null || typeof array === "string" || jQuery.isFunction(array) || array.setInterval)
            ret[0] = array;
        else
            while (i)
                ret[--i] = array[i];
    }
    return ret;
}
var obja={a:00123,b:13123},objb={d:324,b:"bbbb"};
function tes(){
	this.names = "onasf";
	this.a = "Afun"
}
tes.prototype.as = "as";
function tesb(){
	this.names = "fnB";
	this.a = "fnB";
	this.fnb="fnbs"
}
tesb.prototype.bs = "bs";
tesb.prototype.as = "bas";
var a1 = new tes();
var a2 = new tesb();
var test = mix(a1,a2,true);
