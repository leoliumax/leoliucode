define(["avalon", "text!./one.html"], function(avalon, one) {

	avalon.templateCache.one = one
	avalon.define({
		$id: "one",
		username: "司徒正美"
	})
	avalon.vmodels.root.page = "one"
})