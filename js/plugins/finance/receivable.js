﻿//应收单列表页初始化数据
function queryReceiveShouldListLoad(){
	var url = "financial/receive/should/queryReceiveShouldByLoad.ihtml";
	RequestData(url,'{}',function(data){
		for (var key in data.obj) {  
			if(key == "accounts"){   //账户
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj[key],function(n,accountVo) {
	          		htmls +='<option value="'+ accountVo.accountNo +'">'+ accountVo.accountName +'</option>';
	          	});
	            $("#paymentAccountNo").html(htmls);
	        }else if(key == "recdiveTypes"){  
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,receiveTypeVo) {
	          		htmls +='<option value="'+ receiveTypeVo.id +'">'+ receiveTypeVo.typeName +'</option>';
	          	});
	          	$("#receiveShouldTypeNo").html(htmls);
	        }
        }		
        pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_ADD"]){
			var _url="receivable-add.html?supperResId="+ request('supperResId');
			$("#add").html('<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'新增应收单\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "financial/receive/should/queryReceiveShouldList.ihtml";
	var json ='{';
	if($("#receiveShouldNo").val()){json+='"receiveShouldNo":"'+$("#receiveShouldNo").val()+'",';}
	if($("#paymentAccountNo").val()){json+='"paymentAccountNo":"'+$("#paymentAccountNo").val()+'",';}
	if($("#amountStart").val()){json+='"amountStart":"'+$("#amountStart").val()+'",';}
	if($("#amountEnd").val()){json+='"amountEnd":"'+$("#amountEnd").val()+'",';}
	if($("#receiveShouldTypeNo").val()){json+='"receiveShouldTypeNo":"'+$("#receiveShouldTypeNo").val()+'",';}
	if($("#billCode").val()){json+='"billCode":"'+$("#billCode").val()+'",';}
	if($("#endDateStart").val()){json+='"endDateStart":"'+$("#endDateStart").val()+'",';}
	if($("#endDateEnd").val()){json+='"endDateEnd":"'+$("#endDateEnd").val()+'",';}
	if($("#settlementStatus").val()){json+='"settlementStatus":"'+$("#settlementStatus").val()+'",';}
	if($("#settlementPerson").val()){json+='"settlementPerson":"'+$("#settlementPerson").val()+'",';}
	if($("#settlementDateStart").val()){json+='"settlementDateStart":"'+$("#settlementDateStart").val()+'",';}
	if($("#settlementDateEnd").val()){json+='"settlementDateEnd":"'+$("#settlementDateEnd").val()+'",';}
	if($("#status").val()){json+='"status":"'+$("#status").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
	if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#verifierDateStart").val()){json+='"verifierDateStart":"'+$("#verifierDateStart").val()+'",';}
	if($("#verifierDateEnd").val()){json+='"verifierDateEnd":"'+$("#verifierDateEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,receiveShouldVo) {
			var numMoney = (new Number(receiveShouldVo.accountAmount)).toFixed(2);
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ receiveShouldVo.receiveShouldNo +'</td>'
							+'<td>'+ (receiveShouldVo.paymentAccountName?receiveShouldVo.paymentAccountName:"") +'</td>'
							+'<td>'+ (receiveShouldVo.receiveShouldAmount?receiveShouldVo.receiveShouldAmount:"") +'</td>'
							+'<td>'+ (numMoney?numMoney:"") +'</td>'
							+'<td>'+ (receiveShouldVo.endDate?receiveShouldVo.endDate:"") +'</td>'
							+'<td>'+ (receiveShouldVo.receiveShouldTypeName?receiveShouldVo.receiveShouldTypeName:"") +'</td>'
							+'<td>'+ (receiveShouldVo.billCode?receiveShouldVo.billCode:"") +'</td>'
							+'<td>'+ (receiveShouldVo.settlementStatus==1?"未核销":"已核销") +'</td>'
							+'<td>'+ (receiveShouldVo.settlementPerson?receiveShouldVo.settlementPerson:"") +'</td>'
							+'<td>'+ (receiveShouldVo.settlementDate?receiveShouldVo.settlementDate:"") +'</td>'
							+'<td>'+ receiveShouldVo.createTime +'</td>'
							+'<td>'+ (receiveShouldVo.status==1?"草稿":receiveShouldVo.status==2?"待审":receiveShouldVo.status==3?"已审":"注销") +'</td>'
							+'<td>'+ (receiveShouldVo.remark?receiveShouldVo.remark:"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_DETAIL"]){
								var _url="receivable-detail.html?id="+receiveShouldVo.id +"&supperResId="+ request('supperResId');
								htmls +='<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看应收单信息\')">查看</a>';
							}
//							//验证是否有编辑权限
//							if(receiveShouldVo.status==1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_EDIT"]){
//								htmls +='<a href="receivable-modity.html?id='+ receiveShouldVo.id +'&supperResId='+ supperResId +'">编辑</a>';
//							}
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#receiveShouldNo").val()){json+='"receiveShouldNo":"'+$("#receiveShouldNo").val()+'",';}
	if($("#paymentAccountNo").val()){json+='"paymentAccountNo":"'+$("#paymentAccountNo").val()+'",';}
	if($("#amountStart").val()){json+='"amountStart":"'+$("#amountStart").val()+'",';}
	if($("#amountEnd").val()){json+='"amountEnd":"'+$("#amountEnd").val()+'",';}
	if($("#receiveShouldTypeNo").val()){json+='"receiveShouldTypeNo":"'+$("#receiveShouldTypeNo").val()+'",';}
	if($("#billCode").val()){json+='"billCode":"'+$("#billCode").val()+'",';}
	if($("#endDateStart").val()){json+='"endDateStart":"'+$("#endDateStart").val()+'",';}
	if($("#endDateEnd").val()){json+='"endDateEnd":"'+$("#endDateEnd").val()+'",';}
	if($("#settlementStatus").val()){json+='"settlementStatus":"'+$("#settlementStatus").val()+'",';}
	if($("#settlementPerson").val()){json+='"settlementPerson":"'+$("#settlementPerson").val()+'",';}
	if($("#settlementDateStart").val()){json+='"settlementDateStart":"'+$("#settlementDateStart").val()+'",';}
	if($("#settlementDateEnd").val()){json+='"settlementDateEnd":"'+$("#settlementDateEnd").val()+'",';}
	if($("#status").val()){json+='"status":"'+$("#status").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
	if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#verifierDateStart").val()){json+='"verifierDateStart":"'+$("#verifierDateStart").val()+'",';}
	if($("#verifierDateEnd").val()){json+='"verifierDateEnd":"'+$("#verifierDateEnd").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "financial/receive/should/exportExcelReceiveShould.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看应收单详情
function receivebleDetail(){
	loading("正在提交查看应收单详情信息");
	var url = "financial/receive/should/queryReceiveShouldInfo.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			var receiveShouldVo = data.obj["receiveShouldVo"];
			var accountVo = data.obj["accountVo"];
			$("#id").val(receiveShouldVo.id);
			$("#versionLock").val(receiveShouldVo.versionLock);
			$("#organId").val(receiveShouldVo.organId);
			$("#receiveShouldNo").html("ID："+ receiveShouldVo.receiveShouldNo);
			$("#paymentAccountName").html("&nbsp;&nbsp;"+ receiveShouldVo.paymentAccountName);
			$("#receiveShouldAmount").html("&nbsp;&nbsp;"+ receiveShouldVo.receiveShouldAmount);
			$("#receiveShouldTypeName").html("&nbsp;&nbsp;"+ (receiveShouldVo.receiveShouldTypeName?receiveShouldVo.receiveShouldTypeName:""));
			$("#receiveShouldAmountBig").html("&nbsp;&nbsp;"+ convertCurrency(""+receiveShouldVo.receiveShouldAmount));
			$("#billCode").html("&nbsp;&nbsp;"+ (receiveShouldVo.billCode?receiveShouldVo.billCode:""));
			$("#endDate").html("&nbsp;&nbsp;"+ (receiveShouldVo.endDate?receiveShouldVo.endDate:""));
			$("#remark").html("&nbsp;&nbsp;"+ (receiveShouldVo.remark?receiveShouldVo.remark:""));
			$("#status").html("&nbsp;&nbsp;"+ (receiveShouldVo.status==1?"草稿":receiveShouldVo.status==2?"待审":receiveShouldVo.status==3?"已审":"注销"));
			$("#settlementStatus").html("&nbsp;&nbsp;"+ (receiveShouldVo.settlementStatus==1?"未核销":"已核销"));
			$("#settlementPerson").html("&nbsp;&nbsp;"+ (receiveShouldVo.settlementPerson?receiveShouldVo.settlementPerson:""));
			$("#settlementDate").html("&nbsp;&nbsp;"+ (receiveShouldVo.settlementDate?receiveShouldVo.settlementDate:""));
			$("#createPerson").html("&nbsp;&nbsp;"+ receiveShouldVo.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+ receiveShouldVo.createTime);
			$("#verifierPerson").html("&nbsp;&nbsp;"+ (receiveShouldVo.verifierPerson?receiveShouldVo.verifierPerson:""));
			$("#verifierDate").html("&nbsp;&nbsp;"+ (receiveShouldVo.verifierDate?receiveShouldVo.verifierDate:""));
			$("#updatePerson").html("&nbsp;&nbsp;"+ receiveShouldVo.updatePerson?receiveShouldVo.updatePerson:"");
			$("#updateDate").html("&nbsp;&nbsp;"+ receiveShouldVo.updateDate?receiveShouldVo.updateDate:"");
			$("#availableLimit").html("&nbsp;&nbsp;"+ accountVo.availableLimit?accountVo.availableLimit:"");
			$("#usedLimit").html("&nbsp;&nbsp;"+ accountVo.usedLimit?accountVo.usedLimit:"");
			$("#accountAmount").html("&nbsp;&nbsp;"+ accountVo.accountAmount?accountVo.accountAmount:"");
			var pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有提交权限
			if(receiveShouldVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_SUBMIT"]){
				$("#add").append('<button class="btn btn-default" onclick="updateReceiveShould(2)">提交审核</button>&nbsp;');
			}
			//验证是否有审核权限
			if(receiveShouldVo.status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_VERIFI"]){
				$("#add").append('<button class="btn btn-default" onclick="updateReceiveShould(3)">审核通过</button>&nbsp;<button class="btn btn-default" onclick="updateReceiveShould(1)">审核失败</button>');
			}
			//验证是否有审核权限
			if(receiveShouldVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_CANCEL"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="updateReceiveShould(4)">注销单据</button>');
			}
			//验证是否有核销权限
			if(receiveShouldVo.settlementStatus == 1 && receiveShouldVo.status == 3 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SHOULD_ABOLISH"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="settlementReceiveShould(3)">核销</button>');
			}
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加页面加载数据
function addReceivebleLoad(){
	loading("正在提交加载添加应收单信息");
	var url = "financial/receive/should/toAddReceiveShould.ihtml";
	RequestData(url,"{}",function(data){
		unloading();
		if(data.code == "0" && data.obj["recdiveTypes"]) {  
			var htmls = '<option value="">请选择</option>';
            $.each(data.obj["recdiveTypes"],function(n,recdiveType) {
          		htmls +='<option value="'+ recdiveType.id +'">'+ recdiveType.typeName +'</option>';
          	});
            $("#receiveShouldTypeNo").html(htmls);
    	}	
	});
	//添加验证
    validatData(1);
}

//添加应收单
function addReceiveble(){
	loading("正在提交添加应收单信息");
	var url = "financial/receive/should/addReceiveShould.ihtml";
	var json ='{';
	json+='"text4":"'+$("#availableLimit").val()+'",';
	json+='"text5":"'+$("#usedLimit").val()+'",';
	json+='"paymentAccountNo":"'+$("#accountNo").val()+'",';
	json+='"paymentAccountName":"'+$("#accountName").val()+'",';
	json+='"receiveShouldTypeNo":"'+$("#receiveShouldTypeNo").val()+'",';
	json+='"receiveShouldTypeName":"'+$("#receiveShouldTypeNo").find("option:selected").text()+'",';
	json+='"receiveShouldAmount":"'+$("#receiveShouldAmount").val()+'",';
	json+='"endDate":"'+$("#endDate").val()+' 00:00:00",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading()
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editReceivebleLoad(){
	loading("正在提交加载编辑应收单信息");
	var url = "financial/receive/should/toEditReceiveShould.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0") { 
			if(data.obj["recdiveTypes"]) {  
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["recdiveTypes"],function(n,recdiveType) {
	          		htmls +='<option value="'+ recdiveType.id +'">'+ recdiveType.receiveTypeName +'</option>';
	          	});
	            $("#receiveShouldTypeNo").html(htmls);
	    	}
			if(data.obj["receiveShouldVo"]) {  
				var receiveShouldVo = data.obj["receiveShouldVo"];
				$("#id").val(receiveShouldVo.id);
				$("#versionLock").val(receiveShouldVo.versionLock);
				$("#accountNo").val(receiveShouldVo.paymentAccountNo);
				$("#accountName").val(receiveShouldVo.paymentAccountName);
				$("#receiveShouldTypeNo").val(receiveShouldVo.receiveShouldTypeNo);
				$("#receiveShouldAmount").val(receiveShouldVo.receiveShouldAmount);
				$("#endDate").val(receiveShouldVo.endDate);
				blurAmount();
				$("#remark").val(receiveShouldVo.remark);
	    	}	
        }	
	});
	//添加验证
    validatData(2);
}

//编辑应收单
function editReceiveble(){
	loading("正在提交编辑应收单管信息");
	var url = "financial/receive/should/editReceiveShould.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"paymentAccountNo":"'+$("#accountNo").val()+'",';
	json+='"paymentAccountName":"'+$("#accountName").val()+'",';
	json+='"receiveShouldTypeNo":"'+$("#receiveShouldTypeNo").val()+'",';
	json+='"receiveShouldTypeName":"'+$("#receiveShouldTypeNo").find("option:selected").text()+'",';
	json+='"receiveShouldAmount":"'+$("#receiveShouldAmount").val()+'",';
	json+='"endDate":"'+$("#endDate").val()+' 00:00:00",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//添加验证
function validatData(type){
    var validate = $("#expen-from").validate({
        rules:{
            accountName:{required:true},
            receiveShouldAmount:{required:true,number:true},
            endDate:{required:true,date:true}
        },
        submitHandler: function(form){
        	if(!$("#receiveShouldTypeNo").val()){
        		layer.alert("请选择应收类型");
        		return;
        	}
        	if(type == 1){
				addReceiveble();
        	}else{
				editReceiveble();
        	}
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });      
}

//更新应收单状态
function updateReceiveShould(status){
	loading("正在提交更新应收单状态信息");
	var url = "financial/receive/should/updateReceiveShould.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	if($("#cancel").val()){
		json+='"cancellationReason":"'+$("#cancel").val()+'",';
	}else if(status == 4){
//		layer.alert("请录入注销原因");
//		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//核销应收单
function settlementReceiveShould(status){
	loading("正在提交核销应收单信息");
	var url = "financial/receive/should/settlementReceiveShould.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"organId":"'+$("#organId").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount,ownerAccountAmount,availableLimit,usedLimit){
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
	$("#availableLimit").val(availableLimit);
	$("#usedLimit").val(usedLimit);
	var dt = new Date();
    var dt30 = new Date(dt.getTime()+parseInt(accountPeriod)*24*3600*1000);
	$("#endDate").val(dt30.toLocaleDateString().replace('/','-').replace('/','-'));
}

//金额转化大小写
function blurAmount(){
	if($("#receiveShouldAmount").val() && !(isNaN($("#receiveShouldAmount").val()))){
		$("#bigAmount").html(convertCurrency($("#receiveShouldAmount").val()));
	}
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}