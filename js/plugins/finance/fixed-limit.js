﻿//固定额度列表页初始化数据
function queryApplyForLimitListLoad(){
	var url = "financial/limit/fixed/pagedQueryFixedLimitBaseLoad.ihtml";
	RequestData(url,'{}',function(data){
		if (data.code == "0") {  
			if(data.obj["accountVos"]){   //账号
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["accountVos"],function(n,accountVo) {
	          		htmls +='<option value="'+ accountVo.accountNo +'">'+ accountVo.accountName +'</option>';
	          		
	          	});
	            $("#accountNo").html(htmls);
	        }
			pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有添加权限
			if(pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_FIXED_ADD"]){
				$("#add").html('<a href="fixed-amount-add.html?supperResId='+ supperResId +'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
			}
			//验证是否有导出权限
			if(pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_FIXED_EXPORT"]){
				var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
				$("#excel").html(htmls);
			}
			$("#search").click();
        }		
	});
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "financial/limit/fixed/pagedQueryFixedLimit.ihtml";
	var json ='{';
	if($("#accountNo").val()){json+='"accountNo":"'+$("#accountNo").val()+'",';}
	if($("#newLimitStart").val()){json+='"newLimitStart":"'+$("#newLimitStart").val()+'",';}
	if($("#newLimitEnd").val()){json+='"newLimitEnd":"'+$("#newLimitEnd").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#createTimeStart").val()){json+='"createTimeStart":"'+$("#createTimeStart").val()+'",';}
	if($("#createTimeEnd").val()){json+='"createTimeEnd":"'+$("#createTimeEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json+='"applyType":1,';
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,applyForLimit) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ applyForLimit.limitApplyNo +'</td>'
							+'<td>'+ applyForLimit.accountName +'</td>'
							+'<td>'+ applyForLimit.currentLimit +'</td>'
							+'<td>'+ applyForLimit.newLimit +'</td>'
							+'<td>'+ (applyForLimit.status==1?"草稿":applyForLimit.status==2?"待审":applyForLimit.status==3?"已审":"注销") +'</td>'
							+'<td>'+ applyForLimit.createTime +'</td>'
							+'<td>'+ (applyForLimit.remark?applyForLimit.remark:"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_FIXED_DETAIL"]){
								htmls +='<a href="fixed-amount-detail.html?id='+ applyForLimit.id +'&supperResId='+ supperResId +'">查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(applyForLimit.status==1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_FIXED_EDIT"]){
								htmls +='<a href="fixed-amount-modity.html?id='+ applyForLimit.id +'&supperResId='+ supperResId +'">编辑</a>';
							}
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#accountNo").val()){json+='"accountNo":"'+$("#accountNo").val()+'",';}
	if($("#newLimitStart").val()){json+='"newLimitStart":"'+$("#newLimitStart").val()+'",';}
	if($("#newLimitEnd").val()){json+='"newLimitEnd":"'+$("#newLimitEnd").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#createTimeStart").val()){json+='"createTimeStart":"'+$("#createTimeStart").val()+'",';}
	if($("#createTimeEnd").val()){json+='"createTimeEnd":"'+$("#createTimeEnd").val()+'",';}
	json+='"applyType":1,';
	json +='"pageNo":1}';
	var url = getIP() + "financial/limit/fixed/exportFixedLimit.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看申请单详情
function applyForLimitListDetail(){
	loading("正在提交查看固定额度调额单信息");
	var url = "financial/limit/fixed/toDetailFixedLimit.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			var applyForLimit = data.obj["applyForLimitVo"];
			$("#id").val(applyForLimit.id);
			$("#versionLock").val(applyForLimit.versionLock);
			$("#limitApplyNo").html("ID："+ applyForLimit.limitApplyNo);
			$("#accountName").html("&nbsp;&nbsp;"+ applyForLimit.accountName);
			$("#currentLimit").html("&nbsp;&nbsp;"+ applyForLimit.currentLimit);
			$("#newLimit").html("&nbsp;&nbsp;"+ applyForLimit.newLimit);
			$("#applyLimit").html("&nbsp;&nbsp;"+ ((applyForLimit.newLimit-applyForLimit.currentLimit)>0?("+"+(applyForLimit.newLimit-applyForLimit.currentLimit)):(applyForLimit.newLimit-applyForLimit.currentLimit)));
			$("#remark").html("&nbsp;&nbsp;"+ (applyForLimit.remark?applyForLimit.remark:""));
			$("#status").html("&nbsp;&nbsp;"+ (applyForLimit.status==1?"草稿":applyForLimit.status==2?"待审":applyForLimit.status==3?"已审":"注销"));
			$("#createPerson").html("&nbsp;&nbsp;"+ applyForLimit.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+ applyForLimit.createTime);
			$("#verifierPerson").html("&nbsp;&nbsp;"+ (applyForLimit.verifierPerson?applyForLimit.verifierPerson:""));
			$("#verifierDate").html("&nbsp;&nbsp;"+ (applyForLimit.verifierDate?applyForLimit.verifierDate:""));
			$("#updatePerson").html("&nbsp;&nbsp;"+ (applyForLimit.updatePerson?applyForLimit.updatePerson:""));
			$("#updateDate").html("&nbsp;&nbsp;"+ (applyForLimit.updateDate?applyForLimit.updateDate:""));
			var pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有提交权限
			if(applyForLimit.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_FIXED_SUBMIT"]){
				$("#add").append('<button class="btn btn-default" onclick="updateApplyForLimit(1)">提交审核</button>&nbsp;');
			}
			//验证是否有审核权限
			if(applyForLimit.status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_FIXED_VERIFI"]){
				$("#add").append('<button class="btn btn-default" onclick="updateApplyForLimit(2)">审核通过</button>&nbsp;<button class="btn btn-default" onclick="updateApplyForLimit(3)">审核失败</button>');
			}
			//验证是否有审核权限
			if(applyForLimit.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_FIXED_CANCEL"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="updateApplyForLimit(4)">注销单据</button>');
			}
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加固定额度申请单
function addApplyForLimit(){
	loading("正在提交新增固定额度调额单信息");
	var url = "financial/limit/fixed/addFixedLimit.ihtml";
	var json ='{';
	json+='"accountNo":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"currentLimit":"'+$("#fixedLimit").html()+'",';
	json+='"newLimit":"'+$("#newLimit").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editApplyForLimitLoad(){
	loading("正在提交加载编辑固定额度数据的信息");
	var url = "financial/limit/fixed/toUpdateFixedLimit.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0") {  
			var expensesVo = data.obj["applyForLimitVo"];
			$("#id").val(expensesVo.id);
			$("#versionLock").val(expensesVo.versionLock);
			$("#accountNo").val(expensesVo.accountNo);
			$("#accountName").val(expensesVo.accountName);
			$("#fixedLimit").html(expensesVo.currentLimit);
			$("#newLimit").val(expensesVo.newLimit);
			$("#sumLimit").html((expensesVo.newLimit-expensesVo.currentLimit)>0?("+"+(expensesVo.newLimit-expensesVo.currentLimit)):(expensesVo.newLimit-expensesVo.currentLimit));
			$("#remark").val(expensesVo.remark);
        }	
	});
	//添加验证
    validatData(2);
}

//编辑固定额度申请单
function editApplyForLimit(){
	loading("正在提交编辑固定额度申请单信息");
	var url = "financial/limit/fixed/updateFixedLimit.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"accountNo":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"currentLimit":"'+$("#fixedLimit").html()+'",';
	json+='"newLimit":"'+$("#newLimit").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//添加固定额度申请单验证
function validatData(type){
    var validate = $("#expen-from").validate({
        rules:{
            newLimit:{required:true,digits:true}
        },
        submitHandler: function(form){
        	if(type == 1){
				addApplyForLimit();
        	}else{
				editApplyForLimit();
        	}
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });      
}

//更新固定额度申请单状态
function updateApplyForLimit(status){
	var url = "financial/limit/fixed/operateFixedLimit.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"operateType":"'+ status +'",';
	if($("#cancel").val()){
		json+='"cancellationReason":"'+$("#cancel").val()+'",';
	}else if(status == 4){
//		layer.alert("请录入注销原因");
//		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		layer.alert(data.msg);
		if(data.code == "0"){
			location.href = 'fixed-amount-detail.html?id='+ $("#id").val() +'&supperResId='+ supperResId;
		}
	});
}

//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount){
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
	$("#fixedLimit").html(fixedLimit);
	sumAmount();
}

//累计额度
function sumAmount(){
	if($("#newLimit").val() && $("#fixedLimit").html() ){
		var newLimit =  !(isNaN($("#newLimit").val()))?parseInt($("#newLimit").val()):0;
		var fixedLimit = !(isNaN($("#fixedLimit").html()))?parseInt($("#fixedLimit").html()):0;
		$("#sumLimit").html((newLimit-fixedLimit)>0?("+"+(newLimit-fixedLimit)):(newLimit-fixedLimit));
	}
}
