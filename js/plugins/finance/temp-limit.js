﻿//账户列表页初始化数据
function queryApplyForLimitListLoad(){
	var url = "financial/limit/temp/pagedQueryTempLimitLoad.ihtml";
	RequestData(url,'{}',function(data){
		if (data.code == "0") {  
			if(data.obj["accountVos"]){   //账号
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["accountVos"],function(n,accountVo) {
	          		htmls +='<option value="'+ accountVo.accountNo +'">'+ accountVo.accountName +'</option>';
	          	});
	            $("#accountNo").html(htmls);
	        }
			pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有添加权限
			if(pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_TEMP_ADD"]){
				$("#add").html('<a href="temp-amount-add.html?supperResId='+ supperResId +'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
			}
			//验证是否有导出权限
			if(pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_TEMP_EXPORT"]){
				var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
				$("#excel").html(htmls);
			}
			$("#search").click();
        }		
	});
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "financial/limit/temp/pagedQueryTempLimit.ihtml";
	var json ='{';
	if($("#accountNo").val()){json+='"accountNo":"'+$("#accountNo").val()+'",';}
	if($("#newLimitStart").val()){json+='"newLimitStart":"'+$("#newLimitStart").val()+'",';}
	if($("#newLimitEnd").val()){json+='"newLimitEnd":"'+$("#newLimitEnd").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#createTimeStart").val()){json+='"createTimeStart":"'+$("#createTimeStart").val()+'",';}
	if($("#createTimeEnd").val()){json+='"createTimeEnd":"'+$("#createTimeEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json+='"applyType":2,';
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,applyForLimit) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ applyForLimit.limitApplyNo +'</td>'
							+'<td>'+ applyForLimit.accountName +'</td>'
							+'<td>'+ applyForLimit.newLimit +'</td>'
							+'<td>'+ applyForLimit.tempLimitValidity.substring(0,10)+'</td>'
							+'<td>'+ applyForLimit.adjustCause +'</td>'
							+'<td>'+ (applyForLimit.status==1?"草稿":applyForLimit.status==2?"待审":applyForLimit.status==3?"已审":"注销") +'</td>'
							+'<td>'+ applyForLimit.createPerson +'</td>'
							+'<td>'+ applyForLimit.createTime +'</td>'
							+'<td>'+ (applyForLimit.remark?applyForLimit.remark:"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_TEMP_DETAIL"]){
								htmls +='<a href="temp-amount-detail.html?id='+ applyForLimit.id +'&supperResId='+ supperResId +'">查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(applyForLimit.status==1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_TEMP_EDIT"]){
								htmls +='<a href="temp-amount-modity.html?id='+ applyForLimit.id +'&supperResId='+ supperResId +'">编辑</a>';
							}
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#accountNo").val()){json+='"accountNo":"'+$("#accountNo").val()+'",';}
	if($("#newLimitStart").val()){json+='"newLimitStart":"'+$("#newLimitStart").val()+'",';}
	if($("#newLimitEnd").val()){json+='"newLimitEnd":"'+$("#newLimitEnd").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#createTimeStart").val()){json+='"createTimeStart":"'+$("#createTimeStart").val()+'",';}
	if($("#createTimeEnd").val()){json+='"createTimeEnd":"'+$("#createTimeEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json+='"applyType":2,';
	json +='"pageNo":1}';
	var url = getIP() + "financial/limit/temp/exportTempLimit.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看临时额度调额单
function applyForLimitListDetail(){
	loading("提交查看临时额度调额单信息");
	var url = "financial/limit/temp/toDetailTempLimit.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			var applyForLimit = data.obj["applyForLimitVo"];
			$("#id").val(applyForLimit.id);
			$("#versionLock").val(applyForLimit.versionLock);
			$("#limitApplyNo").html("ID："+ applyForLimit.limitApplyNo);
			$("#limitApplyNo").html("ID："+ applyForLimit.limitApplyNo);
			$("#accountName").html("&nbsp;&nbsp;"+ applyForLimit.accountName);
			$("#adjustCause").html("&nbsp;&nbsp;"+ applyForLimit.adjustCause);
			$("#tempLimitValidity").html("&nbsp;&nbsp;"+ applyForLimit.tempLimitValidity.substring(0,10));
			$("#newLimit").html("&nbsp;&nbsp;"+ applyForLimit.newLimit);
			$("img").attr("src",applyForLimit.certificateUrl);
			$("#remark").html("&nbsp;&nbsp;"+ (applyForLimit.remark?applyForLimit.remark:""));
			$("#status").html("&nbsp;&nbsp;"+ (applyForLimit.status==1?"草稿":applyForLimit.status==2?"待审":applyForLimit.status==3?"已审":"注销"));
			$("#createPerson").html("&nbsp;&nbsp;"+ applyForLimit.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+ applyForLimit.createTime);
			$("#verifierPerson").html("&nbsp;&nbsp;"+ (applyForLimit.verifierPerson?applyForLimit.verifierPerson:""));
			$("#verifierDate").html("&nbsp;&nbsp;"+ (applyForLimit.verifierDate?applyForLimit.verifierDate:""));
			$("#updatePerson").html("&nbsp;&nbsp;"+ applyForLimit.updatePerson);
			$("#updateDate").html("&nbsp;&nbsp;"+ applyForLimit.updateDate);
			var pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有提交权限
			if(applyForLimit.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_TEMP_SUBMIT"]){
				$("#add").append('<button class="btn btn-default" onclick="updateApplyForLimit(1)">提交审核</button>&nbsp;');
			}
			//验证是否有审核权限
			if(applyForLimit.status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_TEMP_VERIFI"]){
				$("#add").append('<button class="btn btn-default" onclick="updateApplyForLimit(2)">审核通过</button>&nbsp;<button class="btn btn-default" onclick="updateApplyForLimit(3)">审核失败</button>');
			}
			//验证是否有审核权限
			if(applyForLimit.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_LIMIT_TEMP_CANCEL"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="updateApplyForLimit(4)">注销单据</button>');
			}
			
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加临时额度申请单
function addApplyForLimit(){
	loading("提交添加临时额度申请单信息");
	var url = "financial/limit/temp/addTempLimit.ihtml";
	var json ='{';
	json+='"accountNo":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"adjustCause":"'+$("#adjustCause").val()+'",';
	json+='"newLimit":"'+$("#newLimit").val()+'",';
	json+='"tempLimitValidity":"'+$("#tempLimitValidity").val()+' 00:00:00",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editApplyForLimitLoad(){
	loading("加载编辑临时额度数据");
	var url = "financial/limit/temp/toUpdateTempLimit.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0") { 
			var expensesVo = data.obj["applyForLimitVo"];
			$("#id").val(expensesVo.id);
			$("#versionLock").val(expensesVo.versionLock);
			$("#accountNo").val(expensesVo.accountNo);
			$("#accountName").val(expensesVo.accountName);
			$("#adjustCause").val(expensesVo.adjustCause);
			$("#newLimit").val(expensesVo.newLimit);
			$("#tempLimitValidity").val(expensesVo.tempLimitValidity.substring(0,10));
			$("#remark").val(expensesVo.remark);
			amountBig();
			
        }	
	});
	//添加验证
    validatData(2);
}

//编辑临时额度申请单
function editApplyForLimit(){
	loading("正在提交编辑临时额度申请单信息");
	var url = "financial/limit/temp/updateTempLimit.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"accountNo":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"adjustCause":"'+$("#adjustCause").val()+'",';
	json+='"newLimit":"'+$("#newLimit").val()+'",';
//	json+='"tempLimitValidity":"'+$("#tempLimitValidity").val()+'",';
	json+='"tempLimitValidity":"'+$("#tempLimitValidity").val()+' 00:00:00",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//更新临时额度申请单状态
function updateApplyForLimit(status){
	loading("正在提交更新临时额度申请单状态信息");
	var url = "financial/limit/temp/operateTempLimit.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"operateType":"'+ status +'",';
	if($("#cancel").val()){
		json+='"cancellationReason":"'+$("#cancel").val()+'",';
	}else if(status == 4){
//		layer.alert("请录入注销原因");
//		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		layer.alert(data.msg);
		if(data.code == "0"){
			location.href = 'temp-amount-detail.html?id='+ $("#id").val() +'&supperResId='+ supperResId;
		}
	});
}


//添加临时额度申请单验证
function validatData(type){
    var validate = $("#expen-from").validate({
        rules:{
            newLimit:{required:true,number:true}
        },
        submitHandler: function(form){
        	if(type == 1){
				addApplyForLimit();
        	}else{
				editApplyForLimit();
        	}
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });      
}

//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount){
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
}

//金额大写
function amountBig(){
	if($("#newLimit").val() && isNaN($("#fixedLimit").val()) ){
		$("#amountBig").html(convertCurrency($("#newLimit").val()));
	}
}
