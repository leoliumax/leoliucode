﻿//付款单列表页初始化数据
function queryPaymentListLoad(){
	var url = "payment/payment/querypaymentshoulBaseLoad.ihtml";
	RequestData(url,'{}',function(data){
		for (var key in data.obj) {  
			if(key == "accounother"){   //账号
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj[key],function(n,accountVo) {
	          		htmls +='<option value="'+ accountVo.id +'">'+ accountVo.accountName +'</option>';
	          	});
	            $("#payment-account").html(htmls);
	        }
	        else if(key == "myCardVos"){
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,cardVo) {
	          		htmls +='<option value="'+ cardVo.cardNo +'">'+ cardVo.cardNickname +'</option>';
	          	});
	          	$("#paymentCardNo").html(htmls);
	        }
        }	
         pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_ADD"]){
			var _url="payment-add.html?supperResId="+ request('supperResId');
			$("#add").html('<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'新增付款单\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');

			//$("#add").html('<a href="payment-add.html?supperResId='+ supperResId +'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限 
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "payment/payment/querypaymentshouldpage.ihtml";
	var json ='{';
	if($("#paymentNo").val()){json+='"paymentNo":"'+$("#paymentNo").val()+'",';}
	if($("#receiveAccountName").val()){json+='"receiveAccountName":"'+$("#receiveAccountName").val()+'",';}
	if($("#paymentAmountStart").val()){json+='"paymentAmountStart":"'+$("#paymentAmountStart").val()+'",';}
	if($("#paymentAmountEnd").val()){json+='"paymentAmountEnd":"'+$("#paymentAmountEnd").val()+'",';}
	if($("#paymentType").val()){json+='"receiveType":"'+$("#receiveType").val()+'",';}
	if($("#paymentCardNo").val()){json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';}//paymentCardNo
	if($("#receiveCardNo").val()){json+='"receiveCardNo":"'+$("#receiveCardNo").val()+'",';}
	if($("#remark").val()){json+='"remark":"'+$("#remark").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#verifierDateStart").val()){json+='"verifierDateStart":"'+$("#verifierDateStart").val()+'",';}
	if($("#verifierDateEnd").val()){json+='"verifierDateEnd":"'+$("#verifierDateEnd").val()+'",';}
	if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
	if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,paymentVo) {
	      		htmls +='<tr>'
	      					+'<td>'+ (n+1) +'</td>'
							+'<td>'+ paymentVo.paymentNo +'</td>'
							+'<td>'+ paymentVo.receiveAccountName +'</td>'
							+'<td>'+ ($.formatMoney((paymentVo.paymentAmount?paymentVo.paymentAmount:"0"), 2))+'</td>'
							+'<td>'+ (paymentVo.paymentAccount?paymentVo.paymentAccount:"--")+'</td>'
							+'<td>'+ (paymentVo.status==1?"草稿":paymentVo.status==2?"待审":paymentVo.status==3?"已审":"注销") +'</td>'
							+'<td>'+ paymentVo.createPerson +'</td>'
							+'<td>'+ paymentVo.careteTime +'</td>'
							+'<td>'+ (paymentVo.verifierPerson?paymentVo.verifierPerson:"") +'</td>'
							+'<td>'+ (paymentVo.verifierDate? paymentVo.verifierDate:"")+'</td>'
							+'<td>'+ (paymentVo.remark?paymentVo.remark:"") +'</td>'
							
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_DETAIL"]){
								var _url="payment-detail.html?id="+paymentVo.id +"&supperResId="+ request('supperResId');
								htmls +='<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看付款单\')">查看</a>';
							}
							//验证是否有编辑权限
							/*if(paymentVo.status==1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_EDIT"]){
								var _url="payment-modity.html?id="+paymentVo.id +"&supperResId="+ request('supperResId');
								htmls +='  <a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'编辑付款单\')">编辑</a>';
							}*/
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#paymentNo").val()){json+='"paymentNo":"'+$("#paymentNo").val()+'",';}
	if($("#payment-account").val()){json+='"receiveAccountNo":"'+$("#receiveAccountNo").val()+'",';}
	if($("#paymentAmountStart").val()){json+='"paymentAmountStart":"'+$("#paymentAmountStart").val()+'",';}
	if($("#paymentAmountEnd").val()){json+='"paymentAmountEnd":"'+$("#paymentAmountEnd").val()+'",';}
	if($("#paymentType").val()){json+='"receiveType":"'+$("#receiveType").val()+'",';}
	if($("#paymentCardNo").val()){json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';}
	if($("#receiveCardNo").val()){json+='"receiveCardNo":"'+$("#receiveCardNo").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
	if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "payment/payment/exportexcelpayment.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看付款单详情
function paymentDetail(){
	loading("正在提交查看付款单详情信息");
	var url = "payment/payment/getpaymentbillsbyid.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			for (var key in data.obj) {  
				if(key == "paymentVo"){ 
					$("#id").val(data.obj[key].id);
					$("#versionLock").val(data.obj[key].versionLock);
					
					//基本信息
					$("#paymentNo").html("&nbsp;&nbsp"+ data.obj[key].paymentNo);
					$("#paidMoney").html("&nbsp;&nbsp;"+(data.obj[key].originalOriginal?data.obj[key].originalOriginal:"0"));
					$("#receiveAccountName").html("&nbsp;&nbsp;"+ data.obj[key].receiveAccountName);
					$("#paymentAmount").html("&nbsp;&nbsp;"+ data.obj[key].paymentAmount);
					$("#totalAmount").html("&nbsp;&nbsp;"+ ((data.obj[key].paymentAmount+data.obj[key].originalOriginal)?(data.obj[key].paymentAmount+data.obj[key].originalOriginal):""));
					$("#settlementCount").html("&nbsp;&nbsp;"+ data.obj[key].settlementCount);
					$("#paymentCardNo").html("&nbsp;&nbsp;"+ data.obj[key].paymentAccount);
					$("#remainAmount").html("&nbsp;&nbsp;"+ (data.obj[key].remainAmount?data.obj[key].remainAmount:""));
					
					
					//单据创建信息
					$("#remark").html("&nbsp;&nbsp;"+ (data.obj[key].remark?data.obj[key].remark:""));
					$("#createTime").html("&nbsp;&nbsp;"+ data.obj[key].careteTime);
					$("#status").html("&nbsp;&nbsp;"+ (data.obj[key].status==1?"草稿":data.obj[key].status==2?"待审":data.obj[key].status==3?"已审":"注销"));
					$("#createPerson").html("&nbsp;&nbsp;"+ data.obj[key].createPerson);
					$("#verifierDate").html("&nbsp;&nbsp;"+ (data.obj[key].verifierDate?data.obj[key].verifierDate:""));
					$("#updateTime").html("&nbsp;&nbsp;"+ data.obj[key].updateTime);
					$("#verifierPerson").html("&nbsp;&nbsp;"+ (data.obj[key].verifierPerson?data.obj[key].verifierPerson:""));
					$("#updatePerson").html("&nbsp;&nbsp;"+ data.obj[key].updatePerson);
					
					var pageButtonMap = data.obj["pageButtonMap"];
					//验证是否有提交权限
					if(data.obj[key].status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_SUBMIT"]){
						$("#add").append('<input type="button" class="btn btn-default" onclick="submitAudit(2)" value="提交审核" />&nbsp;');
					}
					//验证是否有审核权限
					if(data.obj[key].status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_VERIFI"]){
					  	$("#add").append('<input type="button" class="btn btn-default" onclick="auditPass(3)" value="审核通过" />&nbsp;<input type="button" class="btn btn-default" onclick="auditFailure(1)" value="审核失败" />');
					}
					//验证是否有注销权限
					if(data.obj[key].status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_CANCEL"]){
						$("#add").append('<input type="button" type="button" class="btn btn-default pull-right" onclick="cancelBills(4)" value="注销单据" />');
					}
		        }else if(key == "paymentShouldVos"){  
		        	var htmls = '';
		        	var amountSum = 0;
		          	$.each(data.obj[key],function(n,paymentShouldVo) {
		          		var secondValue = $.trim(paymentShouldVo.billCode);
	            		var test1= secondValue.substring(0,2);
	            		var test = test1=="BO"?"b2b订单":test1=="JO"?"加盟商订单":
	            				   test1=="SO"?"档口订单":test1=="PO"?"采购单":
	            				   test1=="NL"?"需求单": test1=="PR"?"采购退货单":"无名单";
		          		htmls += '<tr>'+
							'<td>&nbsp;&nbsp;'+ test +'</td>'+
							'<td>&nbsp;&nbsp;'+ (paymentShouldVo.paymentShouldName?paymentShouldVo.paymentShouldName:"") +'</td>'+
							'<td>&nbsp;&nbsp;'+ (paymentShouldVo.paymentShouldAmount?paymentShouldVo.paymentShouldAmount:"") +'</td>'+
							'<td>&nbsp;&nbsp;'+ (paymentShouldVo.settlementStatus==1?"未核销":"已核销") +'</td>'+
							'</tr>';
						amountSum += paymentShouldVo.paymentShouldAmount;
		          	});
		          	$("#dataList1").html(htmls);
		          	$("#num").html("&nbsp;&nbsp;"+ data.obj[key].length);
		          	$("#amountSum").html("&nbsp;&nbsp;"+ amountSum);
		        }
	        }		
		}else{
			layer.alert(data.msg);
		}
	});
}



//更新付款单状态(草稿-->注销)
function cancelBills(status){
	loading("正在提交注销付款单信息");
	var url = "payment/payment/cancelpaymentbillsbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	if($("#cancel").val()){
		json+='"cancellationReason":"'+$("#cancel").val()+'",';
	}else if(status == 4){
//		layer.alert("请录入注销原因");
//		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//更新付款单状态(草稿-->待审)
function submitAudit(status){
	loading("正在提交待审付款单信息");
	var url = "payment/payment/checkpaymentbillsbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//更新付款单状态(待审-->已审)
function auditPass(status){
	loading("正在提交已审付款单信息");
	var url = "payment/payment/auditpaymentshouldbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}



//更新付款单状态(待审-->草稿)
function auditFailure(status){
	loading("正在提交草稿付款单信息");
	var url = "payment/payment/failurepaymentbillsbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//添加页面加载数据
function addPaymentLoad(){
	loading("正在提交加载添加付款单信息");
	var url = "payment/payment/topaymentbills.ihtml";
	RequestData(url,"{}",function(data){
		unloading();
		if(data.code == "0") {  
			if(data.obj["myCardVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["myCardVos"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'" property="'+ card.cardBank +'">'+ card.cardNickname+card.cardBank+card.cardOpenNumber +'</option>';
	          	});
	            $("#paymentCardNo").html(htmls);
			}
			if(data.obj["paymentShouldVos"]){
				var htmls = '';
	            $.each(data.obj["paymentShouldVos"],function(n,paymentShould) {
	            	var secondValue = $.trim(paymentShould.billCode);
	            	var	 test2= secondValue.substring(0,2);
	            		var test = test2=="BO"?"b2b订单":test2=="JO"?"加盟商订单":
	            				   test2=="SO"?"档口订单":test2=="PO"?"采购单":
	            				   test2=="NL"?"需求单": test2=="PR"?"采购退货单":"无名单";
//	            	var test = paymentShould.sourceBillsType==1?"其他单":paymentShould.sourceBillsType ==2?"档口订单":
//	            			  paymentShould.sourceBillsType ==3?"采购退货单":paymentShould.sourceBillsType ==5?"调拨单":
//	            			  paymentShould.sourceBillsType ==6?"盘点差异单":paymentShould.sourceBillsType ==7?"出库单":
//	            			  paymentShould.sourceBillsType ==8?"采购订单":paymentShould.sourceBillsType ==9?"加盟商订单":"无名单";
	          		htmls +='<tr><td><label class="checkbox" for=""><input type="checkbox" name="paymentShoulds" value="'+ paymentShould.id +'" onclick="sumAmount('+ paymentShould.paymentShouldAmount +')" /></label></td><td>'+ paymentShould.paymentShouldNo +'</td><td>'+(paymentShould.paymentShouldName?paymentShould.paymentShouldName:"") +'</td><td>'+ paymentShould.paymentShouldAmount +'</td><td>'+test+'</td></tr>';
	          	});
	            $("#dataList").append(htmls);
			}
    	}	
	});
    $("#save_payment").click(function(){addPayment();});
}

//添加付款单
function addPayment(){
	if(!$("#payment_add_form").validationEngine('validate')){
		return;	
	}
	loading("正在提交添加付款单信息");
	var url = "payment/payment/savepaymentbills.ihtml";
	var json ='{';
	json+='"receiveAccountNo":"'+$("#accountNo").val()+'",';
	json+='"receiveAccountName":"'+$("#accountName").val()+'",';
	json+='"paymentAmount":"'+$("#paymentAmount").val()+'",';
	json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';
	json+='"paymentBank":"'+$("#paymentCardNo").find('option:selected').attr("property")+'",';
	json+='"paymentAccount":"'+$("#paymentCardNo").find('option:selected').text()+'",';
	json+='"originalOriginal":"'+$("#paidMoney").html()+'",';
	json+='"settlementCount":"'+$("#settlementCount").html()+'",';
	json+='"remainAmount":"'+$("#remainAmount").html()+'",';//剩余已付   一为空就报错在后台
	var paymentShouldIds = "";
  	$('input[name="paymentShoulds"]:checked').each(function(i,checkbox){
  		if(i>0){
  			paymentShouldIds+="#";
  		}
  		paymentShouldIds += $(checkbox).val();
  	});
	json+='"paymentShouldIds":"'+paymentShouldIds+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editPaymentLoad(){
	loading("正在提交加载编辑付款单信息");
	var url = "payment/payment/toupdatepaymentbillsbyid.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0") { 
			if(data.obj["myCardVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["myCardVos"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'" property="'+ card.cardBank +'">'+ card.cardNickname +'</option>';
	          	});
	            $("#paymentCardNo").html(htmls);
			}
			if(data.obj["cardVos"]){ 
				var htmls = '';
	            $.each(data.obj["cardVos"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'" property="'+ card.cardBank +'">'+ card.cardNickname +'</option>';
	          	});
	            $("#receiveCardNo").html(htmls);
	        }
			if(data.obj["accountVoOld"]){
	        	$("#originalOriginal").html(data.obj["accountVoOld"].accountAmount);
	        }
			if(data.obj["paymentVo"]){
				var paymentVo = data.obj["paymentVo"];
				$("#id").val(paymentVo.id);
				$("#versionLock").val(paymentVo.versionLock);
				$("#accountNo").val(paymentVo.receiveAccountNo);
				$("#accountName").val(paymentVo.receiveAccountName);
				$("#paymentType").val(paymentVo.receiveType);
				$("#paymentAmount").val(paymentVo.paymentAmount);
				$("#receiveCardNo").val(paymentVo.receiveCardNo);
				$("#paymentCardNo").val(paymentVo.paymentCardNo);
				$("#paidMoney").html(paymentVo.originalOriginal);
				$("#settlementCount").html(paymentVo.settlementCount);
				$("#remainAmount").html(paymentVo.remainAmount);
				$("#remark").val(paymentVo.remark);
				blurAmount();
			}
			alert(data.obj["map"].length)
        }	
	});
    $("#edit_payment").click(function(){editPayment();});
}

//编辑付款单
function editPayment(){
	loading("正在提交编辑付款单信息");
	var url = "payment/payment/updatepaymentbillsbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"receiveAccountNo":"'+$("#accountNo").val()+'",';
	json+='"receiveAccountName":"'+$("#accountName").val()+'",';
	json+='"paymentAmount":"'+$("#paymentAmount").val()+'",';
	json+='"receiveCardNo":"'+$("#receiveCardNo").val()+'",';
	json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';
	json+='"paymentBank":"'+$("#paymentCardNo").find('option:selected').attr("property")+'",';
	json+='"paymentAccount":"'+$("#paymentCardNo").find('option:selected').text()+'",';
    json+='"paidMoney":"'+$("#paidMoney").html()+'",';
	json+='"settlementCount":"'+$("#settlementCount").html()+'",';
	json+='"remainAmount":"'+$("#remainAmount").html()+'",';
	var paymentShouldIds = "";
  	$('input[name="paymentShoulds"]:checked').each(function(i,checkbox){
  		if(i>0){
  			paymentShouldIds+="#";
  		}
  		paymentShouldIds += $(checkbox).val();
  	});
	json+='"paymentShouldIds":"'+paymentShouldIds+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount,ownerAccountAmount){
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
	$("#paidMoney").html(ownerAccountAmount);
	//accountByCard();
	remainAmount();
	loadPaymentShould(accountNo);
}

function loadPaymentShould(accountNo){
	var url="/paymentshould/paymentshould/queryPaymentShould.ihtml?accountNo="+accountNo;
	loading("正在加载未核销的应付单");
	RequestData(url,"",function(data){
		unloading();
		if(data.obj){
			var htmls = '';
            $.each(data.obj,function(n,paymentShould) {
            	var secondValue = $.trim(paymentShould.billCode);
            		var test1= secondValue.substring(0,2);
            		var test = test1=="BO"?"b2b订单":test1=="JO"?"加盟商订单":
            				   test1=="SO"?"档口订单":test1=="PO"?"采购单":
            				   test1=="NL"?"需求单": test1=="PR"?"采购退货单":"无名单";
          		if(data.obj["map"] && data.obj["map"][paymentShould.paymentShouldNo]){
	          		htmls +='<tr><td><label class="checkbox" for=""><input type="checkbox" name="paymentShoulds" checked="checked" value="'+ paymentShould.id +'" onclick="sumAmount('+ paymentShould.paymentShouldAmount +')" /></label></td><td>'+ paymentShould.paymentShouldNo +'</td><td>'+(paymentShould.paymentShouldName?paymentShould.paymentShouldName:"") +'</td><td>'+ paymentShould.paymentShouldAmount +'</td><td>'+ test +'</td></tr>';
            	}else{
	          		htmls +='<tr><td><label class="checkbox" for=""><input type="checkbox" name="paymentShoulds" value="'+ paymentShould.id +'" onclick="sumAmount('+ paymentShould.paymentShouldAmount +')" /></label></td><td>'+ paymentShould.paymentShouldNo +'</td><td>'+(paymentShould.paymentShouldName?paymentShould.paymentShouldName:"") +'</td><td>'+ paymentShould.paymentShouldAmount +'</td><td>'+ test +'</td></tr>';
            	}
          	});
            $("#dataList").html(htmls);
		}
	});
}

//金额转化大小写
function blurAmount(){
	if($("#paymentAmount").val() && !(isNaN($("#paymentAmount").val()))){
		$("#bigAmount").html(convertCurrency($("#paymentAmount").val()));
		sumAmount();
	}
}

////根据账户查询相关的帐号
//function accountByCard(){
//	var url = "payment/payment/getcardnobypaymentbills.ihtml";
//	var json ='{"id":"'+ $("#accountNo").val() +'"}';
//	RequestData(url,json,function(data){
//		if(data.code == "0"){
//			if(data.obj["cardVos"]){ 
//				var htmls = '';
//	            $.each(data.obj["cardVos"],function(n,card) {
//	          		htmls +='<option value="'+ card.cardNo +'" property="'+ card.cardBank +'">'+ card.cardNickname +'</option>';
//	          	});
//	            $("#receiveCardNo").html(htmls);
//	        }
//			if(data.obj["accountVoOld"]){
//	        	$("#originalOriginal").html(data.obj["accountVoOld"].accountAmount);
//	        }
//		}else{
//			layer.alert(data.msg);
//		}
//	});
//}

//核销小计
function sumAmount(){
	var sumAmount = 0;
  	$('input[name="paymentShoulds"]:checked').each(function(i,checkbox){  
  		sumAmount += parseFloat($(checkbox.parentNode.parentNode).next().next().next().html());
  	});
  	$("#settlementCount").html(sumAmount);
  	remainAmount();
}

//计算剩余已付
function remainAmount(){
	var amount = 0;
  	var ownerAccountAmount = $("#paidMoney").html();
  	var paymentAmount = $("#paymentAmount").val();
  	var settlementCount = $("#settlementCount").html();
  	var remainAmount1 = parseFloat(ownerAccountAmount) + parseFloat(paymentAmount) - parseFloat(settlementCount);
  	var remainAmount = new Number(remainAmount1).toFixed(2);
  	$("#remainAmount").html(remainAmount>0?"+"+remainAmount:remainAmount);
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}