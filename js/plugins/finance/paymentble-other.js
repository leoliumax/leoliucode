﻿//应付单列表页初始化数据
function queryReceiveShouldListLoadOther(){
	var url = "paymentshould/paymentshouldOther/querypaymentshouldLoadOther.ihtml";
	RequestData(url,'{}',function(data){
		for (var key in data.obj) {  
	        if(key == "otherPaymentTypeVos"){//otherPaymentType
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,paymentTypeVo) {
	          		htmls +='<option value="'+ paymentTypeVo.id +'">'+ paymentTypeVo.typeName +'</option>';
	          	});
	          	$("#paymentShouldType").html(htmls);
	        }
        }
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_ADD"]){
			var _url="paymentble-other-add.html?supperResId="+ request('supperResId');
			$("#add").html('<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'添加其他应付单\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
			//$("#add").html('<a href="paymentble-other-add.html?supperResId='+ supperResId +'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		
		$("#search").click();
	});
	
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "paymentshould/paymentshouldOther/querypaymentshouldpageOther.ihtml";
	var json ='{';
		if($("#paymentShouldNo").val()){json+='"paymentShouldNo":"'+$("#paymentShouldNo").val()+'",';}
		if($("#receiveAccountNo").val()){json+='"receiveAccountName":"'+$("#receiveAccountNo").val()+'",';}
		if($("#amountStart").val()){json+='"amountStart":"'+$("#amountStart").val()+'",';}
		if($("#amountEnd").val()){json+='"amountEnd":"'+$("#amountEnd").val()+'",';}
		if($("#paymentShouldType").val()){json+='"paymentShouldType":"'+$("#paymentShouldType").val()+'",';}
		if($("#childType").val()){json+='"paymentShouldChildName":"'+$("#childType").val()+'",';}
		if($("#remark").val()){json+='"remark":"'+$("#remark").val()+'",';}
		if($("#status").val()){json+='"status":"'+$("#status").val()+'",';}
		if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}//createPerson
		if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
		if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
		if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
		if($("#verifierDateStart").val()){json+='"verifierDateStart":"'+$("#verifierDateStart").val()+'",';}
		if($("#verifierDateEnd").val()){json+='"verifierDateEnd":"'+$("#verifierDateEnd").val()+'",';}
		if($("#settlementStatus").val()){json+='"settlementStatus":"'+$("#settlementStatus").val()+'",';}
		if($("#settlementDateStart").val()){json+='"settlementDateStart":"'+$("#settlementDateStart").val()+'",';}
		if($("#settlementDateEnd").val()){json+='"settlementDateEnd":"'+$("#settlementDateEnd").val()+'",';}
		if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
		json +='"pageSize":'+ pageSize +'}';
		RequestData(url,json,function(data){	
			$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
			var htmls = '';
			if(data.obj.rows){
				$.each(data.obj.rows,function(n,receiveShouldVo) {
		      		htmls +='<tr>'
		      					+'<td>'+(n+1)+'</td>'
								+'<td>'+ receiveShouldVo.paymentShouldNo +'</td>'
								+'<td>'+ (receiveShouldVo.receiveAccountName?receiveShouldVo.receiveAccountName:"") +'</td>'
								+'<td>'+ (receiveShouldVo.paymentShouldAmount?receiveShouldVo.paymentShouldAmount:"") +'</td>'
								+'<td>'+ (receiveShouldVo.accountAmount?receiveShouldVo.accountAmount:"") +'</td>'
								+'<td>'+ (receiveShouldVo.paymentShouldName?receiveShouldVo.paymentShouldName:"") +'</td>'
								+'<td>'+ (receiveShouldVo.remark?receiveShouldVo.remark:"") +'</td>'
								+'<td>'+ (receiveShouldVo.status==1?"草稿":receiveShouldVo.status==2?"待审":receiveShouldVo.status==3?"已审":"注销") +'</td>'
								+'<td>'+ receiveShouldVo.createDate +'</td>'
								+'<td>'+ receiveShouldVo.createPerson +'</td>'
								+'<td>'+ (receiveShouldVo.verifierDate?receiveShouldVo.verifierDate:"") +'</td>'
								+'<td>'+ (receiveShouldVo.verifierPerson?receiveShouldVo.verifierPerson:"") +'</td>'
								+'<td>'+ (receiveShouldVo.settlementStatus==1?"未核销":"已核销") +'</td>'
								+'<td>'+ (receiveShouldVo.settlementDate?receiveShouldVo.settlementDate:"") +'</td>'
								+'<td>';
								//验证是否有查看权限
								if(pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_DETAIL"]){
									var _url="paymentble-other-detail.html?id="+receiveShouldVo.id +"&supperResId="+ request('supperResId');
									htmls +='  <a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看其他应付单信息\')">查看</a>';
									//htmls +='<a href="paymentble-other-detail.html?id='+ receiveShouldVo.id +'&supperResId='+ supperResId +'">查看</a>&nbsp;&nbsp;';
								}
								//验证是否有编辑权限
								if(receiveShouldVo.status==1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_EDIT"]){
									var _url="paymentble-other-modity.html?id="+receiveShouldVo.id +"&supperResId="+ request('supperResId');
									htmls +='  <a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'编辑其他查看应付单信息\')">编辑</a>';
								}
						htmls +='</td></tr>';
		      });
		    }
			$("#dataList").html(htmls);
			//加载分页模块
			bindPageEvent(data.obj.pageNo,data.obj.page);
			unloading();
		})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	json +='"pageNo":1}';
	var url = getIP() + "paymentshould/paymentshouldOther/exportexcelpaymentshouldOther.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看其他应付单详情
function paymentbleDetailOther(){
	loading("正在提交查看其他应付单详情信息");
	var url = "paymentshould/paymentshouldOther/getpaymentshouldbyidOther.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			var paymentShouldVo = data.obj["paymentShouldVo"];
			var accountVo = data.obj["accountVo"];
			$("#versionLock").val(paymentShouldVo.versionLock);
			$("#id").val(paymentShouldVo.id);
			$("#organId").val(paymentShouldVo.organId);
			
			//基本信息
			$("#paymentShouldNo").html("&nbsp;&nbsp;"+ paymentShouldVo.paymentShouldNo);
			$("#settlementStatus").html("&nbsp;&nbsp;"+ (paymentShouldVo.settlementStatus==1?"未核销":"已核销"));
			$("#paymentShouldName").html("&nbsp;&nbsp;"+ (paymentShouldVo.paymentShouldName?paymentShouldVo.paymentShouldName:""));
			$("#settlementPerson").html("&nbsp;&nbsp;"+ (paymentShouldVo.settlementPerson?paymentShouldVo.settlementPerson:""));
			$("#paymentAccountName").html("&nbsp;&nbsp;"+ (paymentShouldVo.receiveAccountName?paymentShouldVo.receiveAccountName:""));
			$("#settlementDate").html("&nbsp;&nbsp;"+ (paymentShouldVo.settlementDate?paymentShouldVo.settlementDate:""));
			$("#receiveShouldAmount").html("&nbsp;&nbsp;"+ (paymentShouldVo.paymentShouldAmount?paymentShouldVo.paymentShouldAmount:""));
			$("#paymentCode").html("&nbsp;&nbsp;"+ (paymentShouldVo.paymentCode?paymentShouldVo.paymentCode:""));
			$("#paymentAmountBig").html("&nbsp;&nbsp;"+ convertCurrency(""+paymentShouldVo.paymentShouldAmount));
			$("#ownerAccountAmount").html("&nbsp;&nbsp;"+ (accountVo.ownerAccountAmount?accountVo.ownerAccountAmount:""));
			
			//单据创建
			$("#remark").html("&nbsp;&nbsp;"+ (paymentShouldVo.remark?paymentShouldVo.remark:""));
			$("#status").html("&nbsp;&nbsp;"+ (paymentShouldVo.status==1?"草稿":paymentShouldVo.status==2?"待审":paymentShouldVo.status==3?"已审":"注销"));
			$("#createTime").html("&nbsp;&nbsp;"+ paymentShouldVo.createDate);
			$("#createPerson").html("&nbsp;&nbsp;"+ paymentShouldVo.createPerson);
			$("#verifierDate").html("&nbsp;&nbsp;"+ (paymentShouldVo.verifierDate?paymentShouldVo.verifierDate:""));
			$("#updateDate").html("&nbsp;&nbsp;"+ paymentShouldVo.updateDate);
			$("#verifierPerson").html("&nbsp;&nbsp;"+ (paymentShouldVo.verifierPerson?paymentShouldVo.verifierPerson:""));
			$("#updatePerson").html("&nbsp;&nbsp;"+ paymentShouldVo.updatePerson);
			
			var pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有提交权限
			if(paymentShouldVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_SUBMIT"]){
				$("#add").append('<button class="btn btn-default" onclick="checkPendingByIdOther(2)">提交审核</button>&nbsp;');
			}
			//验证是否有审核权限
			if(paymentShouldVo.status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_VERIFI"]){
				$("#add").append('<button class="btn btn-default" onclick="auditPaymentShouldByIdOther(3)">审核通过</button>&nbsp;<button class="btn btn-default" onclick="failureCheckPendingByIdOther(1)">审核失败</button>');
			}
			//验证是否有注销权限
			if(paymentShouldVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_CANCEL"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="cancelPaymentShouldByIdOther(4)">注销单据</button>');
			}
			//验证是否有核销权限  
			if(paymentShouldVo.settlementStatus == 1 && paymentShouldVo.status == 3 && pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_PAYMENT_SHOULD_ABOLISH"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="verificationPaymentShouldByIdOther(3)">核销</button>');
			}
		}else{
			layer.alert(data.msg);
		}
	});
}


//添加页面加载数据
function addPaymentbleLoadOther(){
	loading("正在提交加载添加其他应付单信息");
	var url = "paymentshould/paymentshouldOther/topaymentshouldOther.ihtml";

	RequestData(url,"{}",function(data){
		
//		if(data.code == "0" && data.obj["recdiveTypes"]) {  
//			var htmls = '<option value="">请选择</option>';
//          $.each(data.obj["recdiveTypes"],function(n,recdiveType) {
//        		htmls +='<option value="'+ recdiveType.id +'">'+ recdiveType.receiveTypeName +'</option>';
//        	});
//          $("#receiveShouldTypeNo").html(htmls);
//  	}
		unloading();
	});
	//添加验证
    validatData(1);
}

//添加其他应收单
function addPaymentbleOther(){
	loading("正在提交添加其他应付单信息");
	var url = "paymentshould/paymentshouldOther/savepaymentshouldOther.ihtml";
	var json ='{';
	json+='"receiveAccountNo":"'+$("#accountNo").val()+'",';
	json+='"receiveAccountName":"'+$("#accountName").val()+'",';
	json+='"paymentShouldType":"'+$("#otherPaymentById").val()+'",';  
	json+='"paymentShouldName":"'+$("#typeName").val()+'",';
	json+='"paymentShouldChildName":"'+$("#childType").val()+'",';
	json+='"paymentShouldAmount":"'+$("#receiveShouldAmount").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}

	});
}

//编辑页面加载数据
function editPaymentbleLoadOther(){
	loading("正在提交加载编辑页面数据信息");
	var url = "paymentshould/paymentshouldOther/toupdatepaymentshouldbyidOther.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0") { 
			if(data.obj["paymentShouldVo"]){  
				var paymentShouldVo = data.obj["paymentShouldVo"];
				$("#id").val(paymentShouldVo.id);
				$("#versionLock").val(paymentShouldVo.versionLock);
				$("#accountNo").val(paymentShouldVo.receiveAccountNo);
				$("#accountName").val(paymentShouldVo.receiveAccountName);
				
				$("#otherPaymentById").val(paymentShouldVo.paymentShouldType);   
				$("#typeName").val(paymentShouldVo.paymentShouldName);
				$("#receiveShouldAmount").val(paymentShouldVo.paymentShouldAmount);
				blurAmount();
				$("#remark").val(paymentShouldVo.remark);
	    	}	
        }	
	});
	//添加验证
    validatData(2);
}

//编辑其他应收单
function editPaymentbleOther(){
	loading("正在提交编辑其他应付单信息");
	var url = "paymentshould/paymentshouldOther/updatepaymentshouldbyidOther.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"receiveAccountNo":"'+$("#accountNo").val()+'",';
	json+='"receiveAccountName":"'+$("#accountName").val()+'",';
	json+='"paymentShouldType":"'+$("#otherPaymentById").val()+'",';  
	json+='"paymentShouldName":"'+$("#typeName").val()+'",';
	json+='"paymentShouldChildName":"'+$("#childType").val()+'",';
	json+='"paymentShouldAmount":"'+$("#receiveShouldAmount").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//添加验证
function validatData(type){
    var validate = $("#expen-from").validate({
        rules:{
            accountName:{required:true},
            receiveShouldAmount:{required:true,number:true},
        },
        submitHandler: function(form){
        	if(!$("#otherPaymentById").val()){
        		layer.alert("请选择应付类型");
        		return;
        	}
        	if(type == 1){
				addPaymentbleOther();
        	}else{
				editPaymentbleOther();
        	}
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });      
}

//更新其他应收单状态(草稿-->待审)
function checkPendingByIdOther(status){
	loading("正在提交审核操作");
	var url = "paymentshould/paymentshouldOther/checkpendingbyidOther.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//更新其他应收单状态(待审-->已审)
function auditPaymentShouldByIdOther(status){
	loading("正在提交审核通过操作");
	var url = "paymentshould/paymentshouldOther/auditpaymentshouldbyidOther.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}



//更新其他应收单状态(待审-->草稿)
function failureCheckPendingByIdOther(status){
	loading("正在提审核失败操作");
	var url = "paymentshould/paymentshouldOther/failurecheckpendingbyidOther.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//更新其他应收单状态(草稿-->注销)
function cancelPaymentShouldByIdOther(status){
	loading("正在提交注销操作");
	var url = "paymentshould/paymentshouldOther/cancelpaymentshouldbyidOther.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	if($("#cancel").val()){
		json+='"cancellationReason":"'+$("#cancel").val()+'",';
	}else if(status == 4){
//		layer.alert("请录入注销原因");
//		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//更新其他应收单状态(核销)
function verificationPaymentShouldByIdOther(status){
	loading("正在提交核销操作");
	var url = "paymentshould/paymentshouldOther/verificationpaymentshouldbyidOther.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"organId":"'+$("#organId").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount){
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
	var dt = new Date();
    var dt30 = new Date(dt.getTime()+parseInt(accountPeriod)*24*3600*1000);
	$("#endDate").val(dt30.toLocaleDateString().replace('/','-').replace('/','-'));
}


//添加应付类型
function addMarquePayment(typeName,id,status,supperTypeName){
	$("#typeName").val(typeName);
	$("#otherPaymentById").val(id);
	$("#childType").val(supperTypeName);
}

//金额转化大小写
function blurAmount(){
	if($("#receiveShouldAmount").val() && !(isNaN($("#receiveShouldAmount").val()))){
		$("#bigAmount").html(convertCurrency($("#receiveShouldAmount").val()));
	}
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}