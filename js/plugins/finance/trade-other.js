﻿//账户列表页初始化数据
function queryCardListLoad(){
	var url = "financial/card/pageOtherCardLoad.ihtml";
	RequestData(url,'{}',function(data){
        pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_CARD_OTHER_ADD"]){
			var _url="other-trade-add.html?supperResId="+ request('supperResId');
			$("#add").html('<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'新增其他交易账号\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_CARD_OTHER_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "financial/card/pageOtherCard.ihtml";
	var json ='{';
	if($("#tradeType").val()){json+='"tradeType":"'+$("#tradeType").val()+'",';}
	if($("#cardNickname").val()){json+='"cardNickname":"'+$("#cardNickname").val()+'",';}
	if($("#cardNo").val()){json+='"cardNo":"'+$("#cardNo").val()+'",';}
	if($("#cardOpenName").val()){json+='"cardOpenName":"'+$("#cardOpenName").val()+'",';}
	if($("#cardOpenBank").val()){json+='"cardOpenBank":"'+$("#cardOpenBank").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#startCreateTime").val()){json+='"startCreateTime":"'+$("#startCreateTime").val()+'",';}
	if($("#endCreateTime").val()){json+='"endCreateTime":"'+$("#endCreateTime").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,card) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ card.cardNo +'</td>'
							+'<td>'+ card.cardNickname +'</td>'
							+'<td>'+card.tradeType+'</td>'
							+'<td>'+ card.cardOpenBank +'</td>'
							+'<td>'+ card.cardOpenName +'</td>'
							+'<td>'+ card.createPerson +'</td>'
							+'<td>'+ card.createTime +'</td>'
							+'<td>'+ (card.remark?card.remark:"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_CARD_OTHER_DETAIL"]){
								var _url="other-trade-detail.html?id="+card.id +"&type="+card.type+"&supperResId="+ request('supperResId');
								htmls +='<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看账号信息\')">查看</a>';
							}
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_CARD_OTHER_EDIT"]){
								var _url="other-trade-modity.html?id="+card.id +"&type="+card.type+"&supperResId="+ request('supperResId');
								htmls +='  <a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'编辑账号信息\')">编辑</a>';
							}
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
$("#exportExcel").click(function(){
//	var json ='{';
//	json +='"pageNo":1}';
//	var url = getIP() + "financial/card/exportexcelpaymentshould.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
//	window.location.href = url;
})

//查看其他账号详情
function cardDetail(){
	loading("正在提交查看其他交易账号信息");
	var url = "financial/card/cardDetail.ihtml";
	var json ='{';
		json+='"type":"'+request("type")+'",';
		json+='"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			$("#cardNo").html("ID："+ data.obj.cardNo);
			$("#cardNickname").html("&nbsp;&nbsp;"+ data.obj.cardNickname);
			$("#tradeType").html("&nbsp;&nbsp;"+ data.obj.tradeType);
			$("#cardOpenBank").html("&nbsp;&nbsp;"+ data.obj.cardOpenBank );
			$("#cardOpenNumber").html("&nbsp;&nbsp;"+ data.obj.cardOpenNumber );
			$("#cardOpenName").html("&nbsp;&nbsp;"+ data.obj.cardOpenName );
			$("#cardNo").html("&nbsp;&nbsp;"+ data.obj.cardNo);
			$("#initialAmount").html("&nbsp;&nbsp;"+ data.obj.initialAmount);
			$("#remark").html("&nbsp;&nbsp;"+ (data.obj.remark?data.obj.remark:""));
			$("#createPerson").html("&nbsp;&nbsp;"+ data.obj.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+ data.obj.createTime);
			$("#updatePerson").html("&nbsp;&nbsp;"+ data.obj.updatePerson);
			$("#updateDate").html("&nbsp;&nbsp;"+ data.obj.updateTime);
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加其他账号
function addCard(){
	loading("正在提交添加其他交易账号信息");
	var url = "financial/card/addOtherCard.ihtml";
	var json ='{';
	json+='"accountId":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"cardNickname":"'+$("#cardNickname").val()+'",';
	json+='"tradeType":"'+$("#tradeType").val()+'",';
	json+='"cardOpenName":"'+$("#cardOpenName").val()+'",';
	json+='"cardBank":"'+$("#cardBank").val()+'",';
	json+='"cardOpenBank":"'+$("#cardOpenBank").val()+'",';
	json+='"cardOpenNumber":"'+$("#cardOpenNumber").val()+'",';
	json+='"cardNo":"'+$("#cardNo").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editCardLoad(){
	loading("正在提交加载编辑其他交易账号信息");
	var url = "financial/card/cardDetail.ihtml";
	var json ='{';
		json+='"type":"'+request("type")+'",';
		json+='"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0" && data.obj) { 
			var card = data.obj;
			$("#id").val(card.id);
			$("#versionLock").val(card.versionLock);
			$("#type").val(card.type);
			$("#accountNo").val(card.accountId); 
			$("#accountName").val(card.accountName);
			$("#cardNickname").val(card.cardNickname);
			$("#tradeType").val(card.tradeType);
			$("#cardOpenName").val(card.cardOpenName);
			$("#cardBank").val(card.cardBank);
			$("#cardOpenBank").val(card.cardOpenBank);
			$("#cardOpenNumber").val(card.cardOpenNumber);
			$("#cardNo").val(card.cardNo);
			$("#cardNo2").val(card.cardNo);
			$("#remark").val(card.remark);
    	}	
	});
	//添加验证
    validatData(2);
}


//编辑账号
function editCard(){
	loading("正在提交编辑其他交易账号信息");
	var url = "financial/card/updateCard.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"type":"'+$("#type").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"accountId":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"cardNickname":"'+$("#cardNickname").val()+'",';
	json+='"tradeType":"'+$("#tradeType").val()+'",';
	json+='"cardOpenName":"'+$("#cardOpenName").val()+'",';
	json+='"cardBank":"'+$("#cardBank").val()+'",';
	json+='"cardOpenBank":"'+$("#cardOpenBank").val()+'",';
	json+='"cardOpenNumber":"'+$("#cardOpenNumber").val()+'",';
	json+='"cardNo":"'+$("#cardNo").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加验证
function validatData(type){
    var validate = $("#expen-from").validate({
        rules:{
            accountName:{required:true},
            cardNickname:{required:true},
            cardOpenName:{required:true},
            cardOpenBank:{required:true},
            cardOpenNumber:{required:true,number:true},
            cardNo:{required:true,number:true},
            cardNo2:{required:true,equalTo:cardNo}
        },
        submitHandler: function(form){
        	if(!$("#tradeType").val()){
        		layer.alert("请选择支付方式");
        		return;
        	}
        	if(!$("#cardBank").val()){
        		layer.alert("请选择银行");
        		return;
        	}
        	if(type == 1){
				addCard();
        	}else{
				editCard();
        	}
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });      
}

//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount){
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}
