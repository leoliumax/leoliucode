function loadpagesOtherPaymentTypeVo(){
	//加载列表数据
	var url = "financial/otherPaymentType/loadPagesOtherPaymentType.ihtml";
	RequestData(url,'{}',function(data){
		if(data.code == "0"){
			if(data.obj["pageButtonMap"]){
				pageButtonMap = data.obj["pageButtonMap"];
				//验证是否有添加权限
				if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_TYPE_ADD"]){
					var _url="other-payment-type-add.html?supperResId="+ request('supperResId');
					$("#add").html('<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'编辑其他应付类型\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
				}
				//验证是否有导出权限
				/*if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_TYPE_EXPORT"]){
					var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
					$("#excel").html(htmls);
				}*/
			}
		}else{
			layer.alert(data.msg);
		}
	});
	$("#pageNo").val(1);
	page();
}

function page(){
	loading("正在努力加载");
	var url = "financial/otherPaymentType/pageOtherPaymentType.ihtml";
	var json ='{';
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +',"inTypeGradeList":[2, 3]}';
	RequestData(url,json,function(datas){
		$($(".panel-title")[0]).html("共有"+ datas.obj.total+"条记录，每页"+ datas.obj.pageSize+"条记录");
		var htmls = '';
		if(datas.obj.rows){
			$.each(datas.obj.rows,function(n,otherPaymentTypeVo) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ otherPaymentTypeVo.typeNo +'</td>'
							+'<td>'+ otherPaymentTypeVo.typeName +'</td>'
							+'<td>'+ (otherPaymentTypeVo.remark?otherPaymentTypeVo.remark:"")+'</td>'							
							+'<td>'+ (otherPaymentTypeVo.status==1?"已启用":"已停用") +'</td>'
							+'<td>'+ (otherPaymentTypeVo.updateUserName?otherPaymentTypeVo.updateUserName:"") +'</td>'
							+'<td>'+ (otherPaymentTypeVo.updateDatetime==undefined?'':otherPaymentTypeVo.updateDatetime) +'</td>';
						//验证是否有编辑权限
						if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_TYPE_EDIT"]){
							var _url="other-payment-type-edit.html?id="+otherPaymentTypeVo.id +"&supperResId="+ request('supperResId');
							htmls +='<td><a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'编辑其他应付类型\')">编辑</a></td>';
						}
				htmls+='</tr>';
	      	});
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(datas.obj.pageNo,datas.obj.page);
		unloading();
	});
}


//编辑页面，加载类型数据
function queryOtherPaymentTypeById(){
	loading("加载编辑页面数据");
	var url = "financial/otherPaymentType/queryOtherPaymentTypeById.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			if(data.obj["otherPaymentTypeVo"]){
				var otherPaymentTypeVo = data.obj["otherPaymentTypeVo"];
				$("#id").val(otherPaymentTypeVo.id);
				$("#typeName").val(otherPaymentTypeVo.typeName);
				$("#supperTypeName").val(otherPaymentTypeVo.supperTypeName);
				$("#supperTypeId").val(otherPaymentTypeVo.supperTypeId);
				$("#status").get(0).index = ((otherPaymentTypeVo.status==1?"1":"2"));
				$("#remark").val((otherPaymentTypeVo.remark?otherPaymentTypeVo.remark:""));
			}
		}else{
			layer.alert(data.msg);
		}
	});
	$(function(){
        var validate = $("#paymentType_form").validate({
            rules:{
            },
            submitHandler: function(form){
				updateOtherPaymentType();
            }
        });      
    });
}
//编辑页面修改数据
function updateOtherPaymentType(){
	if(!validuateForm()){
		return;
	}
	loading("正在提交编辑其它应付类型信息");
	var url = "financial/otherPaymentType/updateOtherPaymentType.ihtml";
	var jdata = $("#paymentType_form").serializeObject();
	var json = JSON.stringify(jdata);
	RequestData(url, json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//新增页面保存按钮监听
$("#addSubmit").click(function(){
	$(function(){
        var validate = $("#paymentType_form").validate({
            rules:{
            },
            submitHandler: function(form){
				addOtherPaymentType();
            }
        });      
    });
	
});

//新增页面保存数据
function addOtherPaymentType(){
	if(!validuateForm()){
		return;
	}
	loading("正在提交添加其它应付类型信息");
	var url = "financial/otherPaymentType/addOtherPaymentType.ihtml";
	var jdata = $("#paymentType_form").serializeObject();
	jdata["supperTypeName"] = $("#supperTypeId").find("option :selected").text();
	var json = JSON.stringify(jdata);
	RequestData(url, json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面的父类型修改
function updateSupperType(id, typeName){
	var $supperTypeName = $("#supperTypeName").val();
	var i = 0;
	i = $supperTypeName.length;
	$("#supperTypeName").val(typeName);
	$("#supperTypeId").val(id);
	var $typeName = $("#typeName").val().substr(i);
	$("#typeName").val(typeName+$typeName);
}



//新增页面的父类型修改
function addSupperType(id, typeName){
	$("#supperTypeName").val(typeName);
	$("#supperTypeId").val(id);
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}

function validuateForm(){
	var typeName=$("#typeName").val();
	if(typeName==""){
		layer.tips("请输入类型名称","#typeName");
		return false;
	}
	var supid=$("#supperTypeId").val();
	if(supid=="0"){
		layer.tips("请选择父类型","#supperTypeId");
		return false;
	}
	return true;
}
