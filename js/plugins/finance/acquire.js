function pagereceiveTypeload(){
	//加载列表数据
	var url = "financial/receiveType/pagereceiveType.ihtml";
	RequestData(url,'{}',function(data){
		if(data.code == "0"){
			if(data.obj["pageButtonMap"]){
				pageButtonMap = data.obj["pageButtonMap"];
				//验证是否有添加权限
				if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_TYPE_ADD"]){
					var _url="other-acquire-add.html?supperResId="+ request('supperResId');
					$("#add").html('<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'编辑其他应收类型\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
				}
			}
		}else{
			layer.alert(data.msg);
		}
	});
	$("#pageNo").val(1);
	page();
}

function page(){
	loading("正在努力加载");
	var url = "financial/receiveType/pageReceiveType.ihtml";
	var json ='{';
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +',"inTypeGradeList":[2, 3]}';
	RequestData(url,json,function(data){
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,ReceiveTypeVo) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ ReceiveTypeVo.typeNo +'</td>'
							+'<td>'+ ReceiveTypeVo.typeName +'</td>'
							+'<td>'+ ReceiveTypeVo.remark+'</td>'							
							+'<td>'+ (ReceiveTypeVo.status==1?"已启用":"已停用") +'</td>'
							+'<td>'+ ReceiveTypeVo.updatePerson +'</td>'
							+'<td>'+ (ReceiveTypeVo.updateDate==undefined?'':ReceiveTypeVo.updateDate) +'</td>';
						//验证是否有编辑权限
						if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_TYPE_EDIT"]){
							var _url="other-acquire-edit.html?id="+ReceiveTypeVo.id +"&supperResId="+ request('supperResId');
							htmls +='<td><a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'编辑其他应收类型\')">编辑</a></td>';
						}
				htmls+='</tr>';
	      	});
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	});
}

//编辑页面，加载类型数据
function receiveTypeDetailload(){
	loading("正在提交加载编辑其他应收类型信息");
	var url = "financial/receiveType/receiveTypeDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			if(data.obj["receiveTypeVo"]){
				var receiveTypeVo = data.obj["receiveTypeVo"];
				$("#id").val(receiveTypeVo.id);
				$("#typeName").val(receiveTypeVo.typeName);
				$("#superTypeName").val(receiveTypeVo.superTypeName);
				$("#superId").val(receiveTypeVo.superId);
				$("#status").get(0).index = ((receiveTypeVo.status==1?"1":"2"));
				$("#remark").val((receiveTypeVo.remark?receiveTypeVo.remark:""));
			}
		}else{
			layer.alert(data.msg);
		}
	});
	$(function(){
        var validate = $("#receiveType_form").validate({
            rules:{
                typeName:{required:true},
                superTypeName:{required:true}
            },
            submitHandler: function(form){
				updatereceiveTypeload();
            }
        });      
    });
}
//编辑页面修改数据
function updatereceiveTypeload(){
	if(!validuateForm()){
		return;
	}
	loading("正在提交编辑其他应收类型信息");
	var url = "financial/receiveType/updateReceiveType.ihtml";
	var jdata = $("#receiveType_form").serializeObject();
	var json = JSON.stringify(jdata);
	RequestData(url, json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//新增页面保存按钮监听
$("#addSubmit").click(function(){
	$(function(){
        var validate = $("#receiveType_form").validate({
            rules:{
                typeName:{required:true},
                superTypeName:{required:true}
            },
            submitHandler:function(form){
				addreceiveType();
            }
        });      
    });
});


//编辑页面的父类型修改
function updateSuperType(id, typeName){
	var $superTypeName = $("#superTypeName").val();
	var i = 0;
	i = $superTypeName.length;
	$("#superTypeName").val(typeName);
	$("#superId").val(id);
	var $typeName = $("#typeName").val().substr(i);
	$("#typeName").val(typeName+$typeName);
}

//新增页面的父类型修改
function addSuperType(id, typeName){
	$("#superTypeName").val(typeName);
	$("#superId").val(id);
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}

function validuateForm(){
		var typeName=$("#typeName").val();
		if(typeName==""){
			layer.tips("请输入类型名称","#typeName");
			return false;
		}
		var supid=$("#superId").val();
		if(supid=="0"){
			layer.tips("请选择父类型","#superId");
			return false;
		}
		return true;
	}
