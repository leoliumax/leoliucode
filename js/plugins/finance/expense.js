﻿//账户列表页初始化数据
function queryExpensesListLoad(){
	var url = "financial/expenses/queryExpensesPageLoad.ihtml";
	RequestData(url,'{}',function(data){	
		if(data.obj["expensesUseVos"]){
			expensesUseVos = data.obj["expensesUseVos"];
			var htmls = '<option value="">请选择</option>';
            $.each(expensesUseVos,function(n,expensesUse) {
          		htmls +='<option value="'+ expensesUse.id +'">'+ expensesUse.useName +'</option>';
          	});
            $("#expensesUse").html(htmls);
		}
		if(data.obj["paymentTypeVos"]){
			var htmls = '<option value="">请选择</option>';
            $.each(data.obj["paymentTypeVos"],function(n,paymentType) {
          		htmls +='<option value="'+ paymentType.id +'">'+ paymentType.paymentTypeName +'</option>';
          	});
            $("#paymentType").html(htmls);
		}
		if(data.obj["myCard"]){
			var htmls = '<option value="">请选择</option>';
            $.each(data.obj["myCard"],function(n,card) {
          		htmls +='<option value="'+ card.cardNo +'">'+ card.cardNickname +'</option>';
          	});
            $("#paymentCardNo").html(htmls);
		}
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_ADD"]){
			$("#add").html('<a href="expense-add.html?supperResId='+ supperResId +'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	})
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "financial/expenses/queryExpensesPage.ihtml";
	var json ='{';
	if($("#expensesNo").val()){json+='"expensesNo":"'+$("#expensesNo").val()+'",';}
	if($("#accountName").val()){json+='"accountName":"'+$("#accountName").val()+'",';}
	if($("#startPaymentAmount").val()){json+='"startPaymentAmount":"'+$("#startPaymentAmount").val()+'",';}
	if($("#endPaymentAmount").val()){json+='"endPaymentAmount":"'+$("#endPaymentAmount").val()+'",';}
	if($("#expensesUse").val()){json+='"expensesUse":"'+$("#expensesUse").val()+'",';}
	if($("#paymentType").val()){json+='"paymentType":"'+$("#paymentType").val()+'",';}
	if($("#paymentCardNo").val()){json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#startCreateTime").val()){json+='"startCreateTime":"'+$("#startCreateTime").val()+'",';}
	if($("#endCreateTime").val()){json+='"endCreateTime":"'+$("#endCreateTime").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,expenses) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ expenses.expensesNo +'</td>'
							+'<td>'+ expenses.accountName +'</td>'
							+'<td>'+ (expenses.receiveBank?expenses.receiveBank:"") +'</td>'
							+'<td>'+ expenses.receiveCardNo +'</td>'
							+'<td>'+ expenses.paymentAmount +'</td>'
							+'<td>'+ expenses.expensesUse +'</td>'
							+'<td>'+ (expenses.paymentCardNo?expenses.paymentCardNo:"") +'</td>'
							+'<td>'+ expenses.createPerson +'</td>'
							+'<td>'+ (expenses.verifierPerson?expenses.verifierPerson:"") +'</td>'
							+'<td>'+ expenses.createTime +'</td>'
							+'<td>'+ (expenses.status==1?"草稿":expenses.status==2?"待审":expenses.status==3?"已审":"注销") +'</td>'
							+'<td>'+ (expenses.remark?expenses.remark:"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_DETAIL"]){
								htmls +='<a href="expense-detial.html?id='+ expenses.id +'&supperResId='+ supperResId +'">查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(expenses.status==1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_EDIT"]){
								htmls +='<a href="expense-modity.html?id='+ expenses.id +'&supperResId='+ supperResId +'">编辑</a>';
							}
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#expensesNo").val()){json+='"expensesNo":"'+$("#expensesNo").val()+'",';}
	if($("#accountName").val()){json+='"accountName":"'+$("#accountName").val()+'",';}
	if($("#startPaymentAmount").val()){json+='"startPaymentAmount":"'+$("#startPaymentAmount").val()+'",';}
	if($("#endPaymentAmount").val()){json+='"endPaymentAmount":"'+$("#endPaymentAmount").val()+'",';}
	if($("#expensesUse").val()){json+='"expensesUse":"'+$("#expensesUse").val()+'",';}
	if($("#paymentType").val()){json+='"paymentType":"'+$("#paymentType").val()+'",';}
	if($("#paymentCardNo").val()){json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#startCreateTime").val()){json+='"startCreateTime":"'+$("#startCreateTime").val()+'",';}
	if($("#endCreateTime").val()){json+='"endCreateTime":"'+$("#endCreateTime").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "financial/expenses/exportExcelExpenses.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看费用单详情
function expensesDetail(){
	loading("正在提交查看费用单详情信息");
	var url = "financial/expenses/queryExpensesInfo.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			var expensesVo = data.obj["expensesVo"];
			$("#expensesNo").html("ID："+ expensesVo.expensesNo);
			$("#accountName").html("&nbsp;&nbsp;"+ expensesVo.accountName);
			$("#receiveCardNo").html("&nbsp;&nbsp;"+ expensesVo.receiveCardNo);
			$("#paymentCardNo").html("&nbsp;&nbsp;"+ expensesVo.paymentCardNo);
			$("#paymentAmount").html("&nbsp;&nbsp;"+ expensesVo.paymentAmount);
			$("#paymentType").html("&nbsp;&nbsp;"+ expensesVo.paymentType);
			$("#paymentAmountBig").html("&nbsp;&nbsp;"+ convertCurrency(""+expensesVo.paymentAmount));
			$("#expensesUse").html("&nbsp;&nbsp;"+ expensesVo.expensesUse);
			$("#remark").html("&nbsp;&nbsp;"+ expensesVo.remark);
			$("#status").html("&nbsp;&nbsp;"+ (expensesVo.status==1?"草稿":expensesVo.status==2?"待审":expensesVo.status==3?"已审":"注销"));
			$("#createPerson").html("&nbsp;&nbsp;"+ expensesVo.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+ expensesVo.createTime);
			$("#verifierPerson").html("&nbsp;&nbsp;"+ (expensesVo.verifierPerson?expensesVo.verifierPerson:""));
			$("#verifierDate").html("&nbsp;&nbsp;"+ (expensesVo.verifierTime?expensesVo.verifierTime:""));
			$("#updatePerson").html("&nbsp;&nbsp;"+ expensesVo.updatePerson);
			$("#updateTime").html("&nbsp;&nbsp;"+ expensesVo.updateTime);
			$("#id").val(expensesVo.id);
			$("#versionLock").val(expensesVo.versionLock);
			pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有提交权限
			if(expensesVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_SUBMIT"]){
				$("#add").append('<button class="btn btn-default" onclick="updateExpenses(2)">提交审核</button>&nbsp;');
			}
			//验证是否有审核权限
			if(expensesVo.status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_VERIFIER"]){
				$("#add").append('<button class="btn btn-default" onclick="updateExpenses(3)">审核通过</button>&nbsp;<button class="btn btn-default" onclick="updateExpenses(1)">审核失败</button>');
			}
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加页面加载数据
function addExpenseLoad(){
	loading("正在提交加载添加费用单信息");
	var url = "financial/expenses/toAddExpenses.ihtml";
	RequestData(url,'{}',function(data){
		unloading();
		if(data.code == "0") {  
			if(data.obj["paymentTypeVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["paymentTypeVos"],function(n,paymentType) {
	          		htmls +='<option value="'+ paymentType.id +'">'+ paymentType.paymentTypeName +'</option>';
	          	});
	            $("#paymentType").html(htmls);
			}
			if(data.obj["otherCard"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["otherCard"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'">'+ card.cardNickname +'</option>';
	          		 $("#receiveBank").val(card.cardBank);
	          	});
	            $("#receiveCardNo").html(htmls);
			}
			if(data.obj["expensesUseVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["expensesUseVos"],function(n,expensesUse) {
	          		htmls +='<option value="'+ expensesUse.useName +'">'+ expensesUse.useName +'</option>';
	          	});
	            $("#expensesUse").html(htmls);
			}
			if(data.obj["myCard"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["myCard"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'">'+ card.cardNickname +'</option>';
	          	});
	            $("#paymentCardNo").html(htmls);
			}
        }	
	});
	//添加验证
    validatData(1);
}

//添加费用单
function addExpense(){
	loading("正在提交添加费用单信息");
	var url = "financial/expenses/addExpenses.ihtml";
	var json ='{';
	json+='"receiveBank":"'+$("#receiveBank").val()+'",';
	json+='"accountNo":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"paymentType":"'+$("#paymentType").val()+'",';
	json+='"receiveCardNo":"'+$("#receiveCardNo").val()+'",';
	json+='"expensesUse":"'+$("#expensesUse").val()+'",';
	json+='"paymentAmount":"'+$("#paymentAmount").val()+'",';
	json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editExpenseLoad(){
	loading("正在提交加载编辑费用单信息");
	var url = "financial/expenses/toEditExpenses.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0") {  
			if(data.obj["paymentTypeVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["paymentTypeVos"],function(n,paymentType) {
	          		htmls +='<option value="'+ paymentType.id +'">'+ paymentType.paymentTypeName +'</option>';
	          	});
	            $("#paymentType").html(htmls);
			}
			if(data.obj["otherCard"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["otherCard"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'">'+ card.cardNickname +'</option>';
	          	});
	            $("#receiveCardNo").html(htmls);
			}
			if(data.obj["myCard"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["myCard"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'">'+ card.cardNickname +'</option>';
	          	});
	            $("#paymentCardNo").html(htmls);
			}
			if(data.obj["expensesVo"]){
				var expensesVo = data.obj["expensesVo"];
				$("#id").val(expensesVo.id);
				$("#versionLock").val(expensesVo.versionLock);
				$("#accountNo").val(expensesVo.accountNo);
				$("#accountName").val(expensesVo.accountName);
				$("#paymentType").val(expensesVo.paymentType);
				$("#receiveCardNo").val(expensesVo.receiveCardNo);
				$("#paymentAmount").val(expensesVo.paymentAmount);
				$("#blurAmount").html(convertCurrency(expensesVo.paymentAmount+""));
				$("#paymentCardNo").val(expensesVo.paymentCardNo);
				$("#remark").val(expensesVo.remark);
				if(data.obj["expensesUseVos"]){
					var htmls = '<option value="">请选择</option>';
		            $.each(data.obj["expensesUseVos"],function(n,expensesUse) {
		            	if(expensesVo.expensesUse == expensesUse.useName){
			          		htmls +='<option value="'+ expensesUse.useName +'" selected="selected">'+ expensesUse.useName +'</option>';
		            	}else{
			            	htmls +='<option value="'+ expensesUse.useName +'">'+ expensesUse.useName +'</option>';
		            	}
		          	});
		            $("#expensesUse").html(htmls);
				}
			}
        }	
	});
	//添加验证
    validatData(2);
}

//编辑费用单
function editExpense(){
	loading("正在提交编辑费用单信息");
	var url = "financial/expenses/editExpenses.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"accountNo":"'+$("#accountNo").val()+'",';
	json+='"accountName":"'+$("#accountName").val()+'",';
	json+='"paymentType":"'+$("#paymentType").val()+'",';
	json+='"receiveCardNo":"'+$("#receiveCardNo").val()+'",';
	json+='"expensesUse":"'+$("#expensesUse").val()+'",';
	json+='"paymentAmount":"'+$("#paymentAmount").val()+'",';
	json+='"paymentCardNo":"'+$("#paymentCardNo").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//更新费用单
function updateExpenses(status){
	loading("正在提交更新费用单信息");
	var url = "financial/expenses/updateExpenses.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	if($("#cancellationReason").val()){
		json+='"cancellationReason":"'+$("#cancellationReason").val()+'",';
	}else if(status == 4){
		layer.alert("请录入注销原因");
		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		layer.alert(data.msg);
		if(data.code == "0"){
			location.href = 'expense-detial.html?id='+ $("#id").val() +'&supperResId='+ supperResId;
		}
	});
}


//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount){
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
}

//金额转化大小写
function blurAmount(){
	if($("#paymentAmount").val() && !(isNaN($("#paymentAmount").val()))){
		$("#blurAmount").html(convertCurrency($("#paymentAmount").val()));
	}
}

//添加费用单验证
function validatData(type){
    var validate = $("#expen-from").validate({
        rules:{
            accountName:{required:true},       
            paymentAmount:{required:true,number:true}
        },
        submitHandler: function(form){
        	if(!$("#paymentType").val()){
        		layer.alert("请选择付款方式");
        		return;
        	}
        	if(!$("#receiveCardNo").val()){
        		layer.alert("请选择收款帐号");
        		return;
        	}
        	if(!$("#expensesUse").val()){
        		layer.alert("请选择费用用途");
        		return;
        	}
        	if(!$("#paymentCardNo").val()){
        		layer.alert("请选择付款帐号");
        		return;
        	}
        	if(type == 1){
				addExpense();
        	}else{
				editExpense();
        	}
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });      
}
