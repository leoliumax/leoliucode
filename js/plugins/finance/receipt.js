﻿  //收款单列表页初始化数据
function queryReceiveListLoad(){
	var url = "financial/receive/queryReceiveListLoad.ihtml";
	RequestData(url,'{}',function(data){
		for (var key in data.obj) {  
			if(key == "accounts"){   //账号
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj[key],function(n,accountVo) {
	          		htmls +='<option value="'+ accountVo.accountNo +'">'+ accountVo.accountName +'</option>';
	          	});
	            $("#payment-account").html(htmls);
	        }else if(key == "paymentTypeVos"){  
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,paymentType) {
	          		htmls +='<option value="'+ paymentType.id +'">'+ paymentType.paymentTypeName +'</option>';
	          	});
	          	$("#paymentType").html(htmls);
	        }else if(key == "cardVos"){  
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,cardVo) {
	          		htmls +='<option value="'+ cardVo.cardNo +'">'+ cardVo.cardNickname +'</option>';
	          	});
	          	$("#receiptCardNo").html(htmls);
	        }
        }		
        pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_ADD"]){
			var _url="receipt-add.html?supperResId="+ request('supperResId');
			$("#add").html('<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'新增收款单\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "financial/receive/queryReceiveList.ihtml";
	var json ='{';
	if($("#receiveNo").val()){json+='"receiveNo":"'+$("#receiveNo").val()+'",';}
	if($("#payment-account").val()){json+='"paymentAccountNo":"'+$("#payment-account").val()+'",';}
	if($("#amountStart").val()){json+='"amountStart":"'+$("#amountStart").val()+'",';}
	if($("#amountEnd").val()){json+='"amountEnd":"'+$("#amountEnd").val()+'",';}
	if($("#paymentType").val()){json+='"paymentType":"'+$("#paymentType").val()+'",';}
	if($("#receiptCardNo").val()){json+='"receiveCardNo":"'+$("#receiptCardNo").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
	if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,receiveVo) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ receiveVo.receiveNo +'</td>'
							+'<td>'+ receiveVo.paymentAccountName +'</td>'
							+'<td>'+ receiveVo.receiveAmount +'</td>'
							+'<td>'+ (receiveVo.receiveCardNo?receiveVo.receiveCardNo:"") +'</td>'
							+'<td>'+ receiveVo.createPerson +'</td>'
							+'<td>'+ (receiveVo.verifierPerson?receiveVo.verifierPerson:"") +'</td>'
							+'<td>'+ receiveVo.createTime +'</td>'
							+'<td>'+ (receiveVo.status==1?"草稿":receiveVo.status==2?"待审":receiveVo.status==3?"已审":"注销") +'</td>'
							+'<td>'+ (receiveVo.remark?receiveVo.remark:"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_DETAIL"]){
								var _url="receipt-detail.html?id="+receiveVo.id +"&supperResId="+ request('supperResId');
								htmls +='<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看收款单信息\')">查看</a>';
							}
							//验证是否有编辑权限
							/*if(receiveVo.status==1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_EDIT"]){
								htmls +='<a href="receipt-modity.html?id='+ receiveVo.id +'&supperResId='+ supperResId +'">编辑</a>';
							}*/
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#receiveNo").val()){json+='"receiveNo":"'+$("#receiveNo").val()+'",';}
	if($("#payment-account").val()){json+='"paymentAccountNo":"'+$("#payment-account").val()+'",';}
	if($("#amountStart").val()){json+='"amountStart":"'+$("#amountStart").val()+'",';}
	if($("#amountEnd").val()){json+='"amountEnd":"'+$("#amountEnd").val()+'",';}
	if($("#paymentType").val()){json+='"paymentType":"'+$("#paymentType").val()+'",';}
	if($("#receiptCardNo").val()){json+='"receiveCardNo":"'+$("#receiptCardNo").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#verifierPerson").val()){json+='"verifierPerson":"'+$("#verifierPerson").val()+'",';}
	if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
	if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "financial/receive/exportExcelReceive.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看收款单详情
function receiveDetail(){
	loading("正在提交查看收款单详情信息");
	var url = "financial/receive/queryReceiveInfo.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			if(data.obj["receiveVo"]){   
				var receiveVo = data.obj["receiveVo"];
				var accountVo = data.obj["accountVo"];
				$("#id").val(receiveVo.id);
				$("#versionLock").val(receiveVo.versionLock);
				$("#receiveNo").html("ID："+ receiveVo.receiveNo);
				$("#paymentAccountName").html("&nbsp;&nbsp;"+ receiveVo.paymentAccountName);
				$("#receiveAmount").html("&nbsp;&nbsp;"+ receiveVo.receiveAmount);
				$("#receiveCardNo").html("&nbsp;&nbsp;"+ (receiveVo.receiveCardNo?receiveVo.receiveCardNo:""));
				$("#receiveAmountBig").html("&nbsp;&nbsp;"+ convertCurrency(receiveVo.receiveAmount +"") );
				$("#originalAmount").html("&nbsp;&nbsp;"+ accountVo.accountAmount);
				$("#remainAmount").html("&nbsp;&nbsp;"+ receiveVo.remainAmount);
				$("#remark").html("&nbsp;&nbsp;"+ (receiveVo.remark?receiveVo.remark:""));
				$("#status").html("&nbsp;&nbsp;"+ (receiveVo.status==1?"草稿":receiveVo.status==2?"待审":receiveVo.status==3?"已审":"注销"));
				$("#createPerson").html("&nbsp;&nbsp;"+ receiveVo.createPerson);
				$("#careteTime").html("&nbsp;&nbsp;"+ receiveVo.createTime);
				$("#verifierPerson").html("&nbsp;&nbsp;"+ (receiveVo.verifierPerson?receiveVo.verifierPerson:""));
				$("#verifierDate").html("&nbsp;&nbsp;"+ (receiveVo.verifierDate?receiveVo.verifierDate:""));
				$("#updatePerson").html("&nbsp;&nbsp;"+ receiveVo.updatePerson);
				$("#updateTime").html("&nbsp;&nbsp;"+ receiveVo.updateDate);
//				$("#availableLimit").html("&nbsp;&nbsp;"+ receiveVo.availableLimit?receiveVo.availableLimit:"");
//				$("#usedLimit").html("&nbsp;&nbsp;"+ receiveVo.usedLimit?receiveVo.usedLimit:"");
				
				var pageButtonMap = data.obj["pageButtonMap"];
				//验证是否有提交权限
				if(receiveVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_SUBMIT"]){
					$("#default-header").append('<button class="btn btn-default" onclick="updateReceive(2)">提交审核</button>&nbsp;');
				}
				//验证是否有审核权限
				if(receiveVo.status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_VERIFI"]){
					$("#default-header").append('<button class="btn btn-default" onclick="updateReceive(3)">审核通过</button>&nbsp;<button class="btn btn-default" onclick="updateReceive(1)">审核失败</button>');
				}
				//验证是否有审核权限
				if(receiveVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_RECEIVE_CANCEL"]){
					$("#default-header").append('<button type="button" class="btn btn-default pull-right" id="close-order">注销单据</button>');
					$("#close-header").append('<button id="submit-close" type="button" class="btn btn-default pull-right" onclick="updateReceive(4)">完成注销</button>');
				}
				
				$("#close-order").bind('click',function(){
					$("#close-reason").removeClass('hidden');
					$("#close-header").removeClass('hidden');
					$("#default-header").addClass('hidden');
				});
				$("#close").bind('click',function(){
					$("#close-reason").addClass('hidden');
					$("#close-header").addClass('hidden');
					$("#default-header").removeClass('hidden');
				});
	        }
			if(data.obj["receiveShouldVos"]){
	        	var htmls = '';
	        	var amountSum = 0;
	          	$.each(data.obj["receiveShouldVos"],function(n,receiveShouldVo) {
	          		var secondValue = $.trim(receiveShouldVo.billCode);
	            		var test1= secondValue.substring(0,2);
	            		var test = test1=="BO"?"b2b订单":test1=="JO"?"加盟商订单":
	            				   test1=="SO"?"档口订单":test1=="PO"?"采购单":
	            				   test1=="NL"?"需求单": test1=="PR"?"采购退货单":"无名单";
//	          		var test = receiveShouldVo.sourceBillsType==1?"其他单":receiveShouldVo.sourceBillsType ==2?"档口订单":
//	            			  receiveShouldVo.sourceBillsType ==3?"采购退货单":receiveShouldVo.sourceBillsType ==5?"调拨单":
//	            			  receiveShouldVo.sourceBillsType ==6?"盘点差异单":receiveShouldVo.sourceBillsType ==7?"出库单":
//	            			  receiveShouldVo.sourceBillsType ==8?"采购订单":receiveShouldVo.sourceBillsType ==9?"加盟商订单":"无名单";
	          		htmls += '<tr>'+
						'<td>&nbsp;&nbsp;'+ test +'</td>'+
						'<td>&nbsp;&nbsp;'+ receiveShouldVo.receiveShouldTypeName +'</td>'+
						'<td>&nbsp;&nbsp;'+ receiveShouldVo.receiveShouldAmount +'</td>'+
						'<td>&nbsp;&nbsp;'+ (receiveShouldVo.endDate?receiveShouldVo.endDate:"")+'</td>'+
						'<td>&nbsp;&nbsp;'+ (receiveShouldVo.settlementStatus==2?"已核销":"未核销") +'</td>'+
						'</tr>';
					amountSum += receiveShouldVo.receiveShouldAmount;
	          	});
	          	$("#dataList").html(htmls);
	          	$("#num").html("&nbsp;&nbsp;"+ data.obj["receiveShouldVos"].length);
	          	$("#amountSum").html("&nbsp;&nbsp;"+ amountSum);
	        }
		}else{
			layer.alert(data.msg);
		}
	});
}


//添加页面加载数据
function addReceiveLoad(){
	loading("正在提交加载添加页面数据信息");
	var url = "financial/receive/toAddReceive.ihtml";
	RequestData(url,"{}",function(data){
		unloading();
		if(data.code == "0") {  
			if(data.obj["paymentTypeVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["paymentTypeVos"],function(n,paymentType) {
	          		htmls +='<option value="'+ paymentType.id +'">'+ paymentType.paymentTypeName +'</option>';
	          	});
	            $("#paymentType").html(htmls);
			}
			if(data.obj["myCard"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["myCard"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'">'+ card.cardNickname+card.cardBank+card.cardOpenNumber +'</option>';
	          	});
	            $("#receiveCardNo").html(htmls);
			}
    	}	
	});
	$("#save_receipt").click(function(){addReceive();});
}

//添加收款单
function addReceive(){
	loading("正在提交添加收款单信息");
	var url = "financial/receive/addReceive.ihtml";
	var json ='{';
	json+='"paymentAccountNo":"'+$("#accountNo").val()+'",';
	json+='"paymentAccountName":"'+$("#accountName").val()+'",';
	json+='"paymentType":"'+$("#paymentType").val()+'",';
	json+='"receiveAmount":"'+$("#receiveAmount").val()+'",';
	json+='"receiveCardNo":"'+$("#receiveCardNo").val()+'",';
	json+='"originalAmount":"'+$("#originalAmount").html()+'",';
	json+='"cReceiveAmount":"'+$("#cReceiveAmount").html()+'",';
	json+='"settlementCount":"'+$("#settlementCount").html()+'",';
	json+='"remainAmount":"'+$("#remainAmount").html()+'",';
	json+='"availableLimit":"'+$("#availableLimit").html()+'",';
	json+='"usedLimit":"'+$("#usedLimit").html()+'",';
	var receiveShouldNos = "";
  	$('input[name="receiveShoulds"]:checked').each(function(i,checkbox){
  		if(i>0){
  			receiveShouldNos+=",";
  		}
  		receiveShouldNos += $(checkbox).val();
  	});
	json+='"receiveShouldNos":"'+receiveShouldNos+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editReceiveLoad(){
	loading("正在提交加载编辑页面数据信息");
	var url = "financial/receive/toEditReceive.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0") { 
			if(data.obj["paymentTypeVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["paymentTypeVos"],function(n,paymentType) {
	          		htmls +='<option value="'+ paymentType.id +'">'+ paymentType.paymentTypeName +'</option>';
	          	});
	            $("#paymentType").html(htmls);
			}
			if(data.obj["myCard"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["myCard"],function(n,card) {
	          		htmls +='<option value="'+ card.cardNo +'">'+ card.cardNickname +'</option>';
	          	});
	            $("#receiveCardNo").html(htmls);
			}
			if(data.obj["receiveShouldVos"]){
				var htmls = '';
	            $.each(data.obj["receiveShouldVos"],function(n,receiveShould) {
	            	var secondValue = $.trim(receiveShould.billCode);
	            	var test1 = secondValue.substring(0,2);
	            		var testValue = test1=="BO"?"b2b订单":test1=="JO"?"加盟商订单":
	            				   		test1=="SO"?"档口订单":test1=="PO"?"采购单":
	            				   		test1=="NL"?"需求单": test1=="PR"?"采购退货单":"无名单";
	            	if(data.obj["map"] && data.obj["map"][receiveShould.receiveShouldNo]){
		          		htmls +='<tr><td><label class="checkbox" for=""><input type="checkbox" name="receiveShoulds" checked="checked" value="'+ receiveShould.id +'" onclick="sumAmount('+ receiveShould.receiveShouldAmount +')" /></label></td><td>'+ testValue +'</td><td>'+ receiveShould.receiveShouldAmount +'</td><td>3天8小时14分</td><td>'+ (receiveShould.settlementStatus==1?"未核销":"已核销") +'</td></tr>';
	            	}else{
		          		htmls +='<tr><td><label class="checkbox" for=""><input type="checkbox" name="receiveShoulds" value="'+ receiveShould.id +'" onclick="sumAmount('+ receiveShould.receiveShouldAmount +')" /></label></td><td>'+ testValue +'</td><td>'+ receiveShould.receiveShouldAmount +'</td><td>3天8小时14分</td><td>'+ (receiveShould.settlementStatus==1?"未核销":"已核销") +'</td></tr>';
	            	}
	          	});
	            $("#dataList").append(htmls);
			}
			if(data.obj["receiveVo"]){
				var receiveVo = data.obj["receiveVo"];
				$("#id").val(receiveVo.id);
				$("#versionLock").val(receiveVo.versionLock);
				$("#accountNo").val(receiveVo.paymentAccountNo);
				$("#accountName").val(receiveVo.paymentAccountName);
				$("#paymentType").val(receiveVo.paymentType);
				$("#receiveAmount").val(receiveVo.receiveAmount);
				$("#receiveCardNo").val(receiveVo.receiveCardNo);
				$("#originalAmount").html(receiveVo.originalAmount);
				$("#cReceiveAmount").html(receiveVo.cReceiveAmount);
				$("#settlementCount").html(receiveVo.settlementCount);
				$("#availableLimit").html(receiveVo.availableLimit);
				$("#usedLimit").html(receiveVo.usedLimit);
				$("#remainAmount").html(receiveVo.remainAmount);
				$("#remark").val(receiveVo.remark);
				blurAmount();
			}
        }	
	});
	$("#editor_receipt").click(function(){editReceive();});
}

//编辑收款单
function editReceive(){
	loading("正在提交编辑收款单信息");
	var url = "financial/receive/editReceive.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"paymentAccountNo":"'+$("#accountNo").val()+'",';
	json+='"paymentAccountName":"'+$("#accountName").val()+'",';
	json+='"paymentType":"'+$("#paymentType").val()+'",';
	json+='"receiveAmount":"'+$("#receiveAmount").val()+'",';
	json+='"receiveCardNo":"'+$("#receiveCardNo").val()+'",';
	json+='"originalAmount":"'+$("#originalAmount").html()+'",';
	json+='"cReceiveAmount":"'+$("#cReceiveAmount").html()+'",';
	json+='"settlementCount":"'+$("#settlementCount").html()+'",';
	json+='"remainAmount":"'+$("#remainAmount").html()+'",';
	json+='"availableLimit":"'+$("#availableLimit").html()+'",';
	json+='"usedLimit":"'+$("#usedLimit").html()+'",';
	var receiveShouldNos = "";
  	$('input[name="receiveShoulds"]:checked').each(function(i,checkbox){
  		if(i>0){
  			receiveShouldNos+=",";
  		}
  		receiveShouldNos += $(checkbox).val();
  	});
	json+='"receiveShouldNos":"'+receiveShouldNos+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//更新收款单状态
function updateReceive(status){
	loading("正在提交更新收款单状态信息");
	var url = "financial/receive/updateReceive.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	if($("#cancel").val()){
		json+='"cancellationReason":"'+$("#cancel").val()+'",';
	}else if(status == 4){
		layer.alert("请录入注销原因");
		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

/**
 * 加载该付款账户对应的应收单
 * @param {Object} accountNo
 */
function receiveShould(accountNo){
	var url="financial/receive/should/queryReceiveShouldForReceive.ihtml?paymentAccountNo="+accountNo;
	RequestData(url,"",function(data){
		if(data.obj){
			var htmls = '';
            $.each(data.obj,function(n,receiveShould) {
            	var secondValue = $.trim(receiveShould.billCode);
            	var test1 = secondValue.substring(0,2);
            		var testValue = test1=="BO"?"b2b订单":test1=="JO"?"加盟商订单":
            				   		test1=="SO"?"档口订单":test1=="PO"?"采购单":
            				   		test1=="NL"?"需求单": test1=="PR"?"采购退货单":test1=="SDN"?"销售发货单":"无名单";
          		htmls +='<tr><td><label class="checkbox" for=""><input type="checkbox" name="receiveShoulds" value="'+ receiveShould.id +'" onclick="sumAmount('+ receiveShould.receiveShouldAmount +')" /></label></td><td>'+testValue+'</td><td>'+ receiveShould.receiveShouldAmount +'</td><td>3天8小时14分</td><td>'+ (receiveShould.settlementStatus==1?"未核销":"已核销") +'</td></tr>';
          	});
            $("#dataList").html(htmls);
		}else{
			$("#dataList").html("");
		}
	});
}

//添加账户
function addMarqueAccount(accountNo,accountName,fixedLimit,accountPeriod,accountAmount,ownerAccountAmount,availableLimit,usedLimit){
	var accountAmount1 = new Number(accountAmount).toFixed(2);
	var availableLimit1 =new Number(availableLimit).toFixed(2);
	var usedLimit1 = new Number(usedLimit).toFixed(2);
	$("#accountNo").val(accountNo);
	$("#accountName").val(accountName);
	$("#originalAmount").html(accountAmount1);
	$("#availableLimit").html(availableLimit1);
	$("#usedLimit").html(usedLimit1);
	receiveShould(accountNo);
	remainAmount();
//	var url = 'financial/card/queryCardList.ihtml';
//	var json = '{';
//	json += '"accountId":"'+ accountNo +'","type":'+ 2 +'}';
//	RequestData(url, json, function(data){
//		if(data.code == "0"){
////			var htmls = '<option value=""></option>';
////			if(data.obj.vos){
////				var vos = data.obj.vos;
////				$.each(vos, function(n,itme) {
////					htmls += '<option value="'+ itme.id +'">' + itme.cardNickname + '</option>'           
////				});
////			}
////			$("#receiveCardNo").html(htmls);
//		}else{
//			layer.alert(data.msg);
//		}
//	});
}

//金额转化大小写
function blurAmount(){
	if($("#receiveAmount").val() && !(isNaN($("#receiveAmount").val()))){
		$("#bigAmount").html(convertCurrency($("#receiveAmount").val()));
		$("#cReceiveAmount").html($("#receiveAmount").val());
		remainAmount();
	}
}

//金额小计
function sumAmount(){
	var sumAmount = 0;
  	$('input[name="receiveShoulds"]:checked').each(function(i,checkbox){  
  		sumAmount += parseFloat($(checkbox.parentNode.parentNode).next().next().html());
  	});
  	$("#settlementCount").html(sumAmount);
  	remainAmount();
}

//计算剩余在账
function remainAmount(){
	var amount = 0;
  	var originalAmount = $("#originalAmount").html()||0;
  	var cReceiveAmount = $("#cReceiveAmount").html()||0;
  	var settlementCount = $("#settlementCount").html()||0;
  	var remainAmount2 = parseFloat(originalAmount) + parseFloat(cReceiveAmount) - parseFloat(settlementCount);
  	var remainAmount = new Number(remainAmount2).toFixed(2);
  	$("#remainAmount").html(remainAmount>0?"+"+remainAmount:remainAmount);
}


function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}