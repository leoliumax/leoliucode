function loadpagesExpenseTypeVo(){
	//加载列表数据
	var url = "financial/expensesType/loadpagesExpenseTypeVo.ihtml";
	RequestData(url,'{}',function(data){
		if(data.code == "0"){
			if(data.obj["pageButtonMap"]){
				pageButtonMap = data.obj["pageButtonMap"];
				//验证是否有添加权限
				if(pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_TYPE_ADD"]){
					$("#add").html('<a href="expense-type-add.html?supperResId='+ request("supperResId")+'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
				}
				//验证是否有导出权限
				/*if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_TYPE_EXPORT"]){
					var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
					$("#excel").html(htmls);
				}*/
			}
		}else{
			layer.alert(data.msg);
		}
	});
	$("#pageNo").val(1);
	page();
}

function page(){
	loading("正在努力加载");
	var url = "financial/expensesType/pageExpensesType.ihtml";
	var json ='{';
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +',"inTypeGradeList":[2, 3]}';
	RequestData(url,json,function(datas){
		$($(".panel-title")[0]).html("共有"+ datas.obj.total+"条记录，每页"+ datas.obj.pageSize+"条记录");
		var htmls = '';
		if(datas.obj.rows){
			$.each(datas.obj.rows,function(n,expensesTypeVo) {
	      		htmls +='<tr>'
							+'<td>'+ (n+1) +'</td>'
							+'<td>'+ expensesTypeVo.typeName +'</td>'
							+'<td>'+ (expensesTypeVo.remark==undefined?'':expensesTypeVo.remark)+'</td>'							
							+'<td>'+ (expensesTypeVo.status==1?"已启用":"已停用") +'</td>'
							+'<td>'+ (expensesTypeVo.updateUser==undefined?'':expensesTypeVo.updateUser) +'</td>'
							+'<td>'+ (expensesTypeVo.updateDatetime==undefined?'':expensesTypeVo.updateDatetime) +'</td>';
						//验证是否有编辑权限
						if(pageButtonMap && pageButtonMap["RES_FINANCIAL_EXPENSES_TYPE_EDIT"]){
							htmls +='<td><a href="expense-type-edit.html?id='+ expensesTypeVo.id +'&supperResId='+ request("supperResId") +'">编辑</a></td>';
						}
				htmls+='</tr>';
	      	});
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(datas.obj.pageNo,datas.obj.page);
		unloading();
	});
}


//编辑页面，加载类型数据
function queryExpensesTypeById(){
	loading("加载编辑数据");
	var url = "financial/expensesType/expensesTypeDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			if(data.obj){
				var expensesTypeVo = data.obj;
				$("#id").val(expensesTypeVo.id);
				$("#typeName").val(expensesTypeVo.typeName);
				$("#supperTypeName").val(expensesTypeVo.supperTypeName);
				$("#supperTypeId").val(expensesTypeVo.supperTypeId);
				$("#status").get(0).index = ((expensesTypeVo.status==1?"1":"2"));
				$("#remark").val((expensesTypeVo.remark?expensesTypeVo.remark:""));
			}
		}else{
			layer.alert(data.msg);
		}
	});
	$(function(){
        var validate = $("#expense_form").validate({
            rules:{
                typeName:{required:true,minlength:6},
                supperTypeName:{required:true}
            },
            submitHandler: function(form){
				updateExpensesType();
            }
        });      
    });
}
//编辑页面修改数据
function updateExpensesType(){
	loading("提交编辑信息");
	var url = "financial/expensesType/updateExpensesType.ihtml";
	var jdata = $("#expense_form").serializeObject();
	var json = JSON.stringify(jdata);
	RequestData(url, json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//新增页面保存按钮监听
$("#addSubmit").click(function(){
	$(function(){
        var validate = $("#expense_form").validate({
            rules:{
                typeName:{required:true,minlength:2},
                supperTypeName:{required:true}
            },
            submitHandler: function(form){
				addExpensesType();
            }
        });      
    });
	
});

//新增页面保存数据
function addExpensesType(){
	loading("提交新增信息");
	var url = "financial/expensesType/addExpensesType.ihtml";
	var jdata = $("#expense_form").serializeObject();
	jdata["supperTypeName"] = $("#supperTypeName").val();
	var json = JSON.stringify(jdata);
	RequestData(url, json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面的父类型修改
function updateSupperType(id, typeName){
	var $supperTypeName = $("#supperTypeName").val();
	var i = 0;
	i = $supperTypeName.length;
	$("#supperTypeName").val(typeName);
	$("#supperTypeId").val(id);
	var $typeName = $("#typeName").val().substr(i);
	$("#typeName").val(typeName+$typeName);
}



//新增页面的父类型修改
function addSupperType(id, typeName){
	$("#supperTypeName").val(typeName);
	$("#supperTypeId").val(id);
}

