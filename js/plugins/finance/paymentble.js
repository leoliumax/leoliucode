﻿﻿//应付单列表页初始化数据
function queryPaymentShouldListLoad(){
	var url = "paymentshould/paymentshould/querypaymentshouldLoad.ihtml";
	RequestData(url,'{}',function(data){
		
		for (var key in data.obj) {  
			if(key == "accounother"){   //账户
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj[key],function(n,accountVo) {
	          		htmls +='<option value="'+ accountVo.id +'">'+ accountVo.accountName +'</option>';
	          	});
	            $("#receiveAccountNo").html(htmls);
	        }else if(key == "otherPaymentTypeVos"){  
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,paymentTypeVo) {
	          		htmls +='<option value="'+ paymentTypeVo.id +'">'+ paymentTypeVo.typeName +'</option>';
	          	});
	          	$("#paymentShouldType").html(htmls);
	        }
        }
		pageButtonMap = data.obj["pageButtonMap"];
		
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_SHOULD_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
	
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "paymentshould/paymentshould/querypaymentshouldpage.ihtml";
	var json ='{';
		if($("#paymentShouldNo").val()){json+='"paymentShouldNo":"'+$("#paymentShouldNo").val()+'",';}
		if($("#receiveAccountNo").val()){json+='"receiveAccountName":"'+$("#receiveAccountNo").val()+'",';}
		if($("#sourceBills").val()){json+='"billType":"'+$("#sourceBills").val()+'",';}
		if($("#paymentShouldAmountStart").val()){json+='"paymentShouldAmountStart":"'+$("#paymentShouldAmountStart").val()+'",';}
		if($("#paymentShouldAmountEnd").val()){json+='"paymentShouldAmountEnd":"'+$("#paymentShouldAmountEnd").val()+'",';}
		if($("#paymentShouldType").val()){json+='"paymentShouldType":"'+$("#paymentShouldType").val()+'",';}
		if($("#childType").val()){json+='"paymentShouldChildName":"'+$("#childType").val()+'",';}
		if($("#sourceId").val()){json+='"billCode":"'+$("#sourceId").val()+'",';}
		if($("#endDateStart").val()){json+='"endDateStart":"'+$("#endDateStart").val()+'",';}
		if($("#endDateEnd").val()){json+='"endDateEnd":"'+$("#endDateEnd").val()+'",';}
		if($("#settlementStatus").val()){json+='"settlementStatus":"'+$("#settlementStatus").val()+'",';}
		if($("#settlementDateStart").val()){json+='"settlementDateStart":"'+$("#settlementDateStart").val()+'",';}
		if($("#settlementDateEnd").val()){json+='"settlementDateEnd":"'+$("#settlementDateEnd").val()+'",';}
		if($("#status").val()){json+='"status":"'+$("#status").val()+'",';}
		if($("#createDateStart").val()){json+='"createDateStart":"'+$("#createDateStart").val()+'",';}
		if($("#createDateEnd").val()){json+='"createDateEnd":"'+$("#createDateEnd").val()+'",';}
		if($("#verifierDateStart").val()){json+='"verifierDateStart":"'+$("#verifierDateStart").val()+'",';}
		if($("#verifierDateEnd").val()){json+='"verifierDateEnd":"'+$("#verifierDateEnd").val()+'",';}
		if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
		json +='"pageSize":'+ pageSize +'}';
	
		RequestData(url,json,function(data){	
			$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
			var htmls = '';
			if(data.obj.rows){
			$.each(data.obj.rows,function(n,paymentShould) {
	      		htmls +='<tr>'
	      					+'<td>'+(n+1)+'</td>'
							+'<td>'+ paymentShould.paymentShouldNo +'</td>'
							+'<td>'+ (paymentShould.receiveAccountName==undefined?"":paymentShould.receiveAccountName) +'</td>'
							+'<td>'+ paymentShould.paymentShouldAmount +'</td>'
							+'<td>'+ (paymentShould.accountAmount==undefined?0:paymentShould.accountAmount) +'</td>'//已付金额 
							+'<td>'+ (paymentShould.endDate==undefined?"":paymentShould.endDate) +'</td>'
							+'<td>'+ (paymentShould.paymentShouldName==undefined?"":paymentShould.paymentShouldName) +'</td>'
							+'<td>'+ paymentShould.billCode +'</td>'
							+'<td>'+ (paymentShould.status==1?"草稿":paymentShould.status==2?"待审":paymentShould.status==3?"已审":"注销") +'</td>'
							+'<td>'+ paymentShould.createDate +'</td>'
							+'<td>'+ (paymentShould.settlementStatus==1?"未核销":"已核销") +'</td>'
							+'<td>'+ (paymentShould.settlementDate==undefined?"":paymentShould.settlementDate) +'</td>'
							+'<td>';
							
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_SHOULD_DETAIL"]){
								var _url="paymentble-detail.html?id="+paymentShould.id +"&supperResId="+ request('supperResId');
								htmls +='<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看应付单详情\')">查看</a>';
								//htmls +='<a href="paymentble-detail.html?id='+ paymentShould.id +'&supperResId='+ supperResId +'">查看</a>&nbsp;&nbsp;';
							}
							
						htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	json +='"pageNo":1}';
	var url = getIP() + "paymentshould/paymentshouldOther/exportexcelpaymentshouldOther.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看应付单详情
function paymentShouldDetail(){
	loading("正在提交查看应付单详情信息");
	var url = "paymentshould/paymentshould/getpaymentshouldbyid.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			var paymentShouldVo = data.obj["paymentShouldVo"];
			$("#versionLock").val(paymentShouldVo.versionLock);
			$("#id").val(paymentShouldVo.id);
			
			//基本信息
			$("#paymentShouldNo").html("&nbsp;&nbsp"+ paymentShouldVo.paymentShouldNo);
			$("#settlementStatus").html("&nbsp;&nbsp;"+ (paymentShouldVo.settlementStatus == 1?"未核销":"已核销"));
			$("#paymentShouldName").html("&nbsp;&nbsp;"+ paymentShouldVo.paymentShouldName?paymentShouldVo.paymentShouldName:"");
			$("#endDate").html("&nbsp;&nbsp;"+ (paymentShouldVo.endDate?paymentShouldVo.endDate:""));
			$("#receiveAccountName").html("&nbsp;&nbsp;"+ (paymentShouldVo.receiveAccountName?paymentShouldVo.receiveAccountName:""));
			$("#settlementPerson").html("&nbsp;&nbsp;"+ (paymentShouldVo.settlementPerson?paymentShouldVo.settlementPerson:""));
			$("#settlementPerson").html("&nbsp;&nbsp;"+ (paymentShouldVo.settlementDate?paymentShouldVo.settlementDate:""));
			$("#paymentShouldAmount").html("&nbsp;&nbsp;"+ (paymentShouldVo.paymentShouldAmount?paymentShouldVo.paymentShouldAmount:""));
			$("#settlementDate").html("&nbsp;&nbsp;"+ (paymentShouldVo.settlementDate?paymentShouldVo.settlementDate:""));
			$("#paymentAmountBig").html("&nbsp;&nbsp;"+ convertCurrency(""+paymentShouldVo.paymentShouldAmount));
			$("#paymentCode").html("&nbsp;&nbsp;"+ (paymentShouldVo.paymentCode?paymentShouldVo.paymentCode:"")); 
			$("#sourceType").html("&nbsp;&nbsp;"+ (paymentShouldVo.sourceType==1?"B2B订单":"销售单"));
			$("#sourceId").html("&nbsp;&nbsp;"+ paymentShouldVo.billCode);
			
			
			//单据创建信息
			$("#remark").html("&nbsp;&nbsp;"+ (paymentShouldVo.remark?paymentShouldVo.remark:""));
			$("#createTime").html("&nbsp;&nbsp;"+ paymentShouldVo.createDate);
			$("#status").html("&nbsp;&nbsp;"+ (paymentShouldVo.status==1?"草稿":paymentShouldVo.status==2?"待审":paymentShouldVo.status==3?"已审":"注销"));
			$("#createPerson").html("&nbsp;&nbsp;"+ paymentShouldVo.createPerson);
			$("#verifierDate").html("&nbsp;&nbsp;"+ (paymentShouldVo.verifierDate?paymentShouldVo.verifierDate:""));
			$("#updateDate").html("&nbsp;&nbsp;"+ paymentShouldVo.updateDate);
			$("#verifierPerson").html("&nbsp;&nbsp;"+ (paymentShouldVo.verifierPerson?paymentShouldVo.verifierPerson:""));
			$("#updatePerson").html("&nbsp;&nbsp;"+ paymentShouldVo.updatePerson);
			
			var pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有提交权限
			if(paymentShouldVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_SHOULD_SUBMIT"]){
				$("#add").append('<button class="btn btn-default" onclick="checkPendingById(2)">提交审核</button>&nbsp;');
			}
			//验证是否有审核权限
			if(paymentShouldVo.status == 2 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_SHOULD_VERIFI"]){
				$("#add").append('<button class="btn btn-default" onclick="auditPaymentShouldById(3)">审核通过</button>&nbsp;<button class="btn btn-default" onclick="failureCheckPendingById(1)">审核失败</button>');
			}
			//验证是否有注销权限
			if(paymentShouldVo.status == 1 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_SHOULD_CANCEL"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="cancelPaymentShouldById(4)">注销单据</button>');
			}
			//验证是否有核销权限  
			if(paymentShouldVo.settlementStatus == 1 && paymentShouldVo.status == 3 && pageButtonMap && pageButtonMap["RES_FINANCIAL_PAYMENT_SHOULD_ABOLISH"]){
				$("#add").append('<button type="button" class="btn btn-default pull-right" onclick="verificationPaymentShouldById(3)">核销</button>');
			}
			
		}else{
			layer.alert(data.msg);
		}
	});
}




//更新其他应收单状态(草稿-->待审)
function checkPendingById(status){
	loading("正在提交待审操作");
	var url = "paymentshould/paymentshould/checkpendingbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}



//更新其他应收单状态(草稿-->注销)
function cancelPaymentShouldById(status){
	loading("正在提交注销操作");
	var url = "paymentshould/paymentshould/cancelpaymentshouldbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	if($("#cancel").val()){
		json+='"cancellationReason":"'+$("#cancel").val()+'",';
	}else if(status == 4){
//		layer.alert("请录入注销原因");
//		return;
	}
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//更新其他应收单状态(待审-->已审)
function auditPaymentShouldById(status){
	loading("正在提交审核成功操作");
	var url = "paymentshould/paymentshould/auditpaymentshouldbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//更新其他应收单状态(待审-->草稿)
function failureCheckPendingById(status){
	loading("正在提交审核失败操作");
	var url = "paymentshould/paymentshould/failurecheckpendingbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//更新其他应收单状态(已审-->核销)
function verificationPaymentShouldById(status){
	loading("正在提交核销操作");
	var url = "paymentshould/paymentshould/verificationpaymentshouldbyid.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"status":"'+ status +'",';
	json+='"versionLock":"'+$("#versionLock").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//金额转化大小写
function blurAmount(){
	if($("#paymentShouldAmount").val() && !(isNaN($("#paymentShouldAmount").val()))){
		$("#bigAmount").html(convertCurrency($("#paymentShouldAmount").val()));
	}
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}