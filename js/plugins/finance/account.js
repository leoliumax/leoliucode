﻿//账户列表页初始化数据
function queryAccountListLoad(){
	var url = "financial/account/pageOthersAccountLoad.ihtml";
	RequestData(url,'{}',function(data){
        pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_ACCOUNT_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在努力加载");
	var url = "financial/account/pageOthersAccount.ihtml";
	var json ='{';
	if($("#accountNo").val()){json+='"accountNo":"'+$("#accountNo").val()+'",';}
	if($("#accountPeriod").val()){json+='"accountPeriod":"'+$("#accountPeriod").val()+'",';}
	if($("#startLimit").val()){json+='"startLimit":"'+$("#startLimit").val()+'",';}
	if($("#endLimit").val()){json+='"endLimit":"'+$("#endLimit").val()+'",';}
	if($("#accountName").val()){json+='"accountName":"'+$("#accountName").val()+'",';}
	if($("#oppositeAccountPeriod").val()){json+='"oppositeAccountPeriod":"'+$("#oppositeAccountPeriod").val()+'",';}
	if($("#startUpdateDate").val()){json+='"startUpdateDate":"'+$("#startUpdateDate").val()+'",';}
	if($("#endUpdateDate").val()){json+='"endUpdateDate":"'+$("#endUpdateDate").val()+'",';}
	if($("#createPerson").val()){json+='"createPerson":"'+$("#createPerson").val()+'",';}
	if($("#enableStatus").val()){json+='"enableStatus":"'+$("#enableStatus").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,account) {
	      		htmls +='<tr>'
	      					+'<td>'+ (n+1) +'</td>'
							+'<td>'+ account.accountNo +'</td>'
							+'<td>'+ account.accountName +'</td>'
							+'<td>'+ (new Number(account.accountAmount).toFixed(2))+'</td>'
							+'<td>'+ (new Number(account.ownerAccountAmount).toFixed(2))+'</td>'
							+'<td>'+ account.fixedLimit +'</td>'
							+'<td>'+ (new Number(account.availableLimit).toFixed(2)) +'</td>'
							+'<td>'+ account.accountPeriod +'</td>'
							+'<td>'+ account.oppositeAccountPeriod +'</td>'
							+'<td>'+ (account.enableStatus==1?"启用":"禁用") +'</td>'
							+'<td>'+ (account.updatePerson?account.updatePerson:"") +'</td>'
							+'<td>'+ (account.updateDate?account.updateDate:"") +'</td>'
							+'<td>'+ (account.remark?account.remark:"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_ACCOUNT_DETAIL"]){
								var _url="other-account-detail.html?id="+account.id +"&supperResId="+ request('supperResId');
								htmls +='<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看账户信息\')">查看</a>';
							}
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_FINANCIAL_OTHER_ACCOUNT_EDIT"]){
								//htmls +='<a href="other-account-modity.html?id='+ account.id +'&supperResId='+ supperResId +'">编辑</a>';
							}
					htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	})
}

//导出EXCEL
$("#exportExcel").click(function(){
//	var json ='{';
//	json +='"pageNo":1}';
//	var url = getIP() + "financial/account/exportexcelpaymentshould.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
//	window.location.href = url;
})

//查看账户详情
function accountDetail(){
	loading("正在提交查看客户帐期览表详情信息");
	var url = "financial/account/othersAccountDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			$("#accountNo").html("ID："+ data.obj.accountNo);
			$("#accountName").html("&nbsp;&nbsp;"+ data.obj.accountName);
			$("#accountAmount").html("&nbsp;&nbsp;"+ (new Number(data.obj.accountAmount).toFixed(2)));
			$("#ownerAccountAmount").html("&nbsp;&nbsp;"+ (new Number(data.obj.ownerAccountAmount).toFixed(2)));
			$("#enableStatus").html("&nbsp;&nbsp;"+ (data.obj.enableStatus==1?"启用":"禁用"));
			$("#fixedLimit").html("&nbsp;&nbsp;"+ data.obj.fixedLimit);
			$("#availableLimit").html("&nbsp;&nbsp;"+ data.obj.availableLimit);
			$("#accountPeriod").html("&nbsp;&nbsp;"+ data.obj.accountPeriod + "天");
			$("#oppositeAccountPeriod").html("&nbsp;&nbsp;"+ data.obj.oppositeAccountPeriod + "天");
			$("#remark").html("&nbsp;&nbsp;"+ (data.obj.remark?data.obj.remark:""));
			$("#createPerson").html("&nbsp;&nbsp;"+ data.obj.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+ data.obj.createTime);
			$("#updatePerson").html("&nbsp;&nbsp;"+ data.obj.updatePerson?data.obj.updatePerson:"");
			$("#updateDate").html("&nbsp;&nbsp;"+ data.obj.updateDate?data.obj.updateDate:"");
		}else{
			layer.alert(data.msg);
		}
	});
}


//编辑页面加载数据
function editAccountLoad(){
	loading("正在加载编辑客户帐期览表信息");
	var url = "financial/account/othersAccountDetail.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0" && data.obj) { 
			var account = data.obj;
			$("#id").val(account.id);
			$("#versionLock").val(account.versionLock);
			$("#accountName").html(account.accountName);
			$("#enableStatus").html(account.enableStatus==1?"启用":"禁用");
			$("#fixedLimit").val(account.fixedLimit);
			$("#accountPeriod").val(account.accountPeriod);
			$("#oppositeAccountPeriod").val(account.oppositeAccountPeriod);
			$("#remark").val(account.remark?account.remark:"");
        }	
	});
	
	var validate = $("#expen-from").validate({
        rules:{
            fixedLimit:{required:true,digits:true},
            accountPeriod:{required:true,digits:true},
            oppositeAccountPeriod:{required:true,digits:true}
        },
        submitHandler: function(form){
			editAccount();
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });  
}

//编辑客户账期览表(账户)
function editAccount(){
	loading("正在提交编辑客户账期览表信息");
	var url = "financial/account/updateOthersAccount.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"fixedLimit":"'+$("#fixedLimit").val()+'",';
	json+='"accountPeriod":"'+$("#accountPeriod").val()+'",';
	json+='"oppositeAccountPeriod":"'+$("#oppositeAccountPeriod").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}

