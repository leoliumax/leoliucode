﻿//初始化页面
function pageProductLoad(){
	var url = "commodity/organProduct/toPageOrganProduct.ihtml";
	RequestData(url,'{}',function(data){	
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_COMMODITY_PRODUCT_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	var url = "commodity/organProduct/pageOrganProduct.ihtml";
	var json ='{';
	if($("#catName").val()){json+='"catName":"'+$("#catName").val()+'",';}
	if($("#brandName").val()){json+='"brandName":"'+$("#brandName").val()+'",';}
	if($("#commoName").val()){json+='"commoName":"'+$("#commoName").val()+'",';}
	if($("#prodName").val()){json+='"prodName":"'+$("#prodName").val()+'",';}
	if($("#prodSource").val()){json+='"prodSource":"'+$("#prodSource").val()+'",';}
	if($("#isCode").val()){json+='"isCode":"'+$("#isCode").val()+'",';}
	if($("#createTimeStart").val()){json+='"createTimeStart":"'+$("#createTimeStart").val()+'",';}
	if($("#createTimeEnd").val()){json+='"createTimeEnd":"'+$("#createTimeEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,product) {
	      		htmls +='<tr>'
	      					+'<td>'+ (++n) +'</td>'
							+'<td>'+ product.prodNo +'</td>'
							+'<td>'+ product.prodName +'</td>'
							+'<td>'+ product.catName +'</td>'
							+'<td>'+ product.brandName +'</td>'
							+'<td>'+ product.commoName +'</td>'
							+'<td>'+ (product.isCode=="1"?"有码":"无码") +'</td>'
							+'<td>'+ (product.prodSource == 1?"易道产品库":"自建") +'</td>'
							+'<td>'+ (product.prodStatus == 1?"启用":"禁用") +'</td>'
							+'<td>'+ product.createPerson +'</td>'
							+'<td>'+ product.createTime +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_COMMODITY_PRODUCT_DETAIL"]){
								htmls +='<a href="javascript:openPage(\'查看商品信息\',\'commodity-detail.html?id='+ product.id +'&supperResId='+ supperResId +'\')">查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_COMMODITY_PRODUCT_EDIT"]){
								htmls +='<a href="javascript:openPage(\'编辑商品信息\',\'commodity-modify.html?id='+ product.id +'&supperResId='+ supperResId +'\')">编辑</a>&nbsp;&nbsp;';
							}
						htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	});
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#brandNo").val()){json+='"brandNo":"'+$("#brandNo").val()+'",';}
	if($("#brandName").val()){json+='"brandName":"'+$("#brandName").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "commodity/organProduct/exportExcelOrganProduct.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看商品详情
function productDetail(){
	var url = "commodity/organProduct/productDetailByButtonMap.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			var productVo = data.obj["productVo"];
			$("#id").val(productVo.id);
			$("#prodNo").html("&nbsp;&nbsp;"+ productVo.prodNo);
			$("#sellerProdNo").html("&nbsp;&nbsp;"+ (productVo.sellerProdNo?productVo.sellerProdNo:""));
			$("#prodName").html("&nbsp;&nbsp;"+ productVo.prodName);
			$("#brandName").html("&nbsp;&nbsp;"+ productVo.brandName);
			$("#commoName").html('&nbsp;&nbsp;<a href="">'+ productVo.commoName+'</a>');
			$("#catName").html("&nbsp;&nbsp;"+ productVo.catName);
			$("#isCode").html("&nbsp;&nbsp;"+ (productVo.isCode=="1"?"有码":"无码"));
			$("#prodStatus").html("&nbsp;&nbsp;"+ (productVo.prodStatus==1?"启用":"禁用"));
			$("#remark").html("&nbsp;&nbsp;"+ (productVo.remark?productVo.remark:""));
			$("#defaultPic").attr("src",getIP() +productVo.defaultPic);
			$("#createPerson").html("&nbsp;&nbsp;"+ productVo.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+ productVo.createTime);
			$("#updatePerson").html("&nbsp;&nbsp;"+ productVo.updatePerson);
			$("#updateTime").html("&nbsp;&nbsp;"+ productVo.updateTime);
			
			//验证是否有复制商品权限
			if(data.obj["pageButtonMap"] && data.obj["pageButtonMap"]["RES_COMMODITY_PRODUCT_COPY"]){
				$("#addBtn").append('<a class="btn btn-default" data-toggle="modal" onclick="copyCreateProduct(\''+ productVo.prodName +'\')">'+ (data.obj["pageButtonMap"]["RES_COMMODITY_PRODUCT_COPY"]) +'</a>');
			}
		}else{
			layer.alert(data.msg);
		}
	});
}

//跳转到编辑页面
function toProductEdit(){
	var url = "commodity/organProduct/organProductDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			$("#id").val(data.obj.id);
			$("#versionLock").val(data.obj.versionLock);
			$("#prodName").html("&nbsp;&nbsp;"+ data.obj.prodName);
			$("#brandName").html("&nbsp;&nbsp;"+ data.obj.brandName);
			$("#commoName").html('&nbsp;&nbsp;<a href="">'+ data.obj.commoName+'</a>');
			$("#catName").html("&nbsp;&nbsp;"+ data.obj.catName);
			$("#isCode").html("&nbsp;&nbsp;"+ (data.obj.isCode=="1"?"有码":"无码"));
			$("#sellerProdNo").val(data.obj.sellerProdNo);
			$("#oldSalePrice").val(data.obj.salePrice?data.obj.salePrice:0);
			$("#salePrice").val(data.obj.salePrice);
			$("#prodStatus").val(data.obj.prodStatus);
			$("#remark").val(data.obj.remark);
			$("#defaultPic").attr("src",getIP() +data.obj.defaultPic);
			$("#iframe").attr("src","commodity-pic.html?commoNo="+data.obj.commoNo);
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑商品信息
$("#productEdit").click(function(){
	var url = "commodity/organProduct/updateOrganProduct.ihtml";
	var json ='{"id":"'+ $("#id").val()+'",';
		json+='"versionLock":"'+ $("#versionLock").val()+'",';
		//json+='"sellerProdNo":"'+$("#sellerProdNo").val()+'",';
		//json+='"oldSalePrice":"'+$("#oldSalePrice").val()+'",';
		//json+='"salePrice":"'+$("#salePrice").val()+'",';
		var picUrl = $("#defaultPic").attr("src");
		picUrl = picUrl.substring(picUrl.lastIndexOf("commodity"), picUrl.length);
		json+='"defaultPic":"'+picUrl+'",';
		json+='"prodStatus":"'+$("#prodStatus").val()+'",';
		json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
});

//商品图片列表信息
function choosePic(picUrl){
	$("#defaultPic").attr("src",picUrl);
	$('#myModal').modal('hide');
}


//页面层
function copyCreateProduct(prodName){
	prodDiv = layer.open({
    type: 1,
	title:"SKU复制创建",
    skin: 'layui-layer-rim', //加上边框
    area: ['800px', '150px'], //宽高
    content: '<div style="margin-top: 10px;" id="addProduct">'
				    +'<label for="" class="col-md-2 g-lb">新商品名称: </label>'
				    +'<input type="text" />&nbsp;&nbsp;'
				    +'<spen>'+ prodName +'</spen>&nbsp;&nbsp;'
					+'<input type="text" />'
				+'</div><div style="margin-top: 10px;margin-left: 10px;"><a class="btn btn-default" href="javascript:addProduct(\''+ prodName +'\')">添加商品</a></div>'
	});
}

function addProduct(prodName){
	if($.trim($($('#addProduct input')[0]).val()) || $.trim($($('#addProduct input')[1]).val())){
		prodName = $($('#addProduct input')[0]).val() +" "+ prodName +" "+ $($('#addProduct input')[1]).val();
		var url = "commodity/organProduct/copyCreateProduct.ihtml";
		var json ='{"id":"'+ $("#id").val() +'","prodName":"'+ prodName +'"}';
		RequestData(url,json,function(data){
			layer.alert(data.msg);
			if(data.code == '0'){
				layer.close(prodDiv);
			}
		});
	}else{
		layer.alert("请填写新商品名称");
	}
}

function openPage(title,url){
	layer.open({
	    type: 2,
	    title: title,
	    area: ['80%', '80%'],
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    content: url
	});
}
