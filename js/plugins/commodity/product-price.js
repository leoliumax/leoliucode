var product_price_change_data={};

$(document).ready(function(){
	
	var rgx=/^-?\d+\.?\d{0,2}$/;
	/**编辑价格时在上方显示大的编辑数字**/
	var index;
	$(document).on('keyup','.salePrice',function(event){
		//判断数字区间
		var v=$(this).val();
		if((event.which>=48&&event.which<=57)||(event.which>=96&&event.which<=105)||event.which==8){
			v=$.formatMoney(v,2);
			var mesg="<h2>"+v+"</h2>";
			index=layer.tips(mesg, '#'+$(this).attr("id"), {
				time: 60000,
			    tips: [1, '#0FA6D8'] //还可配置颜色
			});
		}
	}).on('blur','.salePrice',function(){
		if(index){
			layer.close(index);
		}
		var v=$(this).val();
		if(rgx.test(v)){
		 	var cr=$.unformatMoney(v);
			var dfv=$.unformatMoney($(this).attr("data-def"));
			if(cr!=dfv){
				product_price_change_data[$(this).attr("data-id")]=cr;
				$(this).closest("tr").addClass("price-change");
			}else{
				delete product_price_change_data[$(this).attr("data-id")];
				$(this).closest("tr").removeClass("price-change");
			}
		}
	});
	
	$("#allUpdata").click(function(){
		loading("正在更新价格");
		var url="commodity/price/updateProductPrice.ihtml?departmentId=" + $("#departmentId").val();
		RequestData(url,JSON.stringify(product_price_change_data),function(data){	
			unloading();
			product_price_change_data={};
			layer.alert(data.msg);
			$("#search").click();
		});
	});
});

//初始化页面
function pageProductLoad(){
	var url = "commodity/organProduct/toPageOrganProduct.ihtml";
	RequestData(url,'{}',function(data){	
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_COMMODITY_PRODUCT_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	var url = "commodity/organProduct/pageOrganProductByPrice.ihtml";
	var json ='{';
	if($("#departmentId").val()){json+='"departmentId":"'+$("#departmentId").val()+'",';}
	if($("#catName").val()){json+='"catName":"'+$("#catName").val()+'",';}
	if($("#brandName").val()){json+='"brandName":"'+$("#brandName").val()+'",';}
	if($("#commoName").val()){json+='"commoName":"'+$("#commoName").val()+'",';}
	if($("#prodName").val()){json+='"prodName":"'+$("#prodName").val()+'",';}
	if($("#prodSource").val()){json+='"prodSource":"'+$("#prodSource").val()+'",';}
	if($("#prodStatus").val()){json+='"prodStatus":"'+$("#prodStatus").val()+'",';}
	if($("#createTimeStart").val()){json+='"createTimeStart":"'+$("#createTimeStart").val()+'",';}
	if($("#createTimeEnd").val()){json+='"createTimeEnd":"'+$("#createTimeEnd").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,product) {
	      		htmls +='<tr>'
	      					+'<td>'+ (++n) +'</td>'
							+'<td>'+ product.prodNo +'</td>'
							+'<td>'+ product.prodName +'</td>'
							+'<td>'+ product.catName +'</td>'
							+'<td>'+ product.brandName +'</td>'
							+'<td>'+ product.commoName +'</td>'
							+'<td>'+ (product.prodSource == 1?"易道产品库":"自建") +'</td>'
							+'<td>'+ (product.salePrice) +'</td>'
							+'<td><input type="text" id="'+(product.prodNo+n)+'" class="salePrice" style="max-width:110px;" data-id="'+(product.id)+'" data-def="'+(product.salePrice)+'" value="'+(product.salePrice)+'"></td>'
							+'<td><a onclick="openPriceLine(\''+(product.id)+'\')" href="javascript:void(0);">查看</a>&nbsp;&nbsp;</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//格式化所有数字
		//$('.salePrice').formatCurrency();
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	});
}

function openPriceLine(product_id){
	layer.open({
	    type: 2,
	    title: false,
	    shadeClose: true,
	    shade: 0.8,
	    area: ['800px', '560px'],
	    content: 'commondity_price_echart.html?productId='+product_id //iframe的url
	}); 
}
//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#brandNo").val()){json+='"brandNo":"'+$("#brandNo").val()+'",';}
	if($("#brandName").val()){json+='"brandName":"'+$("#brandName").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "commodity/organProduct/exportExcelOrganProduct.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}