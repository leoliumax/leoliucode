﻿//初始化页面
function pageCustomerLoad(){
	var url = "commodity/customer/toPageCustomer.ihtml";
	RequestData(url,'{}',function(data){	
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_COMMODITY_CUSTOMER_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		for (var key in data.obj) {  
			if(key == "organVos"){
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,organVo) {
	          		htmls +='<option value="'+ organVo.organId +'">'+ organVo.organName +'</option>';
	          	});
	          	$("#organId").html(htmls);
	        }
        }	
		
		$("#search").click();
	});
	
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在提交客户管理列表页面");
	var url = "commodity/customer/pageCustomer.ihtml";
	var json ='{';
	if($("#customerId").val()){json+='"customerId":"'+$("#customerId").val()+'",';}
	if($("#organId").val()){json+='"text4":"'+$("#organId").val()+'",';}
	if($("#customerName").val()){json+='"customerName":"'+$("#customerName").val()+'",';}
	if($("#legalName").val()){json+='"legalName":"'+$("#legalName").val()+'",';}
	if($("#contactPhone").val()){json+='"contactPhone":"'+$("#contactPhone").val()+'",';}
	if($("#loginAccount").val()){json+='"loginAccount":"'+$("#loginAccount").val()+'",';}
	if($("#areaName").val()){json+='"areaName":"'+$("#areaName").val()+'",';}
	if($("#shopType").val()){json+='"shopType":"'+$("#shopType").val()+'",';}
	if($("#startStatus").val()){json+='"startStatus":"'+$("#startStatus").val()+'",';}
	if($("#updateTimeStart").val()){json+='"updateTimeStart":"'+$("#updateTimeStart").val()+'",';}
	if($("#updateTimeEnd").val()){json+='"updateTimeEnd":"'+$("#updateTimeEnd").val()+'",';}
	if($("#updatePerson").val()){json+='"updatePerson":"'+$("#updatePerson").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,customer) {
				//验证是否维护地址的权限
				var postaddr="";
				if(pageButtonMap && pageButtonMap["RES_COMMODITY_CUSTOMER_DETAIL"]){
					var _url="customer-postaddr-list.html?id="+ customer.id +"&supperResId="+ supperResId;
					postaddr ='<a href="javascript:void(0)" onclick="openPage(\''+_url+'\',\'维护收件地址\')">收件地址</a>&nbsp;&nbsp;';
				}
	      		htmls +='<tr>'
							+'<td>'+ (++n) +'</td>'
							+'<td>'+ (customer.customerName?customer.customerName:"") +'</td>'
							+'<td>'+ (customer.shopType == 1?"连锁店":customer.shopType == 2?"大型超市":"零售店") +'</td>'
							+'<td>'+ (customer.enterpriseName?customer.enterpriseName:"" ) +'</td>'
							+'<td>'+ (customer.legalName?customer.legalName:"" ) +'</td>'
							+'<td>'+ (customer.contactPerson?customer.contactPerson:"" ) +'</td>'
							+'<td>'+ (customer.contactPhone?customer.contactPhone:"" ) +'</td>'
							+'<td>'+ (customer.text5?customer.text5:"" ) +'</td>'
							+'<td>'+ (customer.startStatus == 1?"启用":"禁用" ) +'</td>'
							+'<td>'+ (customer.updatePerson?customer.updatePerson:"") +'</td>'
							+'<td>'+ (customer.updateTime?customer.updateTime.substring(0,10):"") +'</td>'
							+'<td>'+postaddr
							+'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_COMMODITY_CUSTOMER_DETAIL"]){
								var _url="customer-detail.html?id="+ customer.id +"&supperResId="+ supperResId;
								htmls +='<a href="javascript:void(0)" onclick="openPage(\''+_url+'\',\'查看客户信息\')">查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_COMMODITY_CUSTOMER_EDIT"]){
								var _url="customer-modify.html?id="+ customer.id +"&supperResId="+ supperResId;
								htmls +='<a href="javascript:void(0)" onclick="openPage(\''+_url+'\',\'编辑客户信息\')">编辑</a>&nbsp;&nbsp;';
							}
						htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	});
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#customerId").val()){json+='"customerId":"'+$("#customerId").val()+'",';}
	if($("#customerName").val()){json+='"customerName":"'+$("#customerName").val()+'",';}
	if($("#loginAccount").val()){json+='"loginAccount":"'+$("#loginAccount").val()+'",';}
	if($("#areaName").val()){json+='"areaName":"'+$("#areaName").val()+'",';}
	if($("#shopType").val()){json+='"shopType":"'+$("#shopType").val()+'",';}
	if($("#enterpriseName").val()){json+='"enterpriseName":"'+$("#enterpriseName").val()+'",';}
	if($("#legalName").val()){json+='"legalName":"'+$("#legalName").val()+'",';}
	if($("#startStatus").val()){json+='"startStatus":"'+$("#startStatus").val()+'",';}
	if($("#updatePerson").val()){json+='"updatePerson":"'+$("#updatePerson").val()+'",';}
	if($("#updateTime").val()){json+='"updateTime":"'+$("#updateTime").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "commodity/customer/exportExcelCustomer.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

/**
 *新增客户 
 */
function add_customer(){
	
}

//查看客户信息
function productDetail(){
	var url = "commodity/customer/customerDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			pageButtonMap = data.obj["pageButtonMap"];
			var customerVo = data.obj;
			//基本信息
			$("#customerId").html(customerVo.customerId);
			$("#enterpriseName").html("&nbsp;&nbsp;"+ (customerVo.enterpriseName?customerVo.enterpriseName:""));
			$("#customerName").html("&nbsp;&nbsp;"+ (customerVo.customerName?customerVo.customerName:""));
			$("#legalName").html("&nbsp;&nbsp;"+ (customerVo.legalName?customerVo.legalName:""));
			$("#shopType").html("&nbsp;&nbsp;"+ (customerVo.shopType==1?"连锁店":customerVo.shopType==2?"大型超市":customerVo.shopType==3?"零售店":"--"));
			$("#areaName").html("&nbsp;&nbsp;"+ (customerVo.areaName?customerVo.areaName:""));
			$("#contactPhone").html("&nbsp;&nbsp;"+ (customerVo.contactPhone?customerVo.contactPhone:""));
			$("#companyAddress").html("&nbsp;&nbsp;"+ (customerVo.text2?customerVo.text2:"--"));
			$("#developPerson").html("&nbsp;&nbsp;"+ (customerVo.developPerson?customerVo.developPerson:""));
			$("#cooperationTime").html("&nbsp;&nbsp;"+ (customerVo.cooperationTime?customerVo.cooperationTime:""));
			$("#qq").html("&nbsp;&nbsp;"+ (customerVo.qq?customerVo.qq:""));
			$("#email").html("&nbsp;&nbsp;"+ (customerVo.email?customerVo.email:""));
			$("#contactPerson").html("&nbsp;&nbsp;"+ (customerVo.contactPerson?customerVo.contactPerson:""));
			
			//账号及安全
			$("#loginAccount").html("&nbsp;&nbsp;"+ (customerVo.loginAccount?customerVo.loginAccount:""));
			$("#identityProve").html("&nbsp;&nbsp;"+ (customerVo.identityProve==1?"未认证":"已认证"));
			$("#safetyEmail").html("&nbsp;&nbsp;"+ (customerVo.safetyEmail?customerVo.safetyEmail:""));
			$("#identityName").html("&nbsp;&nbsp;"+ (customerVo.identityName?customerVo.identityName:""));
			$("#loginPassword").html("&nbsp;&nbsp;"+ (customerVo.loginPassword?customerVo.loginPassword:""));//还没弄好
			$("#identityNumber").html("&nbsp;&nbsp;"+ (customerVo.identityNumber?customerVo.identityNumber:""));
			$("#safetyProblem").html("&nbsp;&nbsp;"+ (customerVo.safetyProblem?customerVo.safetyProblem:""));
			$("#identityPhoto").html("&nbsp;&nbsp;"+ customerVo.identityPhoto);//还没弄好
			
			//运营信息
			$("#startStatus").html("&nbsp;&nbsp;"+ (customerVo.startStatus==1?"启用":"禁用"));
			$("#remark").html("&nbsp;&nbsp;"+ (customerVo.remark?customerVo.remark:""));
			$("#auditPerson").html("&nbsp;&nbsp;"+ (customerVo.auditPerson?customerVo.auditPerson:""));
			$("#auditTime").html("&nbsp;&nbsp;"+ (customerVo.auditTime?customerVo.auditTime:""));
			$("#updatePerson").html("&nbsp;&nbsp;"+ (customerVo.updatePerson?customerVo.updatePerson:""));
			$("#updateTime").html("&nbsp;&nbsp;"+ (customerVo.updateTime?customerVo.updateTime:""));
			
			//客户级别
			var htmls1 = '';
			if(customerVo.customerLevelVos){
				$($(".panel-title")).html("共有"+ customerVo.customerLevelVos.length +"条记录");
				$.each(customerVo.customerLevelVos,function(n,itme) {
			      		htmls1 +='<tr>'
									+'<td>'+ itme.itemType +'</td>'
									+'<td >'+ itme.gradeName +'</td>'	
								+'</tr>';
		      });
		    }
			$("#dataList2").html(htmls1);
		}else{
			layer.alert(data.msg);
		}
	});
}

//跳转到编辑页面
function toCustomerEdit(){
	var url = "commodity/customer/toUpdateCustomer.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			if(data.obj["regionVos"]){
				var htmls = '<option value="">请选择</option>';
	            $.each(data.obj["regionVos"],function(n,item) {
	          		htmls +='<option value="'+ item.id +'">'+ item.allianceAreaName +'</option>';
	          	});
	            $("#areaId").html(htmls);
			}
			var customerVo = data.obj["customerVo"];
			//基本信息
			$("#enterpriseName").html( customerVo.enterpriseName);
			$("#legalName").html( customerVo.legalName);
			$("#contactPhone").html(customerVo.contactPhone);
			//var addressName = (customerVo.companyAddressProvince?customerVo.companyAddressProvince:"") + (customerVo.companyAddressCity?customerVo.companyAddressCity:"") + (customerVo.companyAddressCounty?customerVo.companyAddressCounty:"") + (customerVo.companyAddressTown?customerVo.companyAddressTown:"") +(customerVo.companyAddressSamll?customerVo.companyAddressSamll:"");
			$("#companyAddress").html(customerVo.text2?customerVo.text2:"--");
			$("#developPerson").html( customerVo.developPerson);
			$("#cooperationTime").html( customerVo.cooperationTime);
			
			//客户属性
			$("#customerName").val(customerVo.customerName);
			$("#shopType").val(customerVo.shopType);//areaName
			$("#areaName").val(customerVo.areaName);
			$("#startStatus").val(customerVo.startStatus);
			$("#remark").val(customerVo.remark);
			$("#qq").val(customerVo.qq);
			$("#email").val(customerVo.email);
			$("#contactPerson").val(customerVo.contactPerson);
			$("#customerPassword").val(customerVo.loginPassword);
			
			
			$("#versionLock").val(customerVo.versionLock);
			$("#customerId").val(customerVo.customerId);
			$("#companyAddressProvince").val(customerVo.companyAddressProvince);
			$("#companyAddressCity").val(customerVo.companyAddressCity);
			$("#companyAddressCounty").val(customerVo.companyAddressCounty);
			$("#companyAddressTown").val(customerVo.companyAddressTown);
			$("#companyAddressSamll").val(customerVo.companyAddressSamll);
			$("#dateilAddress").val(customerVo.dateilAddress?customerVo.dateilAddress:"");
			$("#text1").val(customerVo.text1?customerVo.text1:"");
			$("#areaId").val(customerVo.areaId);
			if(data.obj["customerGradeTabVos"]){
				customerGradeTabVos = data.obj["customerGradeTabVos"];
			}
			
			//客户级别
			var htmls1 = '';
			if(customerVo.customerLevelVos){
				$($(".panel-title")).html("共有"+ customerVo.customerLevelVos.length +"条记录");
				$.each(customerVo.customerLevelVos,function(n,itme) {
			      		htmls1 +='<tr><input type="hidden" id="'+ itme.itemId +'" value="'+ itme.itemId+'"/>'
									+'<td>'+ itme.itemType +'</td>'
									+'<td><select name="customer-gradeName">';
									if(data.obj["customerGradeTabVos"]){
										$.each(customerGradeTabVos,function(n,grade) {
											htmls1 +='<option value="'+ grade.id +'"';
											if(itme.gradeId == grade.id){htmls1 +='selected=selected '}
											htmls1 +='>'+ grade.gradeName +'</option>';
										});
									}
									htmls1 +='</select></td></tr>';
		      });
		    }
			$("#dataList1").html(htmls1);
			iframe1.page();
//			unloading();
		}else{
			layer.alert(data.msg);
		}
		//重载省份信息
    	getinfo("province",0,"province");
	});
	//添加验证
    validatData();
}

//添加客户级别
function addMarqueCategory(catNo,catName){
	var htmls = '<tr><input type="hidden" id="'+ catNo +'" value="'+ catNo+'"/><td>'+ catName +'</td><td><select class="other-select" name="customer-gradeName">';
	$.each(customerGradeTabVos,function(n,grade) {
		htmls +='<option value="'+ grade.id +'">'+ grade.gradeName +'</option>';
	});
	htmls +='</select></td></tr>';
	$("#dataList1").append(htmls);
	//selectInit();
}


//添加验证
function validatData(){
    var validate = $("#expen-from").validate({
        rules:{
        	customerName:{required:true},
            customerPassword:{required:true,rangelength:[6,16]},
            qq:{digits:true},
            email:{email:true},
            contactPerson:{required:true}
        },
        submitHandler: function(form){
			if(!$("#shopType").val()){
        		layer.alert("请选择店铺类型");
        		return;
        	}
			editCustomerSave();
        },  
        errorPlacement: function(error, element) {
        	element.next().html("");
			error.appendTo(element.next()); 
		}                 
    });      
}



//保存&提交审核
function editCustomerSave(){
	loading("正在提交保存&提交审核信息");
	var url = "commodity/customer/savaSubmitCustomer.ihtml";
	var json = '{';
		//基本信息
		json +='"enterpriseName":"'+$("#enterpriseName").text()+'", ';
		json +='"legalName":"'+$("#legalName").text() +'", ';
		json +='"contactPhone":"'+$("#contactPhone").text() +'", ';
		json +='"developPerson":"'+$("#developPerson").text() +'",';
		if($("#province").val()!=0)
			json +='"companyAddressProvince":"'+$("#province").find("option:selected").text()+'",';
		if($("#city").val()!=0)
			json +='"companyAddressCity":"'+$("#city").find("option:selected").text() +'",';
		
		if($("#county").val()!=0)
			json +='"companyAddressCounty":"'+$("#county").find("option:selected").text() +'",';
		
		if($("#town").val()!=0)
			json +='"companyAddressTown":"'+$("#town").find("option:selected").text() +'",';
		
		if($("#village").val()!=0)
			json +='"companyAddressSamll":"'+$("#village").find("option:selected").text() +'",';
		
		json +='"text1":"'+$("#text1").val() +'",';
		
		//客户属性
		json +='"id":"'+ request("id")+'",';
		json +='"versionLock":"'+ $("#versionLock").val() +'",';
		json += '"customerName":"'+ $("#customerName").val() +'",';
		json += '"qq":"'+ $("#qq").val() +'",';
		json += '"email":"'+ $("#email").val() +'",';
		json += '"contactPerson":"'+ $("#contactPerson").val() +'",';
		json += '"loginPassword":"'+ $("#customerPassword").val() +'",';
		json += '"areaId":"'+ $("#areaId").val() +'",';
		json += '"areaName":"'+ $("#areaId").find("option:selected").text() +'",';
		json += '"shopType":"'+ $("#shopType").val() +'",';
		json += '"startStatus":"'+ $("#startStatus").val() +'",';
		json += '"customerId":"'+ $("#customerId").val() +'",';
		json += '"customerId":"'+ $("#customerId").val() +'",';
		json +='"customerLevelVos":[';
		$("#dataList1").children("tr").each(function(i,tr){
			var itemId = $($(tr).children("input")[0]).val();
			var itemType = $(tr).children('td').eq(0).html();
			var gradeId = $($('select[name="customer-gradeName"]')[i]).val();
			var gradeName = $($('select[name="customer-gradeName"]')[i]).find("option:selected").text();
			if(i>0){
				json +=',';
			}
			json +='{"itemId":"'+itemId+'","gradeId":"'+gradeId+'","gradeName":"'+gradeName+'","itemType":"'+itemType+'"}';
			
		});
		json +='],';
		json += ' "remark":"'+ $("#remark").val() +'" ';
		json +='}';
		RequestData(url,json,function(data){
			unloading();
			if(data.code == "0"){
				layer.alert("客户信息提交成功!",function(){
					parent.location.reload();
				});
			}else{
				layer.alert(data.msg);
			}
	
		});
	
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}
