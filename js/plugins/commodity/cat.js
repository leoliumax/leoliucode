﻿
//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	var url = "commodity/commoCategory/pageCommoCategory.ihtml";
	var json ='{';
	if($("#brandNo").val()){json+='"brandNo":"'+$("#brandNo").val()+'",';}
	if($("#brandName").val()){json+='"brandName":"'+$("#brandName").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,brand) {
	      		htmls +='<tr>'
							+'<td>'+ brand.brandNo +'</td>'
							+'<td>'+ brand.brandName +'</td>'
							+'<td>'+ brand.brandEnName +'</td>'
							+'<td>'+ brand.createPerson +'</td>'
							+'<td>'+ brand.createTime +'</td>'
							+'<td>'+ brand.updatePerson +'</td>'
							+'<td>'+ brand.updateTime +'</td>'
							+'<td><a href="commodity-brand-modity.html?id='+ brand.id +'&supperResId='+ request("supperResId") +'">编辑</a></td>'
						+'</tr>'
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	})
}
