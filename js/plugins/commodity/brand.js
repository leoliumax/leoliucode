﻿//初始化页面
function pageBrandLoad(){
	var url = "commodity/brand/toPageBrand.ihtml";
	loading();
	RequestData(url,'{}',function(data){
		unloading();	
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_COMMODITY_BRAND_ADD"]){
			$("#addBrand").html('<a href="commodity-brand-add.html?supperResId='+ supperResId +'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_COMMODITY_BRAND_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading();
	var url = "commodity/brand/pageBrand.ihtml";
	var json ='{';
	if($("#brandNo").val()){json+='"brandNo":"'+$("#brandNo").val()+'",';}
	if($("#brandName").val()){json+='"brandName":"'+$("#brandName").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){
		unloading();	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,brand) {
	      		htmls +='<tr>'
							+'<td>'+ brand.brandNo +'</td>'
							+'<td>'+ brand.brandName +'</td>'
							+'<td>'+ brand.brandEnName +'</td>'
							+'<td>'+ brand.createPerson +'</td>'
							+'<td>'+ brand.createTime +'</td>'
							+'<td>'+ brand.updatePerson +'</td>'
							+'<td>'+ brand.updateTime +'</td>'
							+'<td>';
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_COMMODITY_BRAND_EDIT"]){
								htmls +='<a href="commodity-brand-modity.html?id='+ brand.id +'&supperResId='+ supperResId +'">编辑</a>';
							}
				htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	});
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#brandNo").val()){json+='"brandNo":"'+$("#brandNo").val()+'",';}
	if($("#brandName").val()){json+='"brandName":"'+$("#brandName").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "commodity/brand/exportExcelBrand.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}


//添加页面加载数据
function addBrandLoad(){
	$(function(){
        var validate = $("#expen-from").validate({
            rules:{
                brandName:{required:true,minlength:2},       
                brandEnName:{required:true}
            },
            submitHandler: function(form){
				addBrand();
            },  
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}                 
        });      
    });
}

//添加品牌
function addBrand(){
	loading("正在提交数据...");
	var url = "commodity/brand/addBrand.ihtml";
	var json ='{';
	json+='"brandName":"'+$("#brandName").val()+'",';
	json+='"brandEnName":"'+$("#brandEnName").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}


//修改页面加载数据
function editBrandLoad(){
	loading("正在加载数据...");
	$(function(){
        var validate = $("#expen-from").validate({
            rules:{
                brandName:{required:true,minlength:2},       
                brandEnName:{required:true}
            },
            submitHandler: function(form){
				editBrand();
            },  
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}                 
        });      
    });
    //加载出品牌数据
    var url = "commodity/brand/brandDetail.ihtml";
	var json ='{"id":"'+ request("id") +'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			$("#id").val(data.obj.id);
			$("#versionLock").val(data.obj.versionLock);
			$("#brandName").val(data.obj.brandName);
			$("#brandEnName").val(data.obj.brandEnName);
		}else{
			layer.alert(data.msg);
		}
	});
}

//修改品牌
function editBrand(){
	loading("正在提交数据...");
	var url = "commodity/brand/updateBrand.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"brandName":"'+$("#brandName").val()+'",';
	json+='"brandEnName":"'+$("#brandEnName").val()+'"}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}