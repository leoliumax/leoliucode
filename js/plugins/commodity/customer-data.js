﻿//初始化页面
function pageProductLoad(){//commodity/customerUpdate
	var url = "commodity/customerUpdate/toPageCustomerUpdate.ihtml";
	RequestData(url,'{}',function(data){	
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_COMMODITY_CUSTOMER_DATA_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		for (var key in data.obj) {  
			if(key == "organVos"){
	        	var htmls = '<option value="">请选择</option>';
	          	$.each(data.obj[key],function(n,organVo) {
	          		htmls +='<option value="'+ organVo.organId +'">'+ organVo.organName +'</option>';
	          	});
	          	$("#organId").html(htmls);
	        }
        }	
		
		$("#search").click();
	});
	
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
//客户资料修改审核
function page(){
	loading("正在提交客户资料修改审核列表信息");
	var url = "commodity/customerUpdate/pageCustomerUpdate.ihtml";
	var json ='{';
	if($("#customerId").val()){json+='"customerId":"'+$("#customerId").val()+'",';}
	if($("#organId").val()){json+='"text4":"'+$("#organId").val()+'",';}
	if($("#enterpriseName").val()){json+='"enterpriseName":"'+$("#enterpriseName").val()+'",';}
	if($("#legalName").val()){json+='"legalName":"'+$("#legalName").val()+'",';}
	if($("#loginAccount").val()){json+='"loginAccount":"'+$("#loginAccount").val()+'",';}
	if($("#customerName").val()){json+='"customerName":"'+$("#customerName").val()+'",';}
	if($("#shopType").val()){json+='"shopType":"'+$("#shopType").val()+'",';}
	if($("#auditStatus").val()){json+='"auditStatus":"'+$("#auditStatus").val()+'",';}
	if($("#submitTimeStart").val()){json+='"submitTimeStart":"'+$("#submitTimeStart").val()+'",';}
	if($("#submitTimeEnd").val()){json+='"submitTimeEnd":"'+$("#submitTimeEnd").val()+'",';}
	if($("#submitPerson").val()){json+='"submitPerson":"'+$("#submitPerson").val()+'",';}
	if($("#auditTimeStart").val()){json+='"auditTimeStart":"'+$("#auditTimeStart").val()+'",';}
	if($("#auditTimeEnd").val()){json+='"auditTimeEnd":"'+$("#auditTimeEnd").val()+'",';}
	if($("#auditPerson").val()){json+='"auditPerson":"'+$("#auditPerson").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,customerUpdate) {
	      		htmls +='<tr>'
							+'<td>'+ (++n) +'</td>'
							+'<td>'+ (customerUpdate.enterpriseName?customerUpdate.enterpriseName:"")  +'</td>'
							+'<td>'+ (customerUpdate.legalName?customerUpdate.legalName:"")  +'</td>'
							+'<td>'+ (customerUpdate.loginAccount?customerUpdate.loginAccount:"") +'</td>'
							+'<td>'+ (customerUpdate.customerName?customerUpdate.customerName:"") +'</td>'
							+'<td>'+ (customerUpdate.shopType == 1?"连锁店":customerUpdate.shopType == 2?"大型超市":"零售店") +'</td>'
							+'<td>'+ (customerUpdate.contactPerson?customerUpdate.contactPerson:"" ) +'</td>'
							+'<td>'+ (customerUpdate.text5?customerUpdate.text5:"" ) +'</td>'
							+'<td>'+ (customerUpdate.submitPerson?customerUpdate.submitPerson:"") +'</td>'
							+'<td>'+ (customerUpdate.submitTime?customerUpdate.submitTime.substring(0,10):"") +'</td>'
							+'<td>'+ (customerUpdate.auditStatus== 1?"待审核":"已审核" ) +'</td>'
							+'<td>'+ (customerUpdate.auditPerson?customerUpdate.auditPerson:"") +'</td>'
							+'<td>'+ (customerUpdate.auditTime?customerUpdate.auditTime.substring(0,10):"") +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_COMMODITY_CUSTOMER_DATA_DETA"]){
								var _url="customer-data-modify.html?id="+ customerUpdate.id +"&supperResId="+ supperResId;
								htmls +='<a href="javascript:void(0);" onclick="openPage(\''+_url+'\',\'查看客户资料修改记录\')">查看</a>&nbsp;&nbsp;';
							}
						htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
		unloading();
	});
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#customerId").val()){json+='"customerId":"'+$("#customerId").val()+'",';}
	if($("#enterpriseName").val()){json+='"enterpriseName":"'+$("#enterpriseName").val()+'",';}
	if($("#legalName").val()){json+='"legalName":"'+$("#legalName").val()+'",';}
	if($("#loginAccount").val()){json+='"loginAccount":"'+$("#loginAccount").val()+'",';}
	if($("#submitPerson").val()){json+='"submitPerson":"'+$("#submitPerson").val()+'",';}
	if($("#submitTime").val()){json+='"submitTime":"'+$("#submitTime").val()+'",';}
	if($("#auditStatus").val()){json+='"auditStatus":"'+$("#auditStatus").val()+'",';}
	if($("#auditPerson").val()){json+='"auditPerson":"'+$("#auditPerson").val()+'",';}
	if($("#auditTime").val()){json+='"auditTime":"'+$("#auditTime").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "commodity/customerUpdate/exportExcelCustomerUpdate.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看客户资料修改记录
function customerUpdateDetail(){
	loading("正在提交查看客户资料修改记录信息");
	var url = "commodity/customerUpdate/customerUpdateDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			unloading();
			pageButtonMap = data.obj["pageButtonMap"];
			var customerUpdateVo = data.obj["customerUpdateVo"];
			//基本信息
			$("#customerId").html(customerUpdateVo.customerId);
			$("#enterpriseName").html("&nbsp;&nbsp;"+ (customerUpdateVo.enterpriseName));
			$("#customerName").html("&nbsp;&nbsp;"+ customerUpdateVo.customerName);
			$("#legalName").html("&nbsp;&nbsp;"+ customerUpdateVo.legalName);
			$("#shopType").html("&nbsp;&nbsp;"+ (customerUpdateVo.shopType==1?"连锁店":customerUpdateVo.shopType==2?"大型超市":"零售店"));
			$("#areaName").html("&nbsp;&nbsp;"+ customerUpdateVo.areaName);
			$("#contactPhone").html("&nbsp;&nbsp;"+ customerUpdateVo.contactPhone);
			$("#companyAddress").html("&nbsp;&nbsp;"+ customerUpdateVo.text2);
			$("#developPerson").html("&nbsp;&nbsp;"+ customerUpdateVo.developPerson);
			$("#cooperationTime").html("&nbsp;&nbsp;"+ (customerUpdateVo.cooperationTime?customerUpdateVo.cooperationTime:""));
			$("#loginAccount").html("&nbsp;&nbsp;"+ (customerUpdateVo.loginAccount?customerUpdateVo.loginAccount:""));
			$("#qq").html("&nbsp;&nbsp;"+ (customerUpdateVo.qq?customerUpdateVo.qq:""));
			$("#email").html("&nbsp;&nbsp;"+ (customerUpdateVo.email?customerUpdateVo.email:""));
			$("#contactPerson").html("&nbsp;&nbsp;"+ (customerUpdateVo.contactPerson?customerUpdateVo.contactPerson:""));
			
			
			
			//运营信息
			$("#startStatus").html("&nbsp;&nbsp;"+ (customerUpdateVo.startStatus==1?"启用":"禁用"));
			$("#remark").html("&nbsp;&nbsp;"+ customerUpdateVo.remark);
			$("#auditPerson").html("&nbsp;&nbsp;"+ (customerUpdateVo.auditPerson?customerUpdateVo.auditPerson:""));
			$("#auditTime").html("&nbsp;&nbsp;"+ (customerUpdateVo.auditTime?customerUpdateVo.auditTime:""));
			$("#submitPerson").html("&nbsp;&nbsp;"+ customerUpdateVo.submitPerson);
			$("#submitTime").html("&nbsp;&nbsp;"+ customerUpdateVo.submitTime);
			
			//加载携带的数据
			$("#versionLock").val(customerUpdateVo.versionLock?customerUpdateVo.versionLock:"");
			$("#auditStatus").val(customerUpdateVo.auditStatus);
			$("#id").val(customerUpdateVo.id?customerUpdateVo.id:"");
			$("#password").val(customerUpdateVo.loginPassword);
			$("#companyAddressProvince").val(customerUpdateVo.companyAddressProvince?customerUpdateVo.companyAddressProvince:"");
			$("#companyAddressCity").val(customerUpdateVo.companyAddressCity?customerUpdateVo.companyAddressCity:"");
			$("#companyAddressCounty").val(customerUpdateVo.companyAddressCounty?customerUpdateVo.companyAddressCounty:"");
			$("#companyAddressTown").val(customerUpdateVo.companyAddressTown?customerUpdateVo.companyAddressTown:"");
			$("#companyAddressSamll").val(customerUpdateVo.companyAddressSamll?customerUpdateVo.companyAddressSamll:"");
			
			//客户级别
			var htmls1 = '';
			if(customerUpdateVo.customerLevelUpdateVoUpdateVos){
				$($(".panel-title")).html("共有"+ customerUpdateVo.customerLevelUpdateVoUpdateVos.length +"条记录");
				$.each(customerUpdateVo.customerLevelUpdateVoUpdateVos,function(n,itme) {
			      		htmls1 +='<tr>'
									+'<td>'+ itme.itemType +'</td>'
									+'<td id="gradeName">'+ itme.gradeName +'</td>'								
								+'</tr>';
		      });
		    }
			$("#dataList1").html(htmls1);
			//验证是否有审核权限
			if(customerUpdateVo.auditStatus == 1 &&  pageButtonMap && pageButtonMap["RES_COMMODITY_CUSTOMER_DATA_VERIFI"]){
				$("#default-header").append('<button class="btn btn-default" onclick="auditPass()">审核通过</button>&nbsp;<button class="btn btn-default" onclick="auditFail()">审核失败</button>');
			}
			
		}else{
			layer.alert(data.msg);
		}
	});
}


//审核通过
function auditPass(){
	loading("正在提交审核通过信息");
	var url = "commodity/customerUpdate/auditPass.ihtml";
	var json = '{';
		//加载携带的数据
		json +='"id":"'+ request("id")+'",';
		json +='"versionLock":"'+ $("#versionLock").val() +'",';
		json +='"auditStatus":"'+ $("#auditStatus").val() +'",';
		
		
		//基本信息
		json +='"customerId":"'+ $("#customerId").val() +'" ,';
		json +='"enterpriseName":"'+ $("#enterpriseName").val() +'" ,';
		json +='"customerName":"'+ $("#customerName").val() +'" ,';
		json +='"legalName":"'+ $("#legalName").val() +'" ,';
		json +='"shopType":"'+ $("#shopType").val() +'" ,';
		json +='"areaName":"'+ $("#areaName").val() +'" ,';
		json +='"contactPhone":"'+ $("#contactPhone").val() +'" ,';
		json +='"companyAddressProvince":"'+ $("#companyAddressProvince").val() +'",';
		json +='"companyAddressCity":"'+ $("#companyAddressCity").val() +'",';
		json +='"companyAddressCounty":"'+ $("#companyAddressCounty").val() +'",';
		json +='"companyAddressSamll":"'+ $("#companyAddressSamll").val() +'",';
		json +='"companyAddressTown":"'+ $("#companyAddressTown").val() +'",';
		json +='"developPerson":"'+ $("#developPerson").val() +'" ,';
		json +='"loginAccount":"'+ $("#loginAccount").val() +'" ,';
		
		json +='"contactPerson":"'+ $("#contactPerson").val() +'" ,';
		json +='"email":"'+ $("#email").val() +'" ,';
		json +='"loginPassword":"'+ $("#password").val() +'" ,';
		json +='"qq":"'+ $("#qq").val() +'" ,';

		
		//运营信息
		var startStatus = $("#startStatus").val()=="是"?1:2;
		json +='"startStatus":"'+ startStatus +'" ,';
		json +='"submitPerson":"'+ $("#submitPerson").val() +'" ,';
		json +='"submitTime":"'+ $("#submitTime").val() +'" ,';
		
		//客户级别
		json +='"customerLevelUpdateVoUpdateVos":[';
		$("#dataList1").children("tr").each(function(i,tr){
			var itemType = $(tr).children().eq(0).html();
			var gradeName = $(tr).children().eq(1).html();
			if(i>0){ json +=',';    }
			json +='{"itemType":"'+itemType+'","gradeName":"'+gradeName+'"}';
		});
		json +='],';
		json += ' "remark":"'+ $("#remark").val() +'" ';
		json +='}';
		RequestData(url,json,function(data){
			unloading();
			if(data.code == "0"){
				layer.alert(data.msg,function(){
					parent.location.reload();
				});
			}
		
		});
	
}
	
//审核失败
function auditFail(){
	loading("正在提交审核失败信息");
	var url = "commodity/customerUpdate/auditFailure.ihtml";
	var json = '{';
		//加载携带的数据
		json +='"id":"'+ request("id")+'",';
		json +='"versionLock":"'+ $("#versionLock").val() +'",';
		json +='"auditStatus":"'+ $("#auditStatus").val() +'",';
		
		
		//基本信息
		json +='"customerId":"'+ $("#customerId").val() +'" ,';
		json +='"enterpriseName":"'+ $("#enterpriseName").val() +'" ,';
		json +='"customerName":"'+ $("#customerName").val() +'" ,';
		json +='"legalName":"'+ $("#legalName").val() +'" ,';
		json +='"shopType":"'+ $("#shopType").val() +'" ,';
		json +='"areaName":"'+ $("#areaName").val() +'" ,';
		json +='"contactPhone":"'+ $("#contactPhone").val() +'" ,';
		json +='"companyAddressProvince":"'+ $("#companyAddressProvince").val() +'",';
		json +='"companyAddressCity":"'+ $("#companyAddressCity").val() +'",';
		json +='"companyAddressCounty":"'+ $("#companyAddressCounty").val() +'",';
		json +='"companyAddressSamll":"'+ $("#companyAddressSamll").val() +'",';
		json +='"companyAddressTown":"'+ $("#companyAddressTown").val() +'",';
		json +='"developPerson":"'+ $("#developPerson").val() +'" ,';
		json +='"loginAccount":"'+ $("#loginAccount").val() +'" ,';

		
		//运营信息
		var startStatus = $("#startStatus").val()=="是"?1:2;
		json +='"startStatus":"'+ startStatus +'" ,';
		json +='"submitPerson":"'+ $("#submitPerson").val() +'" ,';
		json +='"submitTime":"'+ $("#submitTime").val() +'" ,';
		
		//客户级别
		json +='"customerLevelUpdateVoUpdateVos":[';
		$("#dataList1").children("tr").each(function(i,tr){
			var itemType = $(tr).children().eq(0).html();
			var gradeName = $(tr).children().eq(1).html();
			if(i>0){ json +=',';    }
			json +='{"itemType":"'+itemType+'","gradeName":"'+gradeName+'"}';
		});
		json +='],';
		json += ' "remark":"'+ $("#remark").val() +'" ';
		json +='}';
	RequestData(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
	
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: url
	});
}