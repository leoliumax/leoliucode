﻿var colordata = '';

function addcolorel(value,pm,th) {
	var col = $("#colordata");
}

function pageListLoad() {
		var url = "commodity/organCommodity/toPageCommodity.ihtml";
		RequestData(url, '{}', function(data) {
			pageButtonMap = data.obj["pageButtonMap"];
			//验证是否有添加权限
			if (pageButtonMap && pageButtonMap["RES_COMMODITY_COMMO_ADD"]) {
				var _url = "marque-add.html?supperResId=" + supperResId;
				$("#add").html('<a href="javascript:void(0);" onclick="openPagePm(\'' + _url + '\',\'新建商品型号\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
			}
			//验证是否有导出权限
			if (pageButtonMap && pageButtonMap["RES_COMMODITY_COMMO__EXPORT"]) {
				var htmls = '<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
				$("#excel").html(htmls);
			}
			$("#search").click();
		})
	}
	//搜索
$("#search").click(function() {
	$("#pageNo").val(1);
	page();
});

function page() {
	var url = "commodity/organCommodity/pageCommodity.ihtml";
	var json = '{';
	if ($("#brandName").val()) {
		json += '"brandName":"' + $("#brandName").val() + '",';
	}
	if ($("#commoName").val()) {
		json += '"commoName":"' + $("#commoName").val() + '",';
	}
	if($("#isCode").val()){json+='"isCode":"'+$("#isCode").val()+'",';}
	if ($("#createTimeStart").val()) {
		json += '"createTimeStart":"' + $("#createTimeStart").val() + '",';
	}
	if ($("#createTimeEnd").val()) {
		json += '"createTimeEnd":"' + $("#createTimeEnd").val() + '",';
	}
	if ($("#pageNo").val()) {
		json += '"pageNo":"' + $("#pageNo").val() + '",';
	}
	json += '"pageSize":' + pageSize + '}';
	RequestData(url, json, function(data) {
		$($(".panel-title")[0]).html("共有" + data.obj.total + "条记录，每页" + data.obj.pageSize + "条记录");
		var htmls = '';
		if (data.obj.rows) {
			$.each(data.obj.rows, function(n, organCommodity) {
				htmls += '<tr>' + '<td>' + (++n) + '</td>' + '<td>' + organCommodity.commoNo + '</td>' + '<td>' + organCommodity.commoName + '</td>' + '<td>' + organCommodity.commoEnName + '</td>' + '<td>' + organCommodity.brandName + '</td>' +'<td>'+ (organCommodity.isCode=="1"?"有码":organCommodity.isCode=="2"?"无码":"") +'</td>' + '<td>' + organCommodity.createPerson + '</td>' + '<td>' + organCommodity.createTime + '</td>' + '<td>' + organCommodity.updatePerson + '</td>' + '<td>' + organCommodity.updateTime + '</td>' + '<td>';
				//验证是否有查看权限
				if (pageButtonMap && pageButtonMap["RES_COMMODITY_COMMO_DETAIL"]) {
					htmls += '<a href="marque-detail.html?id=' + organCommodity.id + '&supperResId=' + supperResId + '">查看</a>';
				}
				htmls += '</td></tr>';
			});
		}
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo, data.obj.page);
	})
}

//导出EXCEL
function exportExcel() {
	var json = '{';
	if ($("#brandNo").val()) {
		json += '"brandNo":"' + $("#brandNo").val() + '",';
	}
	if ($("#brandName").val()) {
		json += '"brandName":"' + $("#brandName").val() + '",';
	}
	json += '"pageNo":1}';
	var url = getIP() + "commodity/organCommodity/exportExcelOrganCommodity.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

function addMarqueBrand(brandNo, brandName) {
	$("#brandNo").val(brandNo);
	$("#brandName").val(brandName);
}

function addMarqueCategory(catNo, catName) {
	$("#catNo").val(catNo);
	$("#catName").val(catName);

	var url = "commodity/property/queryPropertysByCategoryNo.ihtml";
	var json = '{"catNo":"' + catNo + '"}';
	RequestData(url, json, function(data) {
		if (data.code == '0' && data.obj) {
			var html1 = "";
			var html2 = "";
			$.each(data.obj, function(n, itme) {
				if (itme.propType == 1) {
					if (itme.propName === "颜色") {
						html1 += '<tr><td style="width:130px;max-width:130px;">' + itme.propName + '</td>' + '<input type="hidden" value="' + itme.propNo + '"/>' + '<td style="text-align: left;">' + '<div id="colordata" class="colordata"></div><a href="#colorBrand" data-toggle="modal" ><button id="editcolor" class="btn btn-default">选择颜色</button>' + '</td>' + '</tr>'
					} else {
						html1 += '<tr><td style="width:130px;max-width:130px;">' + itme.propName + '</td>' + '<input type="hidden" value="' + itme.propNo + '"/>' + '<td style="text-align: left;">' + '<span>' + '<select style="width: 100px;">' + '<option value="">请选择</option>';
						if (itme.propertyValueVo) {
							$.each(itme.propertyValueVo, function(n, vo) {
								html1 += '<option value="' + vo.propValue + '">' + vo.propValue + '</option>';
							});
						}
						html1 += '</select>' + '</span>' + '<a onclick="addValue(this)">添加</a>' + '</td>' + '</tr>'
					}
				} else {
					html2 += '<tr><td style="width:130px;max-width:130px;">' + itme.propName + '</td>' + '<input type="hidden" value="' + itme.propNo + '"/>' + '<td style="text-align: left;">' + '<span>' + '<select style="width: 100px;">' + '<option value="">请选择</option>';
					if (itme.propertyValueVo) {
						$.each(itme.propertyValueVo, function(n, vo) {
							html2 += '<option value="' + vo.propValue + '">' + vo.propValue + '</option>';
						});
					}
					html2 += '</select>' + '</span>' + '<a onclick="addValue(this)">添加</a>' + '</td>' + '</tr>';
				}
			});
			$("#dataList1").html(html1);
			$("#dataList2").html(html2);
		}
	});
}

//查看商品型号详情
function commodityDetail() {
	var url = "commodity/organCommodity/commodityDetail.ihtml";
	var json = '{"id":"' + request("id") + '"}';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			pageButtonMap = data.obj["pageButtonMap"];
			var commodityVo = data.obj["commodityVo"];
			$("#id").val(commodityVo.id);
			$("#commoNo").html(commodityVo.commoNo);
			$("#commoName").html("&nbsp;&nbsp;" + commodityVo.commoName);
			$("#brandName").html("&nbsp;&nbsp;" + commodityVo.brandName);
			$("#catName").html("&nbsp;&nbsp;" + commodityVo.catName);
			$("#isCode").html("&nbsp;&nbsp;"+ (commodityVo.isCode=="1"?"有码":"无码"));
			$("#createPerson").html("&nbsp;&nbsp;" + commodityVo.createPerson);
			$("#createTime").html("&nbsp;&nbsp;" + commodityVo.createTime);
			$("#updatePerson").html("&nbsp;&nbsp;" + commodityVo.updatePerson);
			$("#updateTime").html("&nbsp;&nbsp;" + commodityVo.updateTime);
			editor.html(commodityVo.description);
			$("#iframe").attr("src", "marque-create-commodity.html?id=" + commodityVo.id);

			//规格参数
			var htmls1 = '';
			var htmls2 = '';
			if (commodityVo.commoPropList) {
				$.each(commodityVo.commoPropList, function(n, itme) {
					if (itme.propType == 1) {
						htmls1 += '<tr>' + '<td>' + itme.propName + '</td>' + '<td>' + itme.propValue + '</td>' + '</tr>';
					} else {
						htmls2 += '<tr>' + '<td>' + itme.propName + '</td>' + '<td>' + itme.propValue + '</td>' + '</tr>';
					}
				});
			}
			$("#dataList1").html(htmls1);
			$("#dataList2").html(htmls2);
			//图片
			var htmls3 = '';
			if (commodityVo.commoPicList) {
				$.each(commodityVo.commoPicList, function(n, itme) {
					htmls3 += '<div><img src="' + getIP() + itme.picUrl + '" alt="" /></div>'
				});
			}
			$("#commoPic").html(htmls3);
			//验证是否有编辑权限
			if (pageButtonMap && pageButtonMap["RES_COMMODITY_COMMO_EDIT"]) {
				$($(".text-muted")[1]).html('其他参数（<a href="marque-modify-other.html?id=' + commodityVo.id + '&supperResId=' + request("supperResId") + '">编辑</a>）');
				$($(".text-muted")[2]).html('商品主图（<a href="marque-modify-pic.html?id=' + commodityVo.id + '&supperResId=' + request("supperResId") + '">编辑</a>）');
				$($(".text-muted")[3]).html('型号介绍（<a href="marque-modify-description.html?id=' + commodityVo.id + '&supperResId=' + request("supperResId") + '">编辑</a>）');
			}
			//验证是否能创建商品
			if (pageButtonMap && pageButtonMap["RES_COMMODITY_COMMO_CREATE"] && 　commodityVo.commoStatus　 == 1) {
				$("#pageBtn").append('<a href="#createCommodity" id="modelcreateCommodity" data-toggle="modal" class="btn btn-default" >创建商品</a>');
			}
		} else {
			layer.alert(data.msg);
		}
	});
}

//商品型号创建商品
function createProduct() {
	var url = "commodity/organCommodity/createProduct.ihtml";
	var json = '{"id":"' + request("id") + '","versionLock":"' + request("versionLock") + '"}';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			layer.alert(data.msg, function() {
				parent.location.reload();
			});
		} else {
			layer.alert(data.msg);
		}
	});
}

//跳到编辑型号其他参数
function toUpdateCommodityProp() {
	var url = "commodity/organCommodity/toUpdateCommodityProp.ihtml";
	var json = '{"id":"' + request("id") + '"}';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			$("#id").val(data.obj.id);
			$("#commoNo").val(data.obj.commoNo);
			$("#commoName").html(data.obj.commoName);
			$("#catName").html(data.obj.catName);
			$("#brandName").html(data.obj.brandName);

			var htmls = '';
			$.each(data.obj.propertyVos, function(n, itme) {
				if (itme.propType == 2) {
					htmls += '<tr><td>' + itme.propName + '</td>' + '<input type="hidden" value="' + itme.propNo + '"/>' + '<td style="text-align: left;">' + '<span>';
					for (var i = 1; i <= itme.sortNo; i++) {
						htmls += '<select style="width: 100px;"><option value="">请选择</option>';
						if (itme.propertyValueVo) {
							$.each(itme.propertyValueVo, function(n, vo) {
								if (i == vo.selected) {
									htmls += '<option value="' + vo.propValue + '" selected="selected">' + vo.propValue + '</option>';
								} else {
									htmls += '<option value="' + vo.propValue + '">' + vo.propValue + '</option>';
								}
							});
						}
						htmls += '</select>'
					}
					htmls += '</span>' + '<a onclick="addValue(this)">添加</a>' + '</td>' + '</tr>';
				}
			});
			$("#dataList2").html(htmls);
		} else {
			layer.alert(data.msg);
		}
	});
}

//添加型号其他参数
function addCommoProp(type, propNo, propName) {
	if (type == 1) {
		$("#dataList1").append('<tr><td>' + propName + '</td><td></td><td><span><input type="hidden" id="' + propNo + '" value="' + propNo + '"><input type="text" /></span><a onclick="addValue(this)">添加</a></td></tr>');
	} else {
		$("#dataList2").append('<tr><td>' + propName + '</td><td></td><td><input type="hidden" id="' + propNo + '" value="' + propNo + '"><input class="col-md-12" type="text" /></td></tr>');
	}
}

//保存编辑好的型号参数
$("#editCommoProp").click(function() {
	var commoPropList = "";
	var trArr = $("#dataList2").children("tr");
	for (var i = 0; i < trArr.length; i++) {
		var tdArr = $(trArr[i]).children("td");
		var propValue = "";
		$($(tdArr[1]).children("span")[0]).children("select").each(function(i, text) {
			if (text.value) {
				isProp = true;
				propValue += text.value + "/";
			}
		});
		if (propValue) {
			propValue = propValue.substring(0, propValue.length - 1);
			commoPropList += commoPropList ? ',{' : '{';
			commoPropList += '"commoNo":"' + $("#commoNo").val() + '",';
			commoPropList += '"propNo":"' + $(tdArr[0]).next().val() + '",';
			commoPropList += '"propName":"' + $(tdArr[0]).html() + '",';
			commoPropList += '"propValue":"' + propValue + '",';
			commoPropList += '"propType":"2"}';
		}
	}

	var url = "commodity/organCommodity/updateCommodityProp.ihtml";
	var json = '[' + commoPropList + ']';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			layer.alert(data.msg, function() {
				window.location.href = 'marque-detail.html?id=' + $("#id").val() + '&supperResId=' + request("supperResId");
			});
		} else {
			layer.alert(data.msg);
		}
	});
});


//跳到编辑型号介绍
function toUpdateCommodity() {
	var url = "commodity/organCommodity/toUpdateCommodity.ihtml";
	var json = '{"id":"' + request("id") + '"}';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			$("#id").val(data.obj.id);
			$("#versionLock").val(data.obj.versionLock);
			$("#commoName").html(data.obj.commoName);
			$("#catName").html(data.obj.catName);
			$("#brandName").html(data.obj.brandName);
			editor.html(data.obj.description);
		} else {
			layer.alert(data.msg);
		}
	});
}

//保存编辑好的型号介绍
$("#editCommoDescription").click(function() {
	var url = "commodity/organCommodity/updateCommodity.ihtml";
	var json = '{"id":"' + $("#id").val() + '",';
	json += '"description":"' + editor.html() + '",';
	json += '"versionLock":"' + $("#versionLock").val() + '"}';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			layer.alert(data.msg, function() {
				window.location.href = 'marque-detail.html?id=' + $("#id").val() + '&supperResId=' + request("supperResId");
			});
		} else {
			layer.alert(data.msg);
		}
	});
});

//跳到编辑型号图片
function toUpdateCommodityPic() {
	var url = "commodity/organCommodity/toUpdateCommodityPic.ihtml";
	var json = '{"id":"' + request("id") + '"}';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			$("#id").val(data.obj.id);
			$("#commoNo").val(data.obj.commoNo);
			$("#commoName").html(data.obj.commoName);
			$("#catName").html(data.obj.catName);
			$("#brandName").html(data.obj.brandName);
			var htmls = '';
			$.each(data.obj.commoPicList, function(n, itme) {
				htmls += '<div class="photo"><img src="' + getIP() + itme.picUrl + '" alt="" /><a href="javascript:void(0)" onclick="deletePic(this)" class="delphoto">删除</a></div>';
			});
			$("#commodityPicDiv").html(htmls);
		} else {
			layer.alert(data.msg);
		}
	});
}

//保存编辑好的型号图片
$("#editCommoPic").click(function() {
	var divArr = $("#commodityPicDiv").children(".photo");
	if (!divArr || divArr.length == 0) {
		layer.alert("请先上传图片");
		return;
	}
	var url = "commodity/organCommodity/updateCommodityPic.ihtml";
	var json = '[';
	for (var i = 0; i < divArr.length; i++) {
		var picUrl = $($(divArr[i]).children("img")[0]).prop("src");
		picUrl = picUrl.substring(picUrl.lastIndexOf("commodity"), picUrl.length);
		var remark = $($(divArr[i]).children("input")[0]).val();
		json += '{"commoNo":"' + $("#commoNo").val() + '",';
		json += '"picUrl":"' + picUrl + '",';
		json += '"remark":"' + remark + '"}';
		json += i == (divArr.length - 1) ? '' : ',';
	}
	json += ']';
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			layer.alert(data.msg, function() {
				window.location.href = 'marque-detail.html?id=' + $("#id").val() + '&supperResId=' + request("supperResId");
			});
		} else {
			layer.alert(data.msg);
		}
	});
});

//添加商品型号
$("#saveMarque").click(function() {
	var commoName = $("#commoName").val();
	var commoEnName = $("#commoEnName").val();
	var catNo = $("#catNo").val();
	var catName = $("#catName").val();
	var brandNo = $("#brandNo").val();
	var brandName = $("#brandName").val();
	var isCode = $("#isCode").val();
	var json = '{"commoName":"' + commoName + '",';
	if (commoEnName) {
		json += '"commoEnName":"' + commoEnName + '",';
	}
	json += '"catNo":"' + catNo + '",';
	json += '"catName":"' + catName + '",';
	json += '"brandNo":"' + brandNo + '",';
	json += '"brandName":"' + brandName + '",';
	json += '"isCode":"' + isCode + '",';
	json += '"description":"' + editor.html() + '",';

	var trArr = $("#dataList1").children("tr");
	var commoPropList = "";
	for (var i = 0; i < trArr.length; i++) {
		var tdArr = $(trArr[i]).children("td");
		var propValue = "";
		$($(tdArr[1]).children("span")[0]).children("select").each(function(i, text) {
			if (text.value) {
				isProp = true;
				propValue += text.value + "/";
			}
		});
		if (!propValue) {
			break;
		}
		propValue = propValue.substring(0, propValue.length - 1);
		commoPropList += commoPropList ? ',{' : '{';
		commoPropList += '"propNo":"' + $(tdArr[0]).next().val() + '",';
		commoPropList += '"propName":"' + $(tdArr[0]).html() + '",';
		commoPropList += '"propValue":"' + propValue + '",';
		commoPropList += '"propType":"1"}';
	}
	if (!commoPropList) {
		layer.alert("请选择规格参数");
		return;
	}
	json += 'commoPropList:[' + commoPropList;

	trArr = $("#dataList2").children("tr");
	for (var i = 0; i < trArr.length; i++) {
		var tdArr = $(trArr[i]).children("td");
		var propValue = "";
		$($(tdArr[1]).children("span")[0]).children("select").each(function(i, text) {
			if (text.value) {
				isProp = true;
				propValue += text.value + "/";
			}
		});
		if (!propValue) {
			break;
		}
		propValue = propValue.substring(0, propValue.length - 1);
		json += ',{';
		json += '"propNo":"' + $(tdArr[0]).next().val() + '",';
		json += '"propName":"' + $(tdArr[0]).html() + '",';
		json += '"propValue":"' + propValue + '",';
		json += '"propType":"2"}';
	}
	json += '],';


	var divArr = $("#commodityPicDiv").children(".photo");
	if (!divArr || divArr.length == 0) {
		layer.alert("请先上传图片");
		return;
	}
	json += 'commoPicList:[';
	for (var i = 0; i < divArr.length; i++) {
		var picUrl = $($(divArr[i]).children("img")[0]).prop("src");
		picUrl = picUrl.substring(picUrl.lastIndexOf("commodity"), picUrl.length);
		var remark = $($(divArr[i]).children("input")[0]).val();
		json += '{"commoNo":"' + $("#commoNo").val() + '",';
		json += '"picUrl":"' + picUrl + '",';
		json += '"remark":"' + remark + '"}';
		json += i == (divArr.length - 1) ? '' : ',';
	}
	json += ']}';
	var url = "commodity/organCommodity/addCommodity.ihtml";
	RequestData(url, json, function(data) {
		if (data.code == "0") {
			layer.alert(data.msg, function() {
				parent.location.reload();
			});
		} else {
			layer.alert(data.msg);
		}
	});
});

//添加规格属性值
function addValue(obj) {
	$(obj).prev().append("<select style='width: 100px;'>" + ($($(obj).prev().children("select")[0]).html()) + "</select>");
}
