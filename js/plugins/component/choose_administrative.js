$(document).ready(function() {
				
    $('#province').on("change",function(selitem){
    	//重载城市信息
    	getinfo("city",$(this).val(),"city");
    	$("#county").html("<option value='0'>请选择县/区:</option>");
    	$("#town").html("<option value='0'>请选择乡镇/街道办:</option>");
    	$("#village").html("<option value='0'>请选择乡村/社区:</option>");
    });
    
	$('#city').on("change",function(selitem){
		//重载县区信息
		getinfo("county",$(this).val(),"county");
		$("#town").html("<option value='0'>请选择乡镇/街道办:</option>");
    	$("#village").html("<option value='0'>请选择乡村/社区:</option>");
    });
    
    $('#county').on("change",function(selitem){
    	//重载乡镇信息
    	getinfo("town",$(this).val(),"town");
    	$("#village").html("<option value='0'>请选择乡村/社区:</option>");
    });
    
    $('#town').on("change",function(selitem){
    	//重载乡村街道信息
    	getinfo("village",$(this).val(),"village");
    });

});

function getinfo(type,id,xobj){
	var url="system/administrative/info.ihtml";
	var data={};
	data["type"]=type;
	data["id"]=id;
	RequestData(url,JSON.stringify(data),function(data){
		if(data.obj){
			var options=[];
			if(type=="province"){
				options.push("<option value='0'>请选择省份:</option>");
			}else if(type=="city"){
				options.push("<option value='0'>请选择城市:</option>");
			}else if(type=="county"){
				options.push("<option value='0'>请选择县/区:</option>");
			}else if(type=="town"){
				options.push("<option value='0'>请选择乡镇/街道办:</option>");
			}else if(type=="village"){
				options.push("<option value='0'>请选择乡村/社区:</option>");
			}
    		$(data.obj).map(function() {
    			options.push("<option value='"+this.id+"'>"+this.name+"</option>");
            });
            //初始化选择框
            $("#"+xobj).html(options.join(''));
            //判断之前是否已经保存，如果保存，则需要逐级初始化
		    if(type=="province"){
				var x=$("#companyAddressProvince").val();
			    if(x&&x!=""){
			    	$("#province option:contains('"+x+"')").attr('selected', true).change();
			    }
			}else if(type=="city"){
				var x=$("#companyAddressCity").val();
			    if(x&&x!=""){
			    	$("#city option:contains('"+x+"')").attr('selected', true).change();
			    }
			}else if(type=="county"){
				var x=$("#companyAddressCounty").val();
			    if(x&&x!=""){
			    	$("#county option:contains('"+x+"')").attr('selected', true).change();
			    }
			}else if(type=="town"){
				var x=$("#companyAddressTown").val();
			    if(x&&x!=""){
			    	$("#town option:contains('"+x+"')").attr('selected', true).change();
			    }
			}else if(type=="village"){
				var x=$("#companyAddressSamll").val();
			    if(x&&x!=""){
			    	$("#village option:contains('"+x+"')").attr('selected', true).change();
			    }
			}
		}
	});
}