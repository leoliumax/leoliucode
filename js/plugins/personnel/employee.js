﻿
//加载下拉框数据
function queryEmployeePageLoad(){
	var url = "personnel/employee/queryEmployeePageLoad.ihtml";
	RequestData(url,'{}',function(data){	
        pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_PERSONNEL_EMPLOYEE_ADD"]){
			$("#add").html('<a href="employee-add.html?supperResId='+ supperResId +'" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_PERSONNEL_EMPLOYEE_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		for (var key in data.obj) {  
			if(key == "departmentVos"){   //部门
	        	var htmls = '<option value="-7">全部</option>';
	          	$.each(data.obj[key],function(n,departmentVo) {
	          		htmls +='<option value="'+ departmentVo.id +'">'+ departmentVo.departmentName +'</option>';
	          	});
	          	$("#departmentId").html(htmls);
	        }
        }		
		$("#search").click();
	});
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	var url = "personnel/employee/queryEmployeePage.ihtml";
	var json ='{';
	if($("#departmentId").val() && $("#departmentId").val() != "-7"){json+='"departmentId":"'+$("#departmentId").val()+'",';}
	if($("#postId").val()){json+='"postId":"'+$("#postId").val()+'",';}
	if($("#jobStatus").val() && $("#jobStatus").val() != "-7"){json+='"jobStatus":"'+$("#jobStatus").val()+'",';}
	if($("#name").val()){json+='"name":"'+$("#name").val()+'",';}
	if($("#mobile").val()){json+='"mobile":"'+$("#mobile").val()+'",';}
	if($("#systemUserAccount").val()){json+='"systemUserAccount":"'+$("#systemUserAccount").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,employeeVo) {
	      		htmls +='<tr>'
							+'<td>'+ (n+1) +'</td>'
							+'<td>'+ employeeVo.name +'</td>'
							+'<td>'+ employeeVo.mobile +'</td>'
							+'<td>'+ employeeVo.departmentName +'</td>'
							+'<td>'+ employeeVo.postName +'</td>'//href=\"employee-erp-account.html?employeeId="+ employeeVo.id +"&name="+ encodeURI(employeeVo.name) +"&userState="+ employeeVo.jobStatus +'&supperResId='+ supperResId +'
							+'<td>'+ (employeeVo.isCreateSystemUser==1?employeeVo.systemUserAccount:(pageButtonMap && pageButtonMap["RES_PERSONNEL_EMPLOYEE_ADD"])?"<a href='employee-erp-account.html?employeeId="+employeeVo.id+"&name="+encodeURI(employeeVo.name)+"&userState="+ employeeVo.jobStatus +"&supperResId="+ supperResId +"'>点击新增</a>":"") +'</td>'
							+'<td>'+ (employeeVo.jobStatus==1?"在职":"离职") +'</td>'
							+'<td>'+ (employeeVo.updateTime?employeeVo.updateTime:"")  +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_PERSONNEL_EMPLOYEE_DETAIL"]){
								htmls +='<a href="employee-detail.html?id='+ employeeVo.id +'&supperResId='+ supperResId +'">查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_PERSONNEL_EMPLOYEE_EDIT"]){
								htmls +='<a href="employee-modity.html?id='+ employeeVo.id +'&supperResId='+ supperResId +'">编辑</a>&nbsp;&nbsp;';
							}
							//验证是否有重置密码权限
							if(employeeVo.isCreateSystemUser==1 && pageButtonMap && pageButtonMap["RES_PERSONNEL_EMPLOYEE_PASSWORD"]){
								htmls +='<a href="employee-erp-password.html?employeeId='+ employeeVo.id +'&name='+ encodeURI(employeeVo.name) +'&account='+ employeeVo.id +'&systemUserAccount='+employeeVo.systemUserAccount +'&supperResId='+ supperResId +'">重置密码</a>';
							}
						htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	});
}


//员工详情
function employeeDetail(){
	var url = "personnel/employee/queryEmployeeInfo.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		employee = data.obj.employeeVo;
		$("#name").html("&nbsp;&nbsp;"+ employee.name);
		$("#gender").html("&nbsp;&nbsp;"+ (employee.gender==0?"女":employee.gender==0?"男":"未录入"));
		$("#birthDate").html("&nbsp;&nbsp;"+ (employee.birthDate?employee.birthDate.substring(0,10):""));
		$("#stature").html("&nbsp;&nbsp;"+ (employee.stature)?employee.stature:"未知");
		$("#maritalStatus").html("&nbsp;&nbsp;"+ employee.maritalStatus==0?"未婚":employee.maritalStatus==1?"已婚":"未婚");
		$("#cardId").html("&nbsp;&nbsp;"+ (employee.cardId)?employee.cardId:"未知");
		$("#nativePlace").html("&nbsp;&nbsp;"+ (employee.nativePlace)?employee.nativePlace:"未知");
		$("#folk").html("&nbsp;&nbsp;"+ (employee.folk)?employee.folk:"未知");
		$("#education").html("&nbsp;&nbsp;"+ (employee.education)?employee.education:"未知");
		$("#entryDate").html("&nbsp;&nbsp;"+ (employee.entryDate?employee.entryDate.substring(0,10):""));
		$("#mobile").html("&nbsp;&nbsp;"+ employee.mobile);
		$("#qq").html("&nbsp;&nbsp;"+ (employee.qq?employee.qq:""));
		$("#wechat").html("&nbsp;&nbsp;"+ (employee.wechat?employee.wechat:""));
		$("#email").html("&nbsp;&nbsp;"+ (employee.email?employee.email:""));
		$("#address").html("&nbsp;&nbsp;"+ (employee.address)?employee.address:"未知");
		$("#censusRegisterAddress").html("&nbsp;&nbsp;"+ (employee.censusRegisterAddress)?employee.address:"未知");
		if(employee.picture){$("#commodityPicDiv").html('<img src="' + getIP() + employee.picture + '" style="width:120px;height:150px;"/>');}
		var htmls = '';
		if(employee.employeeContactsVos){
			$.each(employee.employeeContactsVos, function(i,employeeContacts) {
				 if(employeeContacts.type == 1){
				 	$("#contactsName1").html("&nbsp;&nbsp;"+ employeeContacts.name);
				 	$("#relation1").html("&nbsp;&nbsp;"+ employeeContacts.relation);
				 	$("#contactsPhone1").html("&nbsp;&nbsp;"+ employeeContacts.phone);
				 }else{
				 	htmls += '<tr>'
					+'<td>'+ employeeContacts.name +'</td>'
					+'<td>'+ employeeContacts.phone +'</td>'
					+'<td>'+ employeeContacts.relation +'</td>'
					+'<td>'+ (employeeContacts.address?employeeContacts.address:"") +'</td></tr>';
				 }
			});
			$("#datalist3").html(htmls);
		}
		htmls = '';
		if(employee.employeeJobVos){
			$.each(employee.employeeJobVos, function(i,employeeJob) {
				htmls += '<tr>'
						+'<td>'+ employeeJob.startDate.substr(0,10) +'<span">&nbsp;~&nbsp;</span>'+ employeeJob.endDate.substr(0,10) +'</td>'
						+'<td>'+ employeeJob.companyName +'</td>'
						+'<td>'+ employeeJob.officePost +'</td>'
						+'<td>'+ employeeJob.reasonsLeaving +'</td></tr>';
			});
			$("#datalist1").html(htmls);	
		}
		
		if(employee.employeeEducationVos){
			htmls = '';
			$.each(employee.employeeEducationVos, function(i,employeeEducation) {    
				htmls +='<tr>'
						+'<td>'+ employeeEducation.startDate.substr(0,10) +'<span>&nbsp;~&nbsp;</span>'+ employeeEducation.endDate.substr(0,10) +'</td>'
						+'<td>'+ employeeEducation.schoolName +'</td>'
						+'<td>'+ employeeEducation.profession +'</td>'
						+'<td>'+ employeeEducation.degrees+'</td></tr>';
			});
			$("#datalist2").html(htmls);
		}
		$("#remark").html("&nbsp;&nbsp;"+ employee.remark);
		$("#createPerson").html("&nbsp;&nbsp;"+ employee.createPerson);
		$("#createTime").html("&nbsp;&nbsp;"+ employee.createTime);
		$("#updatePerson").html("&nbsp;&nbsp;"+ employee.updatePerson);
		$("#updateTime").html("&nbsp;&nbsp;"+ employee.updateTime);
		if(data.obj.pageButtonMap && data.obj.pageButtonMap["RES_PERSONNEL_EMPLOYEE_PRINT"]){
			$("#print").html('<a class="btn btn-default" >打印</a>');
		}

	});
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#organId").val()){json+='"organId":"'+$("#organId").val()+'",';}
	if($("#departmentId").val()){json+='"departmentId":"'+$("#departmentId").val()+'",';}
	if($("#jobStatus").val()){json+='"jobStatus":"'+$("#jobStatus").val()+'",';}
	if($("#postName").val()){json+='"postName":"'+$("#postName").val()+'",';}
	if($("#name").val()){json+='"name":"'+$("#name").val()+'",';}
	if($("#mobile").val()){json+='"mobile":"'+$("#mobile").val()+'",';}
	if($("#loginName").val()){json+='"loginName":"'+$("loginName").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	var url = getIP() + "personnel/employee/exportExcelEmployee.ihtml?data=" + encodeURIComponent(json);
	window.location.href = url ;
}

//添加页面加载数据
function addEmployeeLoad(){
	var url = "personnel/employee/toAddEmployee.ihtml";
	RequestData(url,'{}',function(data){
		$("#jsessionid").val(data.obj.jsessionid);
		for (var key in data.obj) {  
			if(key == "departmentVos"){   //部门
	        	var htmls = '<option value="-7">请选择</option>';
	          	$.each(data.obj[key],function(n,departmentVo) {
	          		htmls +='<option value="'+ departmentVo.id +'">'+ departmentVo.departmentName +'</option>';
	          	});
	          	$("#departmentId").html(htmls);
	        }
			if(key == "provinceVos"){   //籍贯
	        	var htmls = '<option value="-7">请选择</option>';
	          	$.each(data.obj[key],function(n,provinceVo) {
	          		htmls +='<option value="'+ provinceVo.name +'">'+ provinceVo.name +'</option>';
	          	});
	          	$("#nativePlace").html(htmls);
	        }
        }	
	});
	
	$(function(){
        var validate = $("#expen-from").validate({
            submitHandler: function(form){
            	if($("#gender").val() == "" || $("#gender").val() == "-7"){
            		layer.alert("请选择性别");
            		return;
            	}
            	if($("#maritalStatus").val() == "" || $("#maritalStatus").val() == "-7"){
            		layer.alert("请选择婚姻状态");
            		return;
            	}
            	if($("#nativePlace").val() == "-7" || !$("#nativePlace").val()){
            		layer.alert("请选择籍贯");
            		return;
            	}
            	if($("#folk").val() == "-7" || !$("#folk").val()){
            		layer.alert("请选择民族");
            		return;
            	}
            	if($("#education").val() == "-7" || !$("#education").val()){
            		layer.alert("请选择最高学历");
            		return;
            	}
            	if($("#jobStatus").val() == "-7" || !$("#jobStatus").val()){
            		layer.alert("请选择在职状态");
            		return;
            	}
            	if($("#departmentId").val() == "-7" || !$("#departmentId").val()){
            		layer.alert("请选择所属部门");
            		return;
            	}''
            	if($("#postId").val() == "-7" || !$("#postId").val()){
            		layer.alert("请选择岗位");
            		return;
            	}
                addEmployee(); //验证通过，执行添加员工
            },                   
            rules:{
                name:{required:true},       
                mobile:{required:true,digits:true,rangelength:[11,11]},
                cardId:{required:true,rangelength:[18,18]},
                profession:{required:true},
                entryDate:{required:true},
                address:{required:true},
                email:{email:true},
                qq:{digits:true,rangelength:[4,15]},
                censusRegisterAddress:{required:true},
                contactsName1:{required:true},
                address1:{required:true},
                contactsRelation1:{required:true},
                contactsPhone1:{required:true,digits:true,rangelength:[7,11]},        
                contactsPhone2:{digits:true,rangelength:[7,11]},
            },
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}                 
        });      
    });
}

//出生日期加载，自动获取省份证信息
$("#cardId").change(function(){
	var cardId = $("#cardId").val(); 
	if(cardId){$("#birthDate").val(cardId.substring(6, 10) + "-" + cardId.substring(10, 12) + "-" + cardId.substring(12, 14));}else{$("#birthDate").val("");} 
});

//添加员工
function addEmployee(){
	var url = "personnel/employee/addEmployee.ihtml;jsessionid="+$("#jsessionid").val();
	var json ='{';
	json+='"name":"'+$("#name").val()+'",';
	if($("#gender").val()){json+='"gender":"'+$("#gender").val()+'",'};
	if($("#birthDate").val()){json+='"birthDate":"'+$("#birthDate").val()+' 00:00:00",'};
	if($("#stature").val()){json+='"stature":"'+$("#stature").val()+'",'};
	if($("#maritalStatus").val()){json+='"maritalStatus":"'+$("#maritalStatus").val()+'",'};
	if($("#cardId").val()){json+='"cardId":"'+$("#cardId").val()+'",'};
	if($("#nativePlace").val()){json+='"nativePlace":"'+$("#nativePlace").val()+'",'};
	if($("#folk").val()){json+='"folk":"'+$("#folk").val()+'",'};
	if($("#education").val()){json+='"education":"'+$("#education").val()+'",'};
	if($("#profession").val()){json+='"profession":"'+$("#profession").val()+'",'};
	var entryDate = $("#entryDate").val();
	entryDate = entryDate.replace("年","-");
	entryDate = entryDate.replace("月","-");
	entryDate = entryDate.replace("日","");
	if($("#picture")){json+='"picture":"' + $("#picture").attr("property") + '",';}
	
	if($("#entryDate").val()){json+='"entryDate":"'+entryDate+' 00:00:00",'};
	if($("#mobile").val()){json+='"mobile":"'+$("#mobile").val()+'",'};
	if($("#email").val()){json+='"email":"'+$("#email").val()+'",'};
	if($("#qq").val()){json+='"qq":"'+$("#qq").val()+'",'};
	if($("#wechat").val()){json+='"wechat":"'+$("#wechat").val()+'",'};
	if($("#address").val()){json+='"address":"'+$("#address").val()+'",'};
	if($("#censusRegisterAddress").val()){json+='"censusRegisterAddress":"'+$("#censusRegisterAddress").val()+'",'};
	if($("#jobStatus").val()){json+='"jobStatus":"'+$("#jobStatus").val()+'",'};
	json+='"departmentId":"'+$("#departmentId").val()+'",';
	json+='"departmentName":"'+$("#departmentId").find('option:selected').text() +'",';
	json+='"postId":"'+$("#postId").val()+'",';
	json+='"postName":"'+$("#postId").find('option:selected').text() +'",';
	//联系人信息
	var employeeContactsVos = "";
		if($("#contactsName2").val()){employeeContactsVos += '{"type":"2","name":"'+$("#contactsName2").val() +'","relation":"'+$("#contactsRelation2").val() +'","phone":"'+$("#contactsPhone2").val() +'","address":"'+$("#address2").val() +'"},';}
		if($("#contactsName3").val()){employeeContactsVos += '{"type":"2","name":"'+$("#contactsName3").val() +'","relation":"'+$("#contactsRelation3").val() +'","phone":"'+$("#contactsPhone3").val() +'","address":"'+$("#address3").val() +'"},';}
		employeeContactsVos += '{"type":"1","name":"'+$("#contactsName1").val() +'","relation":"'+$("#contactsRelation1").val() +'","phone":"'+$("#contactsPhone1").val() +'","address":"'+$("#address1").val() +'"}';
		json+='"employeeContactsVos":['+ employeeContactsVos +'],';
	
	//教育经历
	var employeeEducationVos = "";
		if($("#edu_schoolName1").val()){employeeEducationVos += '{"schoolName":"'+$("#edu_schoolName1").val() +'","profession":"'+$("#edu_profession1").val() +'","degrees":"'+$("#edu_degrees1").val() +'","startDate":"'+$("#edu_startDate1").val() +' 00:00:00","endDate":"'+$("#edu_endDate1").val() +' 00:00:00"},';}
		if($("#edu_schoolName2").val()){employeeEducationVos += '{"schoolName":"'+$("#edu_schoolName2").val() +'","profession":"'+$("#edu_profession2").val() +'","degrees":"'+$("#edu_degrees2").val() +'","startDate":"'+$("#edu_startDate2").val() +' 00:00:00","endDate":"'+$("#edu_endDate2").val() +' 00:00:00"}';}
		json+='"employeeEducationVos":['+ employeeEducationVos +'],';
		
	//工作经历
	var employeeJobVos = '';
		if($("#job_companyName1").val()){employeeJobVos += '{"companyName":"'+$("#job_companyName1").val() +'","officePost":"'+$("#job_officePost1").val() +'","reasonsLeaving":"'+$("#job_reasonsLeaving1").val() +'","startDate":"'+$("#job_startDate1").val() +' 00:00:00","endDate":"'+$("#job_endDate1").val() +' 00:00:00"},';}
		if($("#job_companyName2").val()){employeeJobVos += '{"companyName":"'+$("#job_companyName2").val() +'","officePost":"'+$("#job_officePost2").val() +'","reasonsLeaving":"'+$("#job_reasonsLeaving2").val() +'","startDate":"'+$("#job_startDate2").val() +' 00:00:00","endDate":"'+$("#job_endDate2").val() +' 00:00:00"}';}
		json+='"employeeJobVos":['+ employeeJobVos +'],';
	
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData2(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				window.location.href = "employee-list.html?supperResId="+ supperResId;
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加ERP账号页面加载数据
function addEmployeeLoginLoad(){
	$("#employeeId").val(request("employeeId"));
	$("#name").html(decodeURI(request("name")));
	$("#userState").val(request("userState"));
	
	$(function(){
        var validate = $("#addEmployeeLogin").validate({
            onkeyup: false,   
            focusCleanup: true,
            submitHandler: function(form){
                addEmployeeLogin(); //验证通过，执行添加员工ERP登录账号
            },                   
            rules:{
                loginName:{required:true,minlength:4},        
                loginPassword:{required:true,rangelength:[6,20]},        
                loginPassword2:{required:true,equalTo:"#loginPassword"}       
            },
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}                 
        });      
    });
    
    $("#addLogin").click(function(){
    	var labels = $(".col-error");
    	for(i=0;i<labels.length;i++){
	         labels[i].innerHTML = "";
    	} 
    });
}

//添加员工ERP登录账号
function addEmployeeLogin(){
	var url = "personnel/employee/addEmployeeErp.ihtml";
	var json ='{';
	json+='"userName":"'+$("#name").html()+'",';
	json+='"loginName":"'+$("#loginName").val()+'",';
	json+='"loginPassword":"'+$("#loginPassword").val()+'",';
	json+='"userState":"'+$("#userState").val()+'",';
	json+='"employeeId":"'+$("#employeeId").val()+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				window.location.href = "employee-list.html?supperResId="+ supperResId;
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//重置ERP账号密码页面加载数据
function updateEmployeeLoginLoad(){
	$("#employeeId").val(request("employeeId"));
	$("#name").html(decodeURI(request("name")));
	$("#account").html(decodeURI(request("systemUserAccount")));
	$("#userState").val(request("userState"));
	$(function(){
        var validate = $("#updateEmployeeLogin").validate({
            onkeyup: false,   
            focusCleanup: true,
            submitHandler: function(form){
                updateEmployeeAccount(); //验证通过，执行添加员工ERP登录账号
            },                   
            rules:{
                loginPassword:{required:true,rangelength:[6,20]},        
                loginPassword2:{required:true,equalTo:"#loginPassword"}       
            },
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}                 
        });      
    });
    
//  $("#addLogin").click(function(){
//  	var labels = $(".col-error");
//  	for(i=0;i<labels.length;i++){
//	         labels[i].innerHTML = "";
//  	} 
//  });
}

//重置员工ERP账号密码
function updateEmployeeAccount(){
	var url = "personnel/employee/updateEmployeeErpPassword.ihtml";
	var json ='{';
	json+='"loginPassword":"'+$("#loginPassword").val()+'",';
	json+='"employeeId":"'+$("#employeeId").val()+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.location.reload();
			});
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面部门发生改变清空岗位信息
$("#supperSupperRes").on( "autocompletechange", function( event, ui ) {
	if($("#departmentId").val()){
	loading("正在加载部门岗位信息...");
	$("#supperSupperPost").find("input").val("请选择");
		var url = "personnel/post/queryPostListByDepartmentId.ihtml";
		var json ='{"departmentId":"'+ $("#departmentId").val() +'"}';
		RequestData(url,json,function(data){
			unloading();
			var htmls = '<option value="">请选择</option>';
            $.each(data.obj,function(n,post) {
          		htmls +='<option value="'+ post.id +'">'+ post.postName +'</option>';
          	});
            $("#postId").html(htmls);
		});
	}
});	


//编辑页面加载数据
function editEmployeeLoad(){
	var url = "personnel/employee/toEditEmployee.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		$("#jsessionid").val(data.obj.jsessionid);
		if(data.obj["departmentVos"]){   //部门
        	var htmls = '<option value="">请选择</option>';
          	$.each(data.obj["departmentVos"],function(n,departmentVo) {
          		htmls +='<option value="'+ departmentVo.id +'"  '+ (data.obj["employeeVo"].employeePostVos[0].departmentId==departmentVo.id?"selected=\"selected\"":"") +'>'+ departmentVo.departmentName +'</option>';
          	});
          	$("#departmentId").html(htmls);
        }
		
		if(data.obj["provinceVos"]){   //籍贯
        	var htmls = '<option value="">请选择</option>';
          	$.each(data.obj["provinceVos"],function(n,provinceVo) {
          		htmls +='<option value="'+ provinceVo.name +'">'+ provinceVo.name +'</option>';
          	});
          	$("#nativePlace").html(htmls);
        }
		
		//岗位数据
		var htmls = '<option value="">请选择</option>';
        $.each(data.obj.postVos,function(n,post) {
      		htmls +='<option value="'+ post.id +'" '+ (data.obj["employeeVo"].employeePostVos[0].postId==post.id?"selected=\"selected\"":"") +'>'+ post.postName +'</option>';
      	});
        $("#postId").html(htmls);
		
		if(data.obj["employeeVo"]){   //员工信息
        	$("#id").val(data.obj["employeeVo"].id);
        	$("#name").val(data.obj["employeeVo"].name);
        	$("#gender").find('option[value="'+ data.obj["employeeVo"].gender +'"]').attr('selected','selected');
        	$("#birthDate").val((data.obj["employeeVo"].birthDate?data.obj["employeeVo"].birthDate.substring(0,10):""));
        	$("#stature").val(data.obj["employeeVo"].stature);
        	$("#maritalStatus").find('option[value="'+ data.obj["employeeVo"].maritalStatus +'"]').attr('selected','selected');
        	$("#cardId").val(data.obj["employeeVo"].cardId);
        	$("#nativePlace").find('option[value="'+ data.obj["employeeVo"].nativePlace +'"]').attr('selected','selected');
        	$("#folk").find('option[value="'+ data.obj["employeeVo"].folk +'"]').attr('selected','selected');
        	$("#education").find('option[value="'+ data.obj["employeeVo"].education +'"]').attr('selected','selected');
        	$("#profession").val(data.obj["employeeVo"].profession);
        	var entryDate = data.obj["employeeVo"].entryDate;
        	entryDate = entryDate.substr(0,10);
			entryDate = entryDate.replace("-","年");
			entryDate = entryDate.replace("-","月");
			entryDate += '日';
			if(data.obj["employeeVo"].picture){$("#commodityPicDiv").html('<img src="' + getIP() + data.obj["employeeVo"].picture + '" id="picture" property="' + data.obj["employeeVo"].picture + '" alt="" style="width:120px;height:150px;"/>');}
			
        	$("#entryDate").val(entryDate);
        	$("#mobile").val(data.obj["employeeVo"].mobile);
        	$("#email").val(data.obj["employeeVo"].email);
        	$("#qq").val(data.obj["employeeVo"].qq);
        	$("#wechat").val(data.obj["employeeVo"].wechat);
        	$("#address").val(data.obj["employeeVo"].address);
        	$("#censusRegisterAddress").val(data.obj["employeeVo"].censusRegisterAddress);
        	$("#organId").val(data.obj["employeeVo"].organId);
        	$("#jobStatus").find('option[value="'+ data.obj["employeeVo"].jobStatus +'"]').attr('selected','selected');
        	$("#remark").val(data.obj["employeeVo"].remark);
        	$("#versionLock").val(data.obj["employeeVo"].versionLock);
        	
			//联系人信息
			if(data.obj.employeeVo.employeeContactsVos){
				var index = 1;
				$.each(data.obj["employeeVo"].employeeContactsVos,function(n,employeeContact) {
	          		if(employeeContact && employeeContact.type == 1){
	          			$("#contactsName1").val(employeeContact.name);
	          			$("#contactsRelation1").val(employeeContact.relation);
	          			$("#contactsPhone1").val(employeeContact.phone);
	          			$("#address1").val(employeeContact.address);
	          		}else if(index == 1){
	          			$("#contactsName2").val(employeeContact.name);
	          			$("#contactsRelation2").val(employeeContact.relation);
	          			$("#contactsPhone2").val(employeeContact.phone);
	          			$("#address2").val(employeeContact.address);
	          			index++ ;
	          		}else{
	          			$("#contactsName3").val(employeeContact.name);
	          			$("#contactsRelation3").val(employeeContact.relation);
	          			$("#contactsPhone3").val(employeeContact.phone);
	          			$("#address3").val(employeeContact.address);
	          		}
	          	});
			}
			
			//工作经历
			if(data.obj.employeeVo.employeeContactsVos){
				var index = 1;
				$.each(data.obj.employeeVo.employeeJobVos,function(n,employeeJob) {
	          		if(n == 0){
	          			$("#job_companyName1").val(employeeJob.companyName);
	          			$("#job_officePost1").val(employeeJob.officePost);
	          			$("#job_reasonsLeaving1").val(employeeJob.reasonsLeaving);
	          			$("#job_startDate1").val(employeeJob.startDate.substr(0,10));
	          			$("#job_endDate1").val(employeeJob.endDate.substr(0,10));
	          		}else{
	          			$("#job_companyName2").val(employeeJob.companyName);
	          			$("#job_officePost2").val(employeeJob.officePost);
	          			$("#job_reasonsLeaving2").val(employeeJob.reasonsLeaving);
	          			$("#job_startDate2").val(employeeJob.startDate.substr(0,10));
	          			$("#job_endDate2").val(employeeJob.endDate.substr(0,10));
	          		}
	          	});
			}
			
			//教育程度
			if(data.obj.employeeVo.employeeEducationVos){
				var index = 1;
				$.each(data.obj.employeeVo.employeeEducationVos,function(n,employeeEducation) {
	          		if(n == 0){
	          			$("#edu_schoolName1").val(employeeEducation.schoolName);
	          			$("#edu_profession1").val(employeeEducation.profession);
	          			$("#edu_degrees1").find('option[value="'+ employeeEducation.degrees +'"]').attr('selected','selected');
	          			$("#edu_startDate1").val(employeeEducation.startDate.substr(0,10));
	          			$("#edu_endDate1").val(employeeEducation.endDate.substr(0,10));
	          		}else{
	          			$("#edu_schoolName2").val(employeeEducation.schoolName);
	          			$("#edu_profession2").val(employeeEducation.profession);
	          			$("#edu_degrees2").find('option[value="'+ employeeEducation.degrees +'"]').attr('selected','selected');
	          			$("#edu_startDate2").val(employeeEducation.startDate.substr(0,10));
	          			$("#edu_endDate2").val(employeeEducation.endDate.substr(0,10));
	          		}
	          	});
			}
        }
	});
	
	$(function(){
        var validate = $("#expen-from").validate({
            submitHandler: function(form){
            	if($("#gender").val() == "" || $("#gender").val() == "-7"){
            		layer.alert("请选择性别");
            		return;
            	}
            	if($("#maritalStatus").val() == "" || $("#maritalStatus").val() == "-7"){
            		layer.alert("请选择婚姻状态");
            		return;
            	}
            	if($("#nativePlace").val() == "-7" || !$("#nativePlace").val()){
            		layer.alert("请选择籍贯");
            		return;
            	}
            	if($("#folk").val() == "-7" || !$("#folk").val()){
            		layer.alert("请选择民族");
            		return;
            	}
            	if($("#education").val() == "-7" || !$("#education").val()){
            		layer.alert("请选择最高学历");
            		return;
            	}
            	if($("#jobStatus").val() == "-7" || !$("#jobStatus").val()){
            		layer.alert("请选择在职状态");
            		return;
            	}
            	if($("#departmentId").val() == "-7" || !$("#departmentId").val()){
            		layer.alert("请选择所属部门");
            		return;
            	}''
            	if($("#postId").val() == "-7" || !$("#postId").val()){
            		layer.alert("请选择岗位");
            		return;
            	}
                editEmployee(); //验证通过，执行编辑员工
            },                   
            rules:{
                name:{required:true},       
                mobile:{required:true,digits:true,rangelength:[11,11]},
                cardId:{required:true,rangelength:[18,18]},
                profession:{required:true},
                entryDate:{required:true},
                address:{required:true},
                email:{email:true},
                qq:{digits:true,rangelength:[4,15]},
                censusRegisterAddress:{required:true},
                contactsName1:{required:true},
                address1:{required:true},
                contactsRelation1:{required:true},
                contactsPhone1:{required:true,digits:true,rangelength:[7,11]},        
                contactsPhone2:{digits:true,rangelength:[7,11]},
            },
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}    
        });      
    });
    
    $("#addWarehouse").click(function(){
    	var labels = $(".col-error");
    	for(i=0;i<labels.length;i++){
	         labels[i].innerHTML = "";
    	} 
    });
}


//编辑员工信息
function editEmployee(){
	var url = "personnel/employee/editEmployee.ihtml;jsessionid="+ $("#jsessionid").val() ;
	var json ='{';
	json+='"id":"'+ $("#id").val() +'","name":"'+$("#name").val()+'",';
	if($("#gender").val()){json+='"gender":"'+$("#gender").val()+'",'};
	if($("#birthDate").val()){json+='"birthDate":"'+$("#birthDate").val()+' 00:00:00",'};
	if($("#stature").val()){json+='"stature":"'+$("#stature").val()+'",'};
	if($("#maritalStatus").val()){json+='"maritalStatus":"'+$("#maritalStatus").val()+'",'};
	if($("#cardId").val()){json+='"cardId":"'+$("#cardId").val()+'",'};
	if($("#nativePlace").val()){json+='"nativePlace":"'+$("#nativePlace").val()+'",'};
	if($("#folk").val()){json+='"folk":"'+$("#folk").val()+'",'};
	if($("#education").val()){json+='"education":"'+$("#education").val()+'",'};
	var entryDate = $("#entryDate").val();
	entryDate = entryDate.replace("年","-");
	entryDate = entryDate.replace("月","-");
	entryDate = entryDate.replace("日","");
	if($("#picture")){json+='"picture":"' + $("#picture").attr("property") + '",';}
	if($("#entryDate").val()){json+='"entryDate":"'+entryDate+' 00:00:00",'};
	if($("#mobile").val()){json+='"mobile":"'+$("#mobile").val()+'",'};
	if($("#email").val()){json+='"email":"'+$("#email").val()+'",'};
	if($("#qq").val()){json+='"qq":"'+$("#qq").val()+'",'};
	if($("#wechat").val()){json+='"wechat":"'+$("#wechat").val()+'",'};
	if($("#address").val()){json+='"address":"'+$("#address").val()+'",'};
	if($("#censusRegisterAddress").val()){json+='"censusRegisterAddress":"'+$("#censusRegisterAddress").val()+'",'};
	if($("#jobStatus").val()){json+='"jobStatus":"'+$("#jobStatus").val()+'",'};
	json+='"departmentId":"'+$("#departmentId").val()+'",';
	json+='"departmentName":"'+$("#departmentId").find('option:selected').text() +'",';
	json+='"postId":"'+$("#postId").val()+'",';
	json+='"postName":"'+$("#postId").find('option:selected').text() +'",';
	//联系人信息
	var employeeContactsVos = "";
		if($("#contactsName2").val()){employeeContactsVos += '{"type":"2","name":"'+$("#contactsName2").val() +'","relation":"'+$("#contactsRelation2").val() +'","phone":"'+$("#contactsPhone2").val() +'","address":"'+$("#address2").val() +'"},';}
		if($("#contactsName3").val()){employeeContactsVos += '{"type":"2","name":"'+$("#contactsName3").val() +'","relation":"'+$("#contactsRelation3").val() +'","phone":"'+$("#contactsPhone3").val() +'","address":"'+$("#address3").val() +'"},';}
		employeeContactsVos += '{"type":"1","name":"'+$("#contactsName1").val() +'","relation":"'+$("#contactsRelation1").val() +'","phone":"'+$("#contactsPhone1").val() +'","address":"'+$("#address1").val() +'"}';
		json+='"employeeContactsVos":['+ employeeContactsVos +'],';
	
	//教育经历
	var employeeEducationVos = "";
		if($("#edu_schoolName1").val()){employeeEducationVos += '{"schoolName":"'+$("#edu_schoolName1").val() +'","profession":"'+$("#edu_profession1").val() +'","degrees":"'+$("#edu_degrees1").val() +'","startDate":"'+$("#edu_startDate1").val() +' 00:00:00","endDate":"'+$("#edu_endDate1").val() +' 00:00:00"},';}
		if($("#edu_schoolName2").val()){employeeEducationVos += '{"schoolName":"'+$("#edu_schoolName2").val() +'","profession":"'+$("#edu_profession2").val() +'","degrees":"'+$("#edu_degrees2").val() +'","startDate":"'+$("#edu_startDate2").val() +' 00:00:00","endDate":"'+$("#edu_endDate2").val() +' 00:00:00"}';}
		json+='"employeeEducationVos":['+ employeeEducationVos +'],';
		
	//工作经历
	var employeeJobVos = '';
		if($("#job_companyName1").val()){employeeJobVos += '{"companyName":"'+$("#job_companyName1").val() +'","officePost":"'+$("#job_officePost1").val() +'","reasonsLeaving":"'+$("#job_reasonsLeaving1").val() +'","startDate":"'+$("#job_startDate1").val() +' 00:00:00","endDate":"'+$("#job_endDate1").val() +' 00:00:00"},';}
		if($("#job_companyName2").val()){employeeJobVos += '{"companyName":"'+$("#job_companyName2").val() +'","officePost":"'+$("#job_officePost2").val() +'","reasonsLeaving":"'+$("#job_reasonsLeaving2").val() +'","startDate":"'+$("#job_startDate2").val() +' 00:00:00","endDate":"'+$("#job_endDate2").val() +' 00:00:00"}';}
		json+='"employeeJobVos":['+ employeeJobVos +'],';
	
	json+='"remark":"'+$("#remark").val()+'","versionLock":"'+ $("#versionLock").val() +'"}';
	loading("正在加载数据.....");
	RequestData2(url,json,function(data){
		unloading();
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				window.location.href = "employee-list.html?supperResId="+ supperResId;
			});
		}else{
			layer.alert(data.msg);
		}
	});
}