﻿//加载列表页面数据
function pageDepartmentLoad(){
	var url = "personnel/department/pageDepartmentLoad.ihtml";
	RequestData(url,'{}',function(data){	
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_PERSONNEL_DEPARTMENT_ADD"]){
			var _url = 'department-add.html';
			$("#add").html('<a onclick="openPage(\''+_url+'\',\'新增部门\')" href="javascript:void(0)" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_PERSONNEL_DEPARTMENT_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#pageNo").val(1);
		page();
	});
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在查询...");
	var url = "personnel/department/pageDepartment.ihtml";
	var json ='{';
	if($("#organType").val()){json+='"organType":"'+$("#organType").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			var number=1;
			$.each(data.obj.rows,function(n,department) {
	      		htmls +='<tr>'
	      					+'<td>'+ (number++) +'</td>'
							+'<td>'+ department.departmentCode +'</td>'
							+'<td>'+ department.departmentName +'</td>'
							+'<td>'+ department.text3+'</td>'
							+'<td>'+ (department.status==1?"启用":"禁用") +'</td>'
							+'<td>'+ department.updateTime +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_PERSONNEL_DEPARTMENT_DETAIL"]){
								var _url = 'department-detail.html?id='+ department.id;
								htmls +='<a onclick="openPage(\''+_url+'\',\'查看部门详情\')" href="javascript:void(0)">查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_PERSONNEL_DEPARTMENT_EDIT"]){
								var _url = 'department-modity.html?id='+ department.id;
								htmls +='<a onclick="openPage(\''+_url+'\',\'编辑部门详情\')" href="javascript:void(0)">编辑</a>';
							}
						+'</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		unloading();
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	});
}

//导出EXCEL
function exportExcel(){
	var json ='{';
	if($("#organId").val()){json+='"organId":"'+$("#organId").val()+'",';}
	json +='"pageNo":1}';
	var url = getIP() + "personnel/department/exportExcelDepartment.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
}

//查看部门详情
function departmentDetail(){
	var url = "personnel/department/departmentDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			$("#id").append("&nbsp;"+data.obj.departmentCode);
			$("#departmentName").html("&nbsp;&nbsp;"+data.obj.departmentName);
			$("#departmentType").html("&nbsp;&nbsp;"+data.obj.text3);
			$("#organName").html("&nbsp;&nbsp;"+data.obj.organName);
			$("#status").html("&nbsp;&nbsp;"+ (data.obj.status==1?"启用":"禁用"));
			$("#text2").html("&nbsp;&nbsp;"+(data.obj.text2?data.obj.text2:"无"));
			$("#createPerson").html("&nbsp;&nbsp;"+data.obj.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+data.obj.createTime);
			$("#updatePerson").html("&nbsp;&nbsp;"+data.obj.updatePerson);
			$("#updateTime").html("&nbsp;&nbsp;"+data.obj.updateTime);
			var htmls = '';
			if(data.obj.depPersonnelList){
				$.each(data.obj.depPersonnelList,function(n,depPersonnel) {
		      		htmls +='<tr>'
								+'<td>'+ depPersonnel.postName +'</td>'
								+'<td>'+ depPersonnel.personnelNum +'</td>'
							+'</tr>';
		      });
		    }
			$("#dataList").html(htmls);
		}else{
			layer.alert(data.msg);
		}
	});
}

//添加页面加载数据
function addDepartmentLoad(){
	var url = "personnel/department/toAddDepartment.ihtml";
	RequestData(url,'{}',function(data){
		if(data.obj) {  
			var htmls = '<option value="-7">请选择</option>';
            $.each(data.obj,function(n,departmentTypeVo) {
          		htmls +='<option value="'+ departmentTypeVo.typeNo +'">'+ departmentTypeVo.typeName +'</option>';
          	});
        	$("#departmentType").html(htmls);
        }
	});
	$(function(){
		var validate = $("#expen-from").validate({
	        rules:{
	            departmentName:{required:true,minlength:2}       
	        },
	        submitHandler: function(form){
	        	if(!$("#departmentType").val() || $("#departmentType").val() == '-7'){
					layer.alert("请选择部门类型");
					return;
				}
				var isValiate = false;
				var isValiate1 = false;
				$('input[name="personnelNum"]').each(function(i,input){
					var personnelNum = $(input).val();
					if(!personnelNum || personnelNum.length == 0){
						isValiate = true;
						return;
					}
					if((/^\d+$/).test(personnelNum) == false){
						isValiate1 = true;
						return;
					}
				});
				if($('input[name="personnelNum"]').length<1){
					layer.alert("请选择岗位编制");
					return;
				}else{
					if(isValiate){
						layer.alert("编制人数不能为空");
						return;
					}
					if(isValiate1){
						layer.alert("岗位编制必须为正整数");
						return;
					}
				}
				addDepartment();
	        },  
	        errorPlacement: function(error, element) {
	        	element.next().html("");
				error.appendTo(element.next()); 
			}                 
	    });
	});
}

//添加部门
function addDepartment(){
	loading("正在提交本门信息...");
	var url = "personnel/department/addDepartment.ihtml";
	var json ='{';
	json+='"departmentCode":"0000",';
	json+='"departmentName":"'+$("#departmentName").val()+'",';
	json+='"departmentType":"'+$("#departmentType").val()+'",';
	json+='"text3":"'+$("#departmentType").find('option:selected').text()+'",';
	json+='"text2":"'+$("#text2").val()+'",';
	json+='"status":"'+$("#status").val()+'",';
	json+='"depPersonnelList":[';
		var postNameArray = $(".postName");
		var personnelNumArray = $('input[name="personnelNum"]');
		$('input[name="postId"]').each(function(i,input){
			var postId = $(input).val();
			var postName = $(postNameArray[i]).html();
			var personnelNum = $(personnelNumArray[i]).val();
			if(i == 0){
				json +='{"postCode":"'+ postId +'","postName":"'+ postName +'","personnelNum":"'+ personnelNum +'"}';
			}else{
				json +=',{"postCode":"'+ postId +'","postName":"'+ postName +'","personnelNum":"'+ personnelNum +'"}';
			}
		});
	json+=']}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.window.page();
				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
				parent.layer.close(index);
				unloading();
			});
		}else{
			layer.alert(data.msg);
			unloading();
		}
	});
}

//编辑页面加载数据
function editDepartmentLoad(){
	var url = "personnel/department/editDepartmentLoad.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			if(data.obj["departmentTypeVos"]) {  
				var htmls = '<option value="-7">请选择</option>';
	            $.each(data.obj["departmentTypeVos"],function(n,departmentTypeVo) {
	          		htmls +='<option value="'+ departmentTypeVo.typeNo +'" '+(data.obj["departmentVo"].departmentType==departmentTypeVo.typeNo?"selected=\"selected\"":"")+'>'+ departmentTypeVo.typeName +'</option>';
	          	});
	        	$("#departmentType").html(htmls);
	        }
			$("#id").val(data.obj["departmentVo"].id);
			$("#versionLock").val(data.obj["departmentVo"].versionLock);
			$("#departmentName").val(data.obj["departmentVo"].departmentName);
//			$("#departmentType").find("option[value='"+ data.obj["departmentVo"].departmentType +"']").attr("selected","selected");
			$("#status").val(data.obj["departmentVo"].status);
			$("#text2").html(data.obj["departmentVo"].text2);
			var htmls = '';
			if(data.obj["departmentVo"].depPersonnelList){
				$.each(data.obj["departmentVo"].depPersonnelList,function(n,depPersonnel) {
		      		htmls +='<tr><input type="hidden" name="postId" id="'+ depPersonnel.postCode +'" value="'+ depPersonnel.postCode +'"/>'
								+'<td class="postName">'+ depPersonnel.postName +'</td>'
								+'<td><input type="text" name="personnelNum" value="'+ depPersonnel.personnelNum+'" class="g-mar-l11 g-h30"></td>'
								+'<td><a href="javascript:void(0)" onclick="deleteNote(this)">删除</a></td>'
							+'</tr>';
		      });
		    }
			$("#dataList").html(htmls);
		}else{
			layer.alert(data.msg);
		}
	});
	
	$(function(){
        var validate = $("#expen-from").validate({
            rules:{
	            departmentName:{required:true,minlength:2}       
	        },
	        submitHandler: function(form){
	        	if(!$("#departmentType").val() || $("#departmentType").val() == '-7'){
					layer.alert("请选择部门类型");
					return;
				}
				var isValiate = false;
				var isValiate1 = false;
				$('input[name="personnelNum"]').each(function(i,input){
					var personnelNum = $(input).val();
					if((/^\d+$/).test(personnelNum) == false){
						isValiate1 = true;
						return;
					}
					if(!personnelNum || personnelNum.length == 0){
						isValiate = true;
						return;
					}
				});
				if($('input[name="personnelNum"]').length<1){
					layer.alert("请选择岗位编制");
					return;
				}else{
					if(isValiate){
						layer.alert("编制人数不能为空");
						return;
					}
					if(isValiate1){
						layer.alert("岗位编制必须为正整数");
						return;
					}
				}
				editDepartment();
	        },  
	        errorPlacement: function(error, element) {
	        	element.next().html("");
				error.appendTo(element.next()); 
			}                
        });      
    });
}

//编辑部门
function editDepartment(){
	loading("正在提交数据...");
	var url = "personnel/department/updateDepartment.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"departmentName":"'+$("#departmentName").val()+'",';
	json+='"departmentType":"'+$("#departmentType").val()+'",';
	json+='"text3":"'+$("#departmentType").find('option:selected').text()+'",';
	json+='"status":"'+$("#status").val()+'",';
	json+='"text2":"'+$("#text2").val()+'",';
	json+='"depPersonnelList":[';
	var postNameArray = $(".postName");
	var personnelNumArray = $('input[name="personnelNum"]');
	$('input[name="postId"]').each(function(i,input){
		var postId = $(input).val();
		var postName = $(postNameArray[i]).html();
		var personnelNum = $(personnelNumArray[i]).val();
		if(i == 0){
			json +='{"postCode":"'+ postId +'","postName":"'+ postName +'","personnelNum":"'+ personnelNum +'"}';
		}else{
			json +=',{"postCode":"'+ postId +'","postName":"'+ postName +'","personnelNum":"'+ personnelNum +'"}';
		}
	});
	json+=']}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.window.page();
				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
				parent.layer.close(index);
				unloading();
			});
		}else{
			layer.alert(data.msg);
			unloading();
		}
	});
}

//更换部门类型，将提出警告
var selectedIndex = 0;
$("#supperSupperRes").on( "autocompletechange", function( event, ui ) {
//$("#departmentType").change(function(){
	if(($("#departmentType").val() || $("#departmentType").val()!="")){
		iframe1.window.page();
	}
	var sele = $("#departmentType").get(0).selectedIndex;
	if( selectedIndex == 0){
		selectedIndex = sele;
	}
	if($("#dataList > tr").length > 0){
		var result = confirm("改变部门类型将清空所有岗位编制信息");
		if(result){
			$("#dataList").html("");
			selectedIndex = sele;
			if($("#departmentType").val() || $("#departmentType").val()==""){
				iframe1.window.page();
			}
		}else{
			$("#departmentType").get(0).selectedIndex = selectedIndex;
		}
	}
});

//添加部门对应岗位信息
function addDepartmentPost(postId,postName){
	$("#dataList").append('<tr><input type="hidden" name="postId" id="'+ postId +'" value="'+ postId +'"/><td class="postName">'+ postName +'</td><td><input type="text" name="personnelNum" class="g-mar-l11 g-h30"></td><td><a href="javascript:void(0)" onclick="deleteNote(this)">删除</a></td></tr>');
}


//添加部门对应岗位信息，删除操作
function deleteNote(note){
	note.parentNode.parentNode.parentNode.removeChild(note.parentNode.parentNode);
	iframe1.window.queryPostLoad();
}

//新增按钮点击事件管理，弹出提示
function addDepartmentBtn(){
	layer.open({
	    type: 2,
	    area: ['95%', '95%'],
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: 'department-post-list.html'
	});
}

function openPage(url,title){
	layer.open({
	    type: 2, //page层
	    area: ['70%', '80%'],
	    title: title,
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    content: url
	});
}