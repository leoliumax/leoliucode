﻿//初始化页面
function pageLoad(){
	var url = "personnel/post/toPagePost.ihtml";
	RequestData(url,'{}',function(data){	
		pageButtonMap = data.obj["pageButtonMap"];
		//验证是否有添加权限
		if(pageButtonMap && pageButtonMap["RES_PERSONNEL_POST_ADD"]){
			$("#add").html('<a href="javascript:void(0)" onclick="addPostBtn(\'post-add.html?supperResId='+ supperResId +'\')" class="glyphicon glyphicon-plus btn btn-default m-mt-m7">新增</a>');
		}
		//验证是否有导出权限
		if(pageButtonMap && pageButtonMap["RES_PERSONNEL_POST_EXPORT"]){
			var htmls ='<strong class="col-md-5 g-file-input-title">导出查询结果:</strong><input id="exportExcel" onclick="exportExcel()" value="导出EXLCE" type="text" class="btn btn-default col-md-3">';
			$("#excel").html(htmls);
		}
		$("#search").click();
	});
}

//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	loading("正在查询...");
	var url = "personnel/post/pagePost.ihtml";
	var json ='{';
	if($("#departmentType").val()){json+='"departmentType":"'+$("#departmentType").val()+'",';}
	if($("#postName").val()){json+='"postName":"'+$("#postName").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			var number=0;
			$.each(data.obj.rows,function(n,postVo) {
	      		htmls +='<tr>'
	      					+'<td>'+ (++number) +'</td>'
							+'<td>'+ postVo.postCode +'</td>'
							+'<td>'+ postVo.postName +'</td>'
							+'<td>'+ (postVo.text2?postVo.text2:"无") +'</td>'
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_PERSONNEL_POST__PERMISSION"]){
								htmls += '<td><a href="rights-mgt.html?id='+ postVo.id +'&supperResId='+ supperResId +'">岗位权限</a></td>';
							}else{
								htmls += '<td></td>';
							}
				htmls += '<td>'+ (postVo.status==1?"启用":"禁用") +'</td>'
							+'<td>'+ postVo.updateTime  +'</td>'
							+'<td>';
							//验证是否有查看权限
							if(pageButtonMap && pageButtonMap["RES_PERSONNEL_POST_DETAIL"]){
								htmls +='<a  href="javascript:void(0)" onclick="detailPostBtn(\'post-detail.html?id='+ postVo.id +'\')" >查看</a>&nbsp;&nbsp;';
							}
							//验证是否有编辑权限
							if(pageButtonMap && pageButtonMap["RES_PERSONNEL_POST_EDIT"]){
								htmls +='<a href="javascript:void(0)" onclick="editPostBtn(\'post-modity.html?id='+ postVo.id +'\')">编辑</a>';
							}
				htmls +='</td></tr>';
	      });
	    }
		$("#dataList").html(htmls);
		unloading();
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	});
}

//导出EXCEL
$("#exportExcel").click(function(){
	var json ='{';
	if($("#organType").val()){json+='"organType":"'+$("#organType").val()+'",';}
	if($("#departmentType").val()){json+='"departmentType":"'+$("#departmentType").val()+'",';}
	if($("#postName").val()){json+='"postName":"'+$("#postName").val()+'",';}
	json +='"pageSize":'+ pageSize +'}';
	var url = getIP() + "personnel/post/exportExcelPost.ihtml?data=" + encodeURIComponent(encodeURIComponent(json));
	window.location.href = url;
});

//添加页面加载数据
function addPosteeLoad(){
	var url = "personnel/post/toAddPost.ihtml";
	RequestData(url,"{}",function(data){
		if(data.code == "0" && data.obj){
			var htmls = "<li>";
			$.each(data.obj,function(n,item){
				if(n != 0 && (n%5) == 0){
					htmls += '</li><li>';
				}
				htmls +='<label class="checkbox-inline"><input class="px" type="checkbox" onchange="depIdCount()" name="depId" value="'+ item.id +'"><span class="lbl">'+ item.departmentName +'</span></label>';
	       });
			htmls += '</li>';
           $("#depList").html(htmls);
		}
	});
	
	$(function(){
        var validate = $("#expen-from").validate({
            rules:{
                postName:{required:true,minlength:2},
                personnelNum:{required:true,digits:true}  
            },
            submitHandler: function(form){
            	/*if(!$("#departmentType").val()){
            		layer.alert("请选择部门类型");
            		return;
            	}*/
				addPost();
            },  
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}                 
        });      
    });
    
    $.validator.addMethod("selectValidate",function(value,element){if(value==0){return false;}else{return true;}},"请选择");
}

//添加岗位
function addPost(){
	loading("加载中...");
	var url = "personnel/post/addPost.ihtml";
	var json ='{';
	json+='"postName":"'+$("#postName").val()+'",';
	json+='"departmentType":"'+1+'",';
	if($("[name='depId']:checked").length!=0){json+='"personnelNum":"'+$("#personnelNum").val()+'",';};
	json+='"text2":"'+$("#text2").val()+'",';
	json+='"status":"'+$("#status").val()+'",';
	var depIds = "";
	$('input[name="depId"]:checked').each(function(i,node){
		var depId = $(node).val();
		if(i>0){
			depIds +=',';
		}
		depIds += '"'+depId+'"';
	});
	json += '"depIds":['+ depIds +'],';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.window.page();
				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
				parent.layer.close(index);
				unloading();
			});
		}else{
			layer.alert(data.msg);
			unloading();
		}
	});
}

//查看岗位详情
function postDetail(){
	var url = "personnel/post/postDetail.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			$("#postName").html("&nbsp;&nbsp;"+data.obj.postName);
			$("#postNo").html("&nbsp;&nbsp;"+(data.obj.postCode));
			//$("#departmentType").html("&nbsp;&nbsp;"+(data.obj.departmentType==1?"自有部门":data.obj.departmentType==2?"加盟商部门":"无"));
			$("#status").html("&nbsp;&nbsp;"+(data.obj.status==1?"启用":"禁用"));
			$("#text2").html("&nbsp;&nbsp;"+(data.obj.text2?data.obj.text2:"无"));
			$("#createPerson").html("&nbsp;&nbsp;"+data.obj.createPerson);
			$("#createTime").html("&nbsp;&nbsp;"+data.obj.createTime);
			$("#updatePerson").html("&nbsp;&nbsp;"+data.obj.updatePerson);
			$("#updateTime").html("&nbsp;&nbsp;"+data.obj.updateTime);
			$("#depNames").html("&nbsp;&nbsp;"+(data.obj.depNames?data.obj.depNames:""));
		}else{
			layer.alert(data.msg);
		}
	});
}

//编辑页面加载数据
function editPosteeLoad(){
	var url = "personnel/post/toUpdatePost.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			var depMap = data.obj["depMap"];
			var htmls = "<li>";
			$.each(data.obj.departmentVos,function(n,item){
				if(n != 0 && (n%5) == 0){
					htmls += '</li><li>';
				}
				if(depMap && depMap[item.id]){
					$("#supperPersonnelNum").show();
					htmls +='<label class="checkbox-inline"><input class="px" onchange="depIdCount()" type="checkbox" name="depId" checked="checked" value="'+ item.id +'"><span class="lbl">'+ item.departmentName +'</span></label>';
				}else{
					htmls +='<label class="checkbox-inline"><input class="px" onchange="depIdCount()" type="checkbox" name="depId" value="'+ item.id +'"><span class="lbl">'+ item.departmentName +'</span></label>';
				}
	       });
			htmls += '</li>';
			
	       $("#depList").html(htmls);
			
			
			$("#id").val(data.obj.vo.id);
			$("#versionLock").val(data.obj.vo.versionLock);
			$("#postName").val(data.obj.vo.postName);
			$("#departmentType").val(data.obj.vo.departmentType);
			$("#personnelNum").val(data.obj.vo.personnelNum);
			$("#status").find("optin[value="+data.obj.vo.status+"]").attr("selected","selected");
			$("#text2").val(data.obj.vo.text2);
			$("#remark").val(data.obj.vo.remark);
		}else{
			layer.alert(data.msg);
		}
	});
	
	$(function(){
        var validate = $("#expen-from").validate({
            rules:{
                postName:{required:true,minlength:2},       
                organType:{selectValidate:true},  
                departmentType:{selectValidate:true} 
            },
            submitHandler: function(form){
				editPost();
            },  
            errorPlacement: function(error, element) {
            	element.next().html("");
				error.appendTo(element.next()); 
			}                 
        });      
    });
    
    $.validator.addMethod("selectValidate",function(value,element){if(value==0){return false;}else{return true;}},"请选择");
}

//编辑岗位
function editPost(){
	loading("加载中...");
	var url = "personnel/post/updatePost.ihtml";
	var json ='{';
	json+='"id":"'+$("#id").val()+'",';
	json+='"versionLock":"'+$("#versionLock").val()+'",';
	json+='"postName":"'+$("#postName").val()+'",';
	var depIds = "";
	$('input[name="depId"]:checked').each(function(i,node){
		var depId = $(node).val();
		if(i>0){
			depIds +=',';
		}
		depIds += '"'+depId+'"';
	});
	json += '"depIds":['+ depIds +'],';
	json+='"departmentType":"'+1+'",';
	if($("[name='depId']:checked").length!=0){json+='"personnelNum":"'+$("#personnelNum").val()+'",';};
	json+='"text2":"'+$("#text2").val()+'",';
	json+='"status":"'+$("#status").val()+'",';
	json+='"remark":"'+$("#remark").val()+'"}';
	RequestData(url,json,function(data){
		if(data.code == "0"){
			layer.alert(data.msg,function(){
				parent.window.page();
				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
				parent.layer.close(index);
				unloading();
			});
		}else{
			layer.alert(data.msg);
			unloading();
		}
	});
}


//加载岗位权限页面数据
function rightsLoad(){
	var url = "personnel/postPermission/toAddPostPermission.ihtml";
	var json ='{"id":"'+request("id")+'"}';
	RequestData(url,json,function(data){
		for (var key in data.obj) {  
			if(key == "postVo"){   //岗位信息
				var htmls = '<tr><input type="hidden" id="postId" value="'+ data.obj[key].id +'">'+
								'<td id="postName">'+ data.obj[key].postName +'</td>'+
								'<td>'+ (data.obj[key].organType==1?"平台机构":"加盟机构")  +'</td>'+
								'<td>'+ (data.obj[key].departmentType==1?"技术支持":data.obj[key].departmentType==2?"产品运营":data.obj[key].departmentType==3?"市场推广":data.obj[key].departmentType==4?"线下业务":"财务会计") +'</td>'+
								'<td>'+ (data.obj[key].text2?data.obj[key].text2:"") +'</td>'+
								'<td>'+ (data.obj[key].status==1?"启用":"禁用")+'</td>'+
							'</tr>';
	            $("#postInfo").html(htmls);
	        }else if(key == "supperResList"){   //所有一级权限模块
	        	var htmls = '';
	          	$.each(data.obj[key],function(n,resource) {
	          		htmls +='<option value="'+ resource.id +'" property="'+ resource.rescCode +'">'+ resource.resName +'</option>';
	          	});
	          	$("#supperRes").html(htmls);
	          	supperSupperRes();
	        }
        }
        $.cookie("jsessionid",data.obj["jsessionid"]);
	});
	
	$("#return-post").click(function(){window.location.href ="post-list.html";});
	$("#save-post").click(function(){savePostPermission();});
	$("#checkedAll").bind("click", function () {
		  $("[name = rescCode]:checkbox").prop("checked", true);
	});
	$("#cancelAll").bind("click", function () {
		$("[name = rescCode]:checkbox").prop("checked", false);
	});
}



//加载模块对应的资源
function supperSupperRes(){
//$("#supperSupperRes").on( "autocompletechange", function( event, ui ) {
	if($("#supperRes").val()){
		var url = "personnel/postPermission/getSystemResource.ihtml";
		var json ='{"id":"'+ $("#supperRes").val() +'",';
		json +='"resCode":"'+$("#supperRes").find('option:selected').attr("property")+'",';
		json +='"postId":"'+$("#postId").val() +'"}';
		loading();
		RequestData(url,json,function(data){
			unloading();
			var htmls = "";
			var count = 0;
				$.each(data.obj["resList"],function(n,resource) {
					 htmls += '<tr>'+
									'<td>'+ resource.remark.substring(1,resource.remark.length) +'--'+ resource.resName +/*'</td><td>'+
							'<input type="radio" name="radio1"/>管理员<br />'+
										'<input type="radio" name="radio1"/>操作员	'+*/'</td><td><input type="hidden" id="'+ resource.rescCode +'" value="'+ resource.resName +'" class="supperResc">';
									$.each(resource.chirldList,function(n,buttonResource) {
										if(data.obj["postPermissionVoMap"][buttonResource.rescCode]){
											 htmls += '<input type="checkbox" checked="checked" name="rescCode" value="'+ buttonResource.rescCode +'"/><label for="">'+ buttonResource.resName +'</label>&nbsp;&nbsp;';
										}else{
											 htmls += '<input type="checkbox" name="rescCode" value="'+ buttonResource.rescCode +'"/><label for="">'+ buttonResource.resName +'</label>&nbsp;&nbsp;';
										}
									});
					htmls += '</td></tr>';
					count++;
		       });
			htmls = '<tr><td rowspan="'+ count +'">'+ $("#supperRes").find('option:selected').text() +'</td>' + htmls.substring(4,htmls.length);
            $("#resList").html(htmls);
		});
	}
//});
}

//设置岗位权限
function savePostPermission(){
	var url = "personnel/postPermission/savePostPermission.ihtml;jsessionid=" + $.cookie("jsessionid");
	var json ='{"postId":"'+ $("#postId").val() +'",';
	json +='"postName":"'+$("#postName").html()+'",';
	json +='"permissions":[{"rescCode":"'+ $("#supperRes").find('option:selected').attr("property") +'","resName":"'+ $("#supperRes").find('option:selected').text() +'",childResource:[';
  	$('input[name="rescCode"]:checked').each(function(i,checkbox){    
  		if(i==0){
	  		json +='{"rescCode":"'+$(checkbox).val()+'",';
  		}else{
  			json +=',{"rescCode":"'+$(checkbox).val()+'",';
  		}
		json +='"resName":"'+$(checkbox).next().html()+'",';
  		json +='"supperRescCode":"'+ $($(checkbox).parent().children(".supperResc")[0]).attr("id") +'",';
		json +='"supperRescName":"'+ $($(checkbox).parent().children(".supperResc")[0]).val() +'"}';
  	});    
	json +=']}]}';
	RequestData2(url,json,function(data){
		layer.alert(data.msg);
	});
}


$("#checkedAll").click(function () {
	$("[name = depId]:checkbox").prop("checked", true);
	$("#supperPersonnelNum").show();
});
$("#cancelAll").click(function () {
	$("[name = depId]:checkbox").prop("checked", false);
	$("#supperPersonnelNum").hide();
});

function depIdCount(){
    if($("[name='depId']:checked").length > 0){
		$("#supperPersonnelNum").show();
	}else{
		$("#supperPersonnelNum").hide();       	
	}
}


//新增按钮iframe层
function addPostBtn(wb){
	layer.open({
		title:'新增岗位',
	    type: 2,
	    area: ['70%', '80%'],
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    content: wb
	});
}

//编辑页面iframe层
function editPostBtn(wb){
	layer.open({
	    type: 2,
		title:'编辑岗位',
	    area: ['70%', '80%'],
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    content: wb
	});
}

//查看页面iframe层
function detailPostBtn(wb){
	layer.open({
	    type: 2,
		title:'查看详情',
	    area: ['70%', '80%'],
	    skin: 'layui-layer-lan',
	    shade: 0.3, //遮罩透明度
	    shadeClose:true,
	    content: wb
	});
}