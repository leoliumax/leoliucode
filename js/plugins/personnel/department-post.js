﻿//初始化岗位查询页面
function queryPostLoad(){
	$("#search").click();
}


//搜索
$("#search").click(function(){
	$("#pageNo").val(1);
	page();
});
	
function page(){
	var url = "personnel/post/pagePost.ihtml";
	var json ='{';
	if($("#postName").val()){json+='"postName":"'+$("#postName").val()+'",';}
	if($("#pageNo").val()){json+='"pageNo":"'+$("#pageNo").val()+'",';}
	json +='"pageSize":'+ pageSize +',"status":"1"}';
	RequestData(url,json,function(data){	
		$($(".panel-title")[0]).html("共有"+ data.obj.total+"条记录，每页"+ data.obj.pageSize+"条记录");
		var htmls = '';
		if(data.obj.rows){
			$.each(data.obj.rows,function(n,postVo) {
				 if($("#"+ postVo.id,parent.document).val()){
		      		htmls +='<tr>'
							+'<td><input type="checkbox" value="'+ postVo.id +'" name="postId"/></td>'
							+'<td>'+ postVo.postCode +'</td>'
							+'<td>'+ postVo.postName +'</td>'
							+'<td>已添加</td>'
						+'</tr>'
				 }else{
				 	htmls +='<tr>'
							+'<td><input type="checkbox" value="'+ postVo.id +'" name="postId"/></td>'
							+'<td>'+ postVo.postCode +'</td>'
							+'<td>'+ postVo.postName +'</td>'
							+'<td id="'+ postVo.id +'"><a href="javascript:addPost(\''+ postVo.id +'\',\''+ postVo.postName +'\')">添加</a></td>'
						+'</tr>'
				 }
	      });
	    }
		$("#dataList").html(htmls);
		//加载分页模块
		bindPageEvent(data.obj.pageNo,data.obj.page);
	})
}

//添加选中岗位
$("#addChecked").click(function(){
	$('input[name="postId"]:checked').each(function(i,checkbox){
		var postId = $(checkbox).val();
		if(!$("#"+ postId,parent.document).val()){
			var postName =$($(checkbox).parent().nextAll()[1]).html();
			window.parent.addDepartmentPost(postId,postName);
			$($(checkbox).parent().nextAll()[2]).html("已添加");
		}
	});
})

//添加岗位
function addPost(postId,postName){
	window.parent.addDepartmentPost(postId,postName);
	$("#"+postId).html("已添加");
}

//全选
$("#checkedAll").click(function(){
	$('input[name="postId"]').each(function(i,checkbox){
		$(checkbox).prop("checked",$("#checkedAll").prop("checked"));
	});
})